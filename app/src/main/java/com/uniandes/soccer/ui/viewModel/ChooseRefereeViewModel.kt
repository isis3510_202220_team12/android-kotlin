package com.uniandes.soccer.ui.viewModel

import android.os.Bundle
import android.util.Log
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.R
import com.uniandes.soccer.data.cache.UserCache
import com.uniandes.soccer.data.model.match.Match
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.domain.user.GetPlayers
import com.uniandes.soccer.domain.user.GetReferees
import com.uniandes.soccer.ui.viewModel.utilities.LocationComparator
import com.uniandes.soccer.ui.viewModel.utilities.TextsComparator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.util.ArrayList
import javax.inject.Inject

@HiltViewModel
class ChooseRefereeViewModel @Inject constructor(
 private val getRefereesUseCase: GetReferees,
 private val userCache: UserCache,
 private val locationComparator: LocationComparator,
): ViewModel() {
    lateinit var firebaseAnalytics: FirebaseAnalytics

    val refereesList: MutableState<List<User>> = mutableStateOf(listOf())
    private val _isCharging = MutableLiveData<Boolean>()
    val isCharging: LiveData<Boolean> = _isCharging
    private val _search = MutableLiveData<String>()
    val search : LiveData<String> = _search
    val location: MutableState<GeoPoint> = mutableStateOf(userCache.location)
    private val _initialList: MutableState<List<User>> = mutableStateOf(listOf())
    val _contentText: MutableState <String> = mutableStateOf("")

    val showDialog: MutableState <Boolean> = mutableStateOf(false)

    fun getReferees(){
        viewModelScope.launch {
            val parameters = Bundle().apply {
                this.putString("user_id", userCache.id)
                this.putString("user_name", userCache.name)
                this.putString("user_location", userCache.locationName)
            }
            try {
                val referees = getRefereesUseCase()
                locationComparator.setActualLocation(userCache.location)
                _initialList.value= referees.sortedWith(locationComparator.thenByDescending { it.rating })
                refereesList.value= _initialList.value
                _contentText.value = "The closest and best rated referee to you is: " + refereesList.value[0].name
                onOpenDialog()


                var r = ""
                if(refereesList.value.isNotEmpty()) {
                    val a = refereesList.value[0]
                        r = "Id: " + a.id + " / Referee_name: " + a.name
                }

                parameters.putString("referee",r)
                firebaseAnalytics.logEvent( "Closest_and_best_referee", parameters)
            }
            catch (e:Exception){
                val error = e.toString().split(":").toTypedArray()
                Log.d("Referee list", "get the referee list failed: ${error[1]}")
                e.printStackTrace()
            }

            _isCharging.value = false

        }
    }


    fun setLocation(lat: Float, lng: Float){
        location.value = GeoPoint(lat.toDouble(), lng.toDouble())
    }


    fun orderReferees(){
        locationComparator.setActualLocation(userCache.location)
        refereesList.value = refereesList.value.sortedWith(locationComparator.thenByDescending { it.rating })
    }

    fun onSearchBarChanged(search:String) {
        _search.value = search
        searchReferee(search)
    }

    private fun searchReferee(search:String){
        refereesList.value = _initialList.value.filter {
            it.name.contains(search, ignoreCase = true) ||
                    search.contains(it.name, ignoreCase = true)
        }
    }

    fun addAnalytics(analytics: FirebaseAnalytics) {
        firebaseAnalytics = analytics
    }

    fun onOpenDialog() {
        showDialog.value = true
    }

    fun onDialogConfirm() {
        showDialog.value = false
    }

    fun onContentTextChange(text: String) {
        _contentText.value = text
    }

    fun onDialogDismiss() {
        showDialog.value = false
    }
}

