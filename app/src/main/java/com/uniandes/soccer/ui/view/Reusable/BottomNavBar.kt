package com.uniandes.soccer.ui.view.Reusable

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material.BottomNavigation
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.uniandes.soccer.R
import com.uniandes.soccer.data.cache.UserCache

@Composable
fun BottomNavBar(modifier: Modifier, navController: NavController){
    val userCache: UserCache = UserCache
    Spacer(modifier=modifier)
    BottomNavigation (
            backgroundColor = Color.White
            ){

        Row(modifier = Modifier
            .fillMaxWidth()
            .height(55.dp), horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
            ) {
            Image(
                painterResource(R.drawable.trophy),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.fillMaxHeight(0.9f)
            )
            Image(
                painterResource(R.drawable.play),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.fillMaxHeight(0.9f)
            )
            Image(
                painterResource(R.drawable.house),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.fillMaxHeight(0.9f)
            )
            Image(
                painterResource(R.drawable.bell),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.fillMaxHeight(0.9f)
            )
            Image(
                painterResource(R.drawable.user),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .fillMaxHeight(0.9f)
                    .clickable { navController.navigate("playerDetail/${userCache.id}") }
            )

        }
    }
}