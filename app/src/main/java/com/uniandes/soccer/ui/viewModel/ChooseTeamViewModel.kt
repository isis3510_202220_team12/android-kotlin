package com.uniandes.soccer.ui.viewModel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.uniandes.soccer.data.cache.MyTeamsList
import com.uniandes.soccer.data.cache.UserCache
import com.uniandes.soccer.data.model.roomDB.TeamRoom
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.domain.team.GetUsersTeams
import com.uniandes.soccer.domain.team.GetUsersTeamsRoom
import com.uniandes.soccer.ui.view.isConnected
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChooseTeamViewModel @Inject constructor(
    private val getUsersTeamsUseCase: GetUsersTeams,
    private val userCache: UserCache,
    private val myTeams: MyTeamsList,
    private val getTeamsRoom: GetUsersTeamsRoom
): ViewModel(){
    val myTeamsList: MutableState<List<Team>> = mutableStateOf(listOf())
    private val _myInitialList: MutableState<List<Team>> = mutableStateOf(listOf())
    private val _isCharging = MutableLiveData<Boolean>()
    val isCharging: LiveData<Boolean> = _isCharging
    private val _showDialog = MutableStateFlow(false)
    val showDialog: StateFlow<Boolean> = _showDialog.asStateFlow()


    fun getTeamsByUserId() {
        viewModelScope.launch {
            if (isConnected()) {
                try {
                    val teams = getUsersTeamsUseCase(userCache.id)
                    myTeams.saveTeamsToCache(userCache.id, teams)
                    _myInitialList.value = teams
                    myTeamsList.value = _myInitialList.value
                } catch (e: Exception) {
                    val error = e.toString().split(":").toTypedArray()
                    Log.d("My Teams list", "get the teams list failed: ${error[1]}")
                    e.printStackTrace()
                }
            }
            val usersTeamsRoom = getTeamsRoom(userCache.id)
            if(usersTeamsRoom.isNotEmpty()) {
                _myInitialList.value = usersTeamsRoom
                myTeamsList.value = _myInitialList.value
            } else {
                val teams = myTeams.retrieveTeamsFromCache(userCache.id)
                if (teams != null) {
                    _myInitialList.value = teams
                    myTeamsList.value = _myInitialList.value
                }
            }
            _isCharging.value = false
        }
    }

    fun onOpenDialog() {
        _showDialog.value = true
    }

    fun onDialogConfirm() {
            _showDialog.value = false
    }

    fun onDialogDismiss() {
        _showDialog.value = false
    }


}