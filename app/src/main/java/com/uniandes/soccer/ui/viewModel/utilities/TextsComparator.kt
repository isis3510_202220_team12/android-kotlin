package com.uniandes.soccer.ui.viewModel.utilities

import android.util.Log
import com.uniandes.soccer.data.model.user.User
import javax.inject.Inject

class TextsComparator @Inject constructor(

): Comparator<User>{
    private val compareStringsAlgorithm: CompareStringsAlgorithm = CompareStringsAlgorithm()
    private var search: String = ""
    override fun compare(u1: User?, u2: User?): Int {
        return if (u1==null||u2==null){
            0
        }else {
            val similarity1 = compareStringsAlgorithm.calculateSimilarity(
                search,
                u1.description!!
            )
            Log.d("u1 simil", similarity1.toString())
            val similarity2 = compareStringsAlgorithm.calculateSimilarity(
                search,
                u2.description!!
            )
            Log.d("u2 simil", similarity2.toString())
            similarity2.compareTo(similarity1)
        }
    }

    fun setSearch(search: String){
        this.search = search
    }

}