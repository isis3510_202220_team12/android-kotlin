package com.uniandes.soccer.ui.viewModel

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.CountDownTimer
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.core.app.ActivityCompat
import androidx.lifecycle.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.cache.LocationCache
import com.uniandes.soccer.data.cache.UserCache
import com.uniandes.soccer.data.localDB.LocationDBHandler
import com.uniandes.soccer.data.model.location.LocationModel
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.domain.team.CreateTeam
import com.uniandes.soccer.ui.view.MainActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@HiltViewModel
class CreateTeamViewModel @Inject constructor(
    application: Application,
    private val userCache: UserCache,
    private val createTeam: CreateTeam,
    private val locationCache: LocationCache,
): AndroidViewModel(application) {

    private val app = application

    private val _name = MutableLiveData<String>()
    val name: LiveData<String> = _name

    private val _ranking = MutableLiveData<String>()
    val ranking: LiveData<String> = _ranking

    private val _desc = MutableLiveData<String>()
    val desc: LiveData<String> = _desc

    private val _createEnable = MutableLiveData<Boolean>()
    val createEnable: LiveData<Boolean> = _createEnable

    val location= MutableStateFlow<Location>(getInitialLocation())

    private val _locAcc = MutableLiveData(false)
    val locAcc : LiveData<Boolean> = _locAcc

    val selectedPlayers:  MutableState<List<String>> = mutableStateOf(listOf())

    val dataRecovered: MutableState<Boolean> = mutableStateOf(false)

    private val _createTeamEnable = MutableLiveData<Boolean>()

    fun onCreateTeamChanged(name: String, contact: String, desc: String, loc: Boolean) {

        _name.value = name
        _ranking.value = contact
        _desc.value = desc
        _locAcc.value = loc
        try{
            _createEnable.value =
                isValidName(name) && isValidContact(contact) && isValidDescription(desc) && _locAcc.value!!
        }catch (e:Exception){
            _createEnable.value = false
        }
    }

    fun setDataRecovered(b: Boolean) {
        dataRecovered.value=b
    }

    private fun isValidName(name: String): Boolean = name.length > 5

    private fun isValidContact(contact: String): Boolean = contact.length <= 5

    private fun isValidDescription(desc: String): Boolean = desc.length > 10

    private fun getInitialLocation(): Location {
        val initialLocation = Location("")
        initialLocation.latitude = 4.6011
        initialLocation.longitude = -74.065
        return initialLocation
    }
    fun updateLocation(latitude: Double, longitude: Double) {
        //TODO change for local storage
        if (latitude != location.value.latitude) {
            val plocation = Location("")
            plocation.latitude = latitude
            plocation.longitude = longitude
            location.value = plocation
        }
    }


    fun createATeam() = viewModelScope.launch {
        try {
            createTeam(
                _name.value!!, userCache.id, _desc.value!!, _ranking.value?.toInt()!!, selectedPlayers.value,
                GeoPoint(location.value.latitude, location.value.longitude)
            )
        } catch(e:Exception) {
            val error = e.toString().split(":").toTypedArray()
            Log.d("Create Team", "the creation of the team failed: ${error[1]}")
            e.printStackTrace()
        }
    }

    fun updateLocation(context: Context, sharedPref: SharedPreferences){
        lateinit var locationList: List<LocationModel>
        val dbHandler: LocationDBHandler = LocationDBHandler(context);
        locationList = java.util.ArrayList<LocationModel>()
        if(locationCache.latitude == 0.0)
        {
            locationList = dbHandler.readLocation()!!
        }
        viewModelScope.launch {
            val mFusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(app)
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                mFusedLocationClient.lastLocation
                    .addOnSuccessListener { pLocation ->
                        if (pLocation != null) {
                            location.value.latitude = pLocation.latitude
                            location.value.longitude = pLocation.longitude
                            launch(Dispatchers.IO) {
                                dbHandler.addLocation(
                                    pLocation.latitude,
                                    pLocation.longitude,
                                )
                                with(sharedPref.edit()) {
                                    putString("createTeam/latitude", pLocation.latitude.toString())
                                    putString("createTeam/longitude", pLocation.longitude.toString())
                                    commit()
                                }
                            }
                        }
                        else{
                            if(locationCache.latitude == 0.0) {
                                if (locationList.isNotEmpty()) {
                                    var loc = locationList.last()

                                    location.value.latitude = loc.latitude
                                    location.value.longitude = loc.longitude
                                }
                            }
                            else{
                                location.value.latitude = locationCache.latitude
                                location.value.longitude = locationCache.longitude
                            }
                        }

                    }
            }
        }


    }

    fun setUpValues(mainActivity: MainActivity){
        val sharedPref: SharedPreferences = mainActivity.getPreferences(Context.MODE_PRIVATE)
        val name = sharedPref.getString("createTeam/name", "")
        if(name != null)
        {
            _name.value = name
        }
        else{
            _name.value = ""
        }
        val ranking = sharedPref.getString("createTeam/ranking", "")
        if(ranking != null)
        {
            _ranking.value = ranking
        }
        else{
            _ranking.value = ""
        }
        val description = sharedPref.getString("createTeam/desc", "")
        if(description != null)
        {
            _desc.value = description
        }
        else{
            _desc.value = ""
        }
        var lat = sharedPref.getString("createMatch/latitude", "4.6011")
        var lon = sharedPref.getString("createMatch/longitude", "-74.065")
        if(lat != null && lat != "")
        {
            location.value.latitude = lat.toDouble()
        }
        else{
            location.value.latitude = "4.6011".toDouble()
        }
        if(lon != null && lon != "")
        {
            location.value.longitude = lon.toDouble()
        }
        else{
            location.value.longitude = "-74.065".toDouble()
        }
        _createTeamEnable.value = false
    }

    fun resetValues(sharedPref: SharedPreferences){
        with(sharedPref.edit()) {
            putString("createTeam/name", "")
            putString("createTeam/ranking", "")
            putString("createTeam/desc", "")
            commit()
        }
    }

    val addressText = mutableStateOf("")
    var isMapEditable = mutableStateOf(true)
    var timer: CountDownTimer? = null

    fun getAddressFromLocation(context: Context): String {

        val geocoder = Geocoder(context, Locale.getDefault())
        var addresses: List<Address>? = null
        val address: Address?
        var addressText = ""

        try {
            addresses =
                geocoder.getFromLocation(location.value.latitude, location.value.longitude, 1)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        address = addresses?.get(0)
        addressText = address?.getAddressLine(0) ?: ""

        return addressText
    }

    fun onTextChanged(context: Context, text: String) {
        if (text == "")
            return
        timer?.cancel()
        timer = object : CountDownTimer(1000, 1500) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                location.value = getLocationFromAddress(context, text)
            }
        }.start()
    }


    fun getLocationFromAddress(context: Context, strAddress: String): Location {

        val geocoder = Geocoder(context, Locale.getDefault())
        val addresses: List<Address>?
        val address: Address?

        addresses = geocoder.getFromLocationName(strAddress, 1)

        if (addresses!!.isNotEmpty()) {
            address = addresses[0]

            var loc = Location("")
            loc.latitude = address.getLatitude()
            loc.longitude = address.getLongitude()
            return loc
        }

        return location.value
    }

    fun addPlayer(playerId: String) {
        if(!selectedPlayers.value.contains(playerId) && playerId!== "") {
            val aux = mutableListOf<String>()
            aux.addAll(selectedPlayers.value)
            aux.add(playerId)
            selectedPlayers.value = aux
        }
    }
}
