@file:JvmName("TeamsListKt")

package com.uniandes.soccer.ui.view

import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.uniandes.soccer.R
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.ui.view.Reusable.ConnectionAlertDialog
import com.uniandes.soccer.ui.view.Reusable.LoadingScreen
import com.uniandes.soccer.ui.view.Reusable.TeamItem
import com.uniandes.soccer.ui.viewModel.TeamsListViewModel

private lateinit var toast1: Toast

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun TeamsList(navController: NavHostController, teamsListViewModel: TeamsListViewModel) {
    val search: String by teamsListViewModel.search.observeAsState(initial = "")
    val teamsList: List<Team> by teamsListViewModel.teamsList
    val context = LocalContext.current
    val keyboardController = LocalSoftwareKeyboardController.current
    val isCharging: Boolean by teamsListViewModel.isCharging.observeAsState(initial = true)
    val focusRequester = remember { FocusRequester() }
    val focusManager = LocalFocusManager.current
    val showDialogVisible: Boolean by teamsListViewModel.showDialog.collectAsState()



    LaunchedEffect(Unit) {
        if(!isConnected() ){
            teamsListViewModel.onOpenDialog()
        }
        teamsListViewModel.getTeamsByUserId()
    }

    Column(
        modifier = Modifier
            .background(Color.White)
            .fillMaxSize()
            .fillMaxHeight()
            .padding(bottom = 55.dp)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = Color.White),
            ) { focusManager.clearFocus() },
        horizontalAlignment = CenterHorizontally
    ) {
        Surface(
            elevation = 3.dp
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(86.dp)
                    .focusRequester(focusRequester)
                    .padding(start = 10.dp, top = 7.dp, bottom = 7.dp)
            ) {
                TextField(
                    modifier = Modifier
                        .fillMaxWidth(0.8f)
                        .fillMaxHeight()
                        .padding(10.dp),
                    singleLine = true,
                    value = search,
                    onValueChange = {
                        if (it.length <= 20) teamsListViewModel.onSearchBarChanged(it)
                        else {
                            try {
                                toast1.cancel()
                            } catch (e: Exception) {         // invisible if exception
                                toast1 = Toast.makeText(
                                    context,
                                    "Cannot be more than 20 Characters",
                                    Toast.LENGTH_SHORT
                                );
                            }
                            toast1.show();
                        }
                    },
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
                    keyboardActions = KeyboardActions(
                        onSearch = {
                            keyboardController?.hide(); teamsListViewModel.onSearchBarChanged(
                            search
                        )
                        }),
                    label = { Text(text = "Search") },
                    leadingIcon = {
                        Icon(Icons.Filled.Search, "")
                    },
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = Color(0xFFFCF0F0),
                        unfocusedIndicatorColor = Color.Transparent
                    ),
                    shape = RoundedCornerShape(8.dp)
                )

                Image(
                    painterResource(R.drawable.menu),
                    contentDescription = "",
                    contentScale = ContentScale.FillHeight,
                    modifier = Modifier
                        .fillMaxHeight(0.8f)
                        .padding(top = 11.dp, start = 12.dp)
                        .clickable(
                            enabled = true,
                            onClickLabel = "Clickable image",
                            onClick = {
                                navController.navigate("mainMenu")
                            }
                        ),


                    )

            }
        }

        if(isConnected()) {
            if (!isCharging) {
                LazyColumn() {

                    items(teamsList) { team ->
                        var modifier = Modifier
                            .border(
                                BorderStroke(1.dp, Color(0xFFD1D1D1)),
                                shape = RoundedCornerShape(5.dp)
                            )
                            .padding(start = 5.0.dp, end = 5.0.dp)
                            .height(60.dp)
                            .fillMaxWidth(0.9f)
                            .clickable { //navController.navigate("matchesList/${team.id}") **/
                            }
                        TeamItem(navController,modifier = modifier, team = team)

                    }

                }
            } else {
                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .padding(bottom = 20.dp),
                    verticalArrangement = Arrangement.Center
                ) {
                    LoadingScreen()
                }
            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 70.dp, end = 20.dp),
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.End
            ) {
                FloatingActionButton(
                    onClick = { navController.navigate("createTeam") },
                    modifier = Modifier
                        .size(60.dp),
                    backgroundColor = Color(0xFFFFE5E5),

                    elevation = FloatingActionButtonDefaults.elevation(3.dp)

                ) {
                    Icon(
                        modifier = Modifier
                            .fillMaxSize(0.6F),
                        imageVector = Icons.Default.Add,
                        contentDescription = "Create team",
                        tint = Color(0xFF950000),
                    )
                }
            }
        }

        ConnectionAlertDialog(
            title = "Connection failure",
            contentText= "Please connect to access to this service",
            show = showDialogVisible,
            onDismiss = teamsListViewModel::onDialogDismiss,
            onConfirm = teamsListViewModel::onDialogConfirm
        )
    }
}