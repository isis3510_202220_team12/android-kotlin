package com.uniandes.soccer.ui.view

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.material.*
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.navigation.NavHostController
import com.uniandes.soccer.R
import com.uniandes.soccer.ui.view.Reusable.BottomNavBar
import com.uniandes.soccer.ui.viewModel.LoginViewModel


@Composable
fun MainMenu(navController: NavHostController, viewModel: LoginViewModel) {

    Column(modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center) {

        Surface(
            elevation = 8.dp
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp)
                    .height(50.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.Top
            ) {
                Image(
                    painterResource(R.drawable.user),
                    contentDescription = "",
                    contentScale = ContentScale.FillHeight,
                    modifier = Modifier.fillMaxHeight()
                )
                MyImage()
                Column(horizontalAlignment = Alignment.End) {
                    Image(
                        painterResource(R.drawable.menu),
                        contentDescription = "",
                        contentScale = ContentScale.FillHeight,
                        modifier = Modifier
                            .fillMaxHeight()
                            .clickable (
                                interactionSource = remember { MutableInteractionSource() },
                                indication = rememberRipple(color = Color.Red)
                            ){
                                viewModel.signOut()
                                navController.navigate("login"){
                                    popUpTo(0)
                                }
                            }
                        ,
                    )
                }

            }
        }
        Spacer(modifier=Modifier.weight(1f))
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 40.dp, bottom = 40.dp, top = 20.dp)
            .height(105.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painterResource(R.drawable.teams),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .fillMaxHeight()
                    .clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = null,
                        enabled = true,
                        onClickLabel = "Clickable image",
                        onClick = {
                            navController.navigate("teamsList")
                        }
                    )
                    .padding(end = 5.dp)
            )
            Image(
                painterResource(R.drawable.matches),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .fillMaxHeight()
                    .clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = null,
                        enabled = true,
                        onClickLabel = "Clickable image",
                        onClick = {
                            navController.navigate("chooseTeam")
                        }
                    )
                    .padding(start = 5.dp)
            )
        }
        Row(modifier = Modifier
            .fillMaxWidth()
            .height(190.dp),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painterResource(R.drawable.fubol),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.fillMaxHeight()
            )
        }
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(40.dp)
            .height(105.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painterResource(R.drawable.players),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .fillMaxHeight()
                    .clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = null,
                        enabled = true,
                        onClickLabel = "Clickable image",
                        onClick = {
                            navController.navigate("choosePlayers")
                        }
                    )
            )
            Image(
                painterResource(R.drawable.referees),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .fillMaxHeight()
                    .clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = null,
                        enabled = true,
                        onClickLabel = "Clickable image",
                        onClick = {
                            navController.navigate("chooseReferee")
                        }
                    )
            )
        }
        BottomNavBar(modifier=Modifier.weight(1f), navController)
    }
}

@Composable
fun MyImage(){
    Image(painterResource(R.drawable.ic_launcher_foreground),
        "Mi imagen de prueba",
        modifier= Modifier
            .size(64.dp)
            .clip(CircleShape)
            .background(MaterialTheme.colors.primary)

    )
}