package com.uniandes.soccer.ui.viewModel

import android.os.Bundle
import android.util.Log
import android.util.Patterns
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.cache.MyMatchesList
import com.uniandes.soccer.data.cache.MyTeamsList
import com.uniandes.soccer.data.cache.UserCache
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.domain.authentication.SignIn
import com.uniandes.soccer.domain.authentication.SignOut
import com.uniandes.soccer.domain.team.DeleteTeamsRoom
import com.uniandes.soccer.domain.team.GetUsersTeams
import com.uniandes.soccer.domain.team.GetUsersTeamsRoom
import com.uniandes.soccer.domain.team.InsertUsersTeamsRoom
import com.uniandes.soccer.domain.user.DeleteUserRoom
import com.uniandes.soccer.domain.user.GetUserByEmail
import com.uniandes.soccer.domain.user.GetUserRoom
import com.uniandes.soccer.domain.user.InsertUserRoom
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val signInUseCase: SignIn,
    private val signOutUseCase: SignOut,
    private val userCache: UserCache,
    private val getUserByEmail: GetUserByEmail,
    private val deleteUserRoom: DeleteUserRoom,
    private val myMatchesList: MyMatchesList,
    private val myTeamsList: MyTeamsList,
    private val getUsersTeams: GetUsersTeams,
    private val getUsersTeamsRoom: GetUsersTeamsRoom,
    private val getUserRoom: GetUserRoom,
    private val insertUsersTeamsRoom: InsertUsersTeamsRoom,
    private val insertUserRoom: InsertUserRoom,
    private val deleteTeamsRoom: DeleteTeamsRoom
): ViewModel() {


    lateinit var firebaseAnalytics: FirebaseAnalytics
    var loginResult: MutableState<String> = mutableStateOf("")
    var signIn: MutableState<Boolean> = mutableStateOf(false)

    private val _email = MutableLiveData<String>()
    val email : LiveData<String> = _email

    private val _pass = MutableLiveData<String>()
    val pass : LiveData<String> = _pass

    private val _loginEnable = MutableLiveData<Boolean>()
    val loginEnable : LiveData<Boolean> = _loginEnable

    private val _logged = MutableLiveData(false)
    val logged : LiveData<Boolean> = _logged

    fun onLoginChanged(email: String, pass: String) {

        _email.value = email
        _pass.value = pass
        _loginEnable.value = isValidEmail(email) && isValidPassword(pass)
    }

    private fun isValidPassword(pass: String): Boolean = pass.length > 6

    private fun isValidEmail(email: String): Boolean = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    fun signOut() = viewModelScope.launch {
        try {
            signOutUseCase()
            userCache.name = ""
            userCache.age = 0
            userCache.email = ""
            userCache.id = ""
            userCache.location = GeoPoint(0.0,0.0)
            userCache.password = ""
            userCache.isReferee = false
            userCache.teams = null
            userCache.photo=null
            deleteUserRoom()
            deleteTeamsRoom()
            myMatchesList.lru.evictAll()
            myTeamsList.lru.evictAll()
            loginResult.value = "Logged out"
        }catch(e:Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d("SignOut", "signOut failed: ${error[1]}")
            e.printStackTrace()
        }
    }

    fun autoSignInUser() = viewModelScope.launch {
        try {
            val savedUser = getUserRoom()
            if(savedUser != null) {
                _loginEnable.value = false
                _email.value = savedUser.email
                _pass.value = savedUser.password
                userCache.name = savedUser.name
                userCache.age = savedUser.age
                userCache.email = savedUser.email
                userCache.id = savedUser.id
                userCache.location = savedUser.location
                userCache.password = savedUser.password
                userCache.isReferee = savedUser.isReferee
                userCache.teams = savedUser.teams
                userCache.photo = savedUser.photo
                val teams = getUsersTeamsRoom(savedUser.id)
                myTeamsList.saveTeamsToCache(savedUser.id, teams)
            }

            loginResult.value = "Login success"
            _logged.postValue(true)
            _loginEnable.value = true
            signIn.value = true
        } catch(e:Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d("Login", "autoSignInUser: ${error[1]}")
            e.printStackTrace()
        }
    }

            fun signInUser() = viewModelScope.launch {
        try {
            _loginEnable.value = false
            val user: FirebaseUser? = signInUseCase(_email.value!!, _pass.value!!)
            user?.let {
                val userPersisted = getUserByEmail(it.email!!)
                if(userPersisted!=null) {
                    userCache.name = userPersisted.name
                    userCache.age = userPersisted.age
                    userCache.email = userPersisted.email
                    userCache.id = userPersisted.id
                    userCache.location = userPersisted.location
                    userCache.password = userPersisted.password
                    userCache.isReferee = userPersisted.isReferee
                    userCache.teams = userPersisted.teams
                    userCache.photo = userPersisted.photo
                    insertUserRoom(userPersisted)
                    val teams = getUsersTeams(userPersisted.id)
                    myTeamsList.saveTeamsToCache(userPersisted.id, teams)
                    insertUsersTeamsRoom(teams)
                }
            }
            val parameters = Bundle().apply {
                this.putString("user_id", userCache.id)
                this.putString("user_name", userCache.name)
                this.putInt("user_age", userCache.age)
                this.putString("user_email", userCache.email)
                this.putString("isReferee", userCache.isReferee.toString())
                this.putString("user_locationName", userCache.locationName)

            }
            firebaseAnalytics.logEvent( "Sign_In", parameters)
            loginResult.value="Login success"
            _logged.postValue(true)
            _loginEnable.value= true
            signIn.value = true
        }catch(e:Exception){
            loginResult.value="Login failed, check your email and password"
            val error = e.toString().split(":").toTypedArray()
            Log.d("Login", "signInUser: ${error[1]}")
            e.printStackTrace()
        }
    }

    fun cleanLoginMessage(){
        loginResult.value=""
    }

    fun clearLoggedStatus() {
        _logged.value = false
    }

    fun addAnalytics(analytics: FirebaseAnalytics) {
        firebaseAnalytics = analytics
    }


}
