package com.uniandes.soccer.ui.view

import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Bottom
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.google.firebase.analytics.FirebaseAnalytics
import com.uniandes.soccer.R
import com.uniandes.soccer.data.model.match.Match
import com.uniandes.soccer.ui.view.Reusable.BottomNavBar
import com.uniandes.soccer.ui.view.Reusable.LoadingScreen
import com.uniandes.soccer.ui.viewModel.MatchesListViewModel
import java.text.SimpleDateFormat
import androidx.compose.runtime.*
import com.uniandes.soccer.ui.view.Reusable.ConnectionAlertDialog

private lateinit var toast1: Toast

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun MatchesList(
    navController: NavHostController,
    matchesListViewModel: MatchesListViewModel,
    analytics: FirebaseAnalytics,
    teamId: String?
) {

    LaunchedEffect(Unit){

        matchesListViewModel.onContentTextChange("Please connect to access to this service")
        if(!isConnected() ){
            matchesListViewModel.onOpenDialog()
        }
        if (teamId != null) {
            matchesListViewModel.setTeamId(teamId)
        }
        matchesListViewModel.onSearchBarChanged("")
        matchesListViewModel.onSelectChanged(true)
        matchesListViewModel.getJoinableMatches()
        matchesListViewModel.getMyMatches()
        matchesListViewModel.addAnalytics(analytics)
    }
    val search: String by matchesListViewModel.search.observeAsState(initial = "")
    val matchesList: List<Match> by matchesListViewModel.matchesList
    val myMatchesList: List<Match> by matchesListViewModel.myMatchesList
    val context = LocalContext.current
    val keyboardController = LocalSoftwareKeyboardController.current
    val selected: Boolean by matchesListViewModel.selected.observeAsState(initial = true)
    val isCharging: Boolean by matchesListViewModel.isCharging.observeAsState(initial = true)
    val focusRequester = remember { FocusRequester() }
    val focusManager = LocalFocusManager.current
    val contentTextDialog: String by matchesListViewModel.contentText.observeAsState(initial = "")

    val showDialogVisible: Boolean by matchesListViewModel.showDialog.collectAsState()

    Column(
        modifier = Modifier
            .background(Color.White)
            .fillMaxSize()
            .fillMaxHeight()
            .padding(bottom=55.dp)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = Color.White),
            ) { focusManager.clearFocus() },
        horizontalAlignment = CenterHorizontally
    ) {
        Surface(
            elevation= 3.dp
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(86.dp)
                    .focusRequester(focusRequester)
                    .padding(start= 10.dp, top=7.dp, bottom=7.dp)
            ) {
                TextField(
                    modifier = Modifier
                        .fillMaxWidth(0.8f)
                        .fillMaxHeight()
                        .padding(10.dp)
                        ,
                    singleLine = true,
                    value = search,
                    onValueChange = {
                        if (it.length <= 20) matchesListViewModel.onSearchBarChanged(it)
                        else {
                            try {
                                toast1.cancel()
                            } catch (e: Exception) {         // invisible if exception
                                toast1 = Toast.makeText(
                                    context,
                                    "Cannot be more than 20 Characters",
                                    Toast.LENGTH_SHORT
                                );
                            }
                            toast1.show();
                        }
                    },
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
                    keyboardActions = KeyboardActions(
                        onSearch = {
                            keyboardController?.hide(); matchesListViewModel.onSearchBarChanged(
                            search
                        )
                        }),
                    label = { Text(text = "Search") },
                    leadingIcon = {
                        Icon(Icons.Filled.Search, "")
                    },
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = Color(0xFFFCF0F0),
                        unfocusedIndicatorColor = Color.Transparent
                    ),
                    shape= RoundedCornerShape(8.dp)
                )

                Image(
                    painterResource(R.drawable.menu),
                    contentDescription = "",
                    contentScale = ContentScale.FillHeight,
                    modifier = Modifier
                        .fillMaxHeight(0.8f)
                        .padding(top = 11.dp, start = 12.dp)
                        .clickable(
                            enabled = true,
                            onClickLabel = "Clickable image",
                            onClick = {
                                navController.navigate("mainMenu")
                            }
                        ),


                    )

            }
        }

        Spacer(Modifier.height(25.0.dp))
        Column(
            modifier = Modifier,
            verticalArrangement = Arrangement.Center
        ){

            Row(
                modifier = Modifier
                    .padding(top = 5.0.dp, start = 10.0.dp, end = 10.0.dp)
                    .height(63.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                var modifierTrue: Modifier
                if(selected) {
                    modifierTrue = Modifier
                        .drawBehind {
                            val strokeWidth = Stroke.DefaultMiter
                            val y = size.height - strokeWidth / 2
                            drawLine(
                                Color(0xFF000000),
                                Offset(0f, y),
                                Offset(size.width, y),
                                strokeWidth
                            )
                        }
                        .weight(1f)
                } else{
                    modifierTrue = Modifier.weight(1f)
                }

                Row(
                    modifier = modifierTrue
                ) {
                    Text(
                        text = "Matches to join",
                        fontSize = 20.sp,
                        color = Color(0xFF000000),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 5.dp)
                            .clickable(
                                interactionSource = remember { MutableInteractionSource() },
                                indication = rememberRipple(color = Color.Red),
                            ) { matchesListViewModel.onSelectChanged(true); matchesListViewModel.refresh()
                                if(!isConnected() && selected){
                                    matchesListViewModel.onContentTextChange("Please connect to access to this service")
                                    matchesListViewModel.onOpenDialog()
                                } else {
                                    matchesListViewModel.getJoinableMatches()
                                }
                              },
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.Bold
                    )
                }
                Spacer(Modifier.width(6.0.dp))
                var modifierFalse: Modifier
                if(!selected) {
                    modifierFalse = Modifier
                        .drawBehind {
                            val strokeWidth = Stroke.DefaultMiter
                            val y = size.height - strokeWidth / 2
                            drawLine(
                                Color(0xFF000000),
                                Offset(0f, y),
                                Offset(size.width, y),
                                strokeWidth
                            )
                        }
                        .weight(1f)
                } else{
                    modifierFalse = Modifier.weight(1f)
                }
                Row(modifier = modifierFalse
                ) {
                    Text(
                        text = "My matches",
                        fontSize = 20.sp,
                        color = Color(0xFF000000),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 5.dp)
                            .clickable(
                                interactionSource = remember { MutableInteractionSource() },
                                indication = rememberRipple(color = Color.Red),
                            ) { matchesListViewModel.onSelectChanged(false);matchesListViewModel.refresh()
                                if(!isConnected() && !selected){
                                    matchesListViewModel.onContentTextChange("This list is outdated, please connect to update it")
                                    matchesListViewModel.onOpenDialog()
                                }
                                matchesListViewModel.getMyMatches()

                              },
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.Bold

                    )
                }

            }
        }

        if (!isCharging) {
            LazyColumn(
                modifier = Modifier
            ) {

                if (selected ) {
                    items(matchesList) { match ->

                        matchItem(match = match, navController = navController, selected)

                    }
                } else {
                    items(myMatchesList) { match ->

                        matchItem(match = match, navController = navController, selected)

                    }
                }

            }
        } else {
            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .padding(bottom = 20.dp),
                verticalArrangement = Arrangement.Center
            ) {
                LoadingScreen()
            }
        }


    }

    Column(modifier = Modifier
        .fillMaxSize()
        .padding(bottom = 70.dp, end = 20.dp),
        verticalArrangement = Arrangement.Bottom,
        horizontalAlignment = Alignment.End
    ) {
        FloatingActionButton(
            onClick = { navController.navigate("createMatch/teamId=${teamId}") },
            modifier = Modifier
                .size(60.dp),
            backgroundColor = Color(0xFFFFE5E5),

            elevation = FloatingActionButtonDefaults.elevation(3.dp)

            ) {
            Icon(modifier=Modifier
                .fillMaxSize(0.6F),
                imageVector = Icons.Default.Add,
                contentDescription = "Create match",
                tint = Color(0xFF950000),
            )
        }
    }

    ConnectionAlertDialog(
        title = "Connection failure",
        contentText=contentTextDialog,
        show = showDialogVisible,
        onDismiss = matchesListViewModel::onDialogDismiss,
        onConfirm = matchesListViewModel::onDialogConfirm
    )

    Column(modifier = Modifier
        .fillMaxSize(),
        verticalArrangement = Arrangement.Bottom,
    ) {
        BottomNavBar(modifier = Modifier.weight(1f), navController)
    }



}



@Composable
fun matchItem(match: Match, navController: NavHostController, selected: Boolean){
    Row(modifier = Modifier
        .border(BorderStroke(1.dp, Color(0xFFD1D1D1)), shape = RoundedCornerShape(5.dp))
        .padding(5.0.dp)
        .height(155.dp)
        .fillMaxWidth(0.9f)
        .clickable { navController.navigate("matchDetail/${match.locationName +"|"+match.dateMatch.toString()+"|"+match.referee}") },
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center)
    {
        Column(modifier = Modifier
            .weight(3f)
            .fillMaxHeight(),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center) {
            if (selected) {
                Row(
                    modifier = Modifier
                        .padding(start = 5.dp)
                        .fillMaxHeight(0.3f)
                        .fillMaxWidth(),
                    verticalAlignment = Bottom
                ) {
                    Image(
                        painterResource(R.drawable.mapa),
                        contentDescription = "",
                        contentScale = ContentScale.FillHeight,
                        modifier = Modifier
                            .fillMaxWidth()
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(0.6f)
                        .padding(bottom = 5.dp, top = 12.dp),
                    verticalAlignment = Alignment.Top
                ) {
                    Text(
                        text = match.locationName,
                        fontSize = 13.sp,
                        color = Color(0xFF000000),
                        modifier = Modifier
                            .fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )
                }
            } else{
                Row(
                    modifier = Modifier
                        .padding(start = 5.dp)
                        .fillMaxHeight(0.4f)
                        .fillMaxWidth(),
                    verticalAlignment = Bottom,
                    horizontalArrangement = Arrangement.Center

                ) {
                    Image(
                        painterResource(R.drawable.icono1),
                        contentDescription = "",
                        contentScale = ContentScale.FillHeight,
                        modifier = Modifier
                            .fillMaxWidth()
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(0.6f)
                        .padding(bottom = 5.dp, top = 5.dp),
                    verticalAlignment = Alignment.Top,
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = match.team1.name,
                        fontSize = 17.sp,
                        color = Color(0xFF000000),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 5.dp),
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.Bold
                    )
                }
            }
        }

        Column(modifier = Modifier.weight(3.2f),
            horizontalAlignment = CenterHorizontally,
            verticalArrangement = Arrangement.Center) {
            Text(
                text = "${SimpleDateFormat("yyyy/MM/dd hh:mm a").format(match.dateMatch.toDate())}",
                fontSize = 15.sp,
                color = Color.Black,
                modifier= Modifier
                    .fillMaxWidth(),
                textAlign = TextAlign.Center,
            )
        }
        if(selected || match.team2!=null) {
            Column(
                modifier = Modifier
                    .weight(0.8f),
                horizontalAlignment = Alignment.End,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "vs.",
                    fontSize = 17.sp,
                    color = Color.Black,
                    modifier = Modifier
                        .fillMaxWidth(),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold
                )
            }
        }

        Column(modifier = Modifier
            .weight(3f)
            .fillMaxHeight(),
            horizontalAlignment = Alignment.End,
            verticalArrangement = Arrangement.Center) {
            if (!selected && match.team2==null) {
                Row(
                    modifier = Modifier
                        .padding(start = 5.dp)
                        .fillMaxHeight(0.3f)
                        .fillMaxWidth(),
                    verticalAlignment = Bottom
                ) {
                    Image(
                        painterResource(R.drawable.mapa),
                        contentDescription = "",
                        contentScale = ContentScale.FillHeight,
                        modifier = Modifier
                            .fillMaxWidth()
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(0.6f)
                        .padding(bottom = 5.dp, top = 12.dp),
                    verticalAlignment = Alignment.Top
                ) {
                    Text(
                        text = match.locationName,
                        fontSize = 13.sp,
                        color = Color(0xFF000000),
                        modifier = Modifier
                            .fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )
                }
            } else {
                Row(
                    modifier = Modifier
                        .padding(start = 5.dp)
                        .fillMaxHeight(0.4f)
                        .fillMaxWidth(),
                    verticalAlignment = Bottom,
                    horizontalArrangement = Arrangement.Center

                ) {
                    if (selected) {

                        Image(
                            painterResource(R.drawable.icono2),
                            contentDescription = "",
                            contentScale = ContentScale.FillHeight,
                            modifier = Modifier
                                .fillMaxWidth(),
                            )
                    } else {
                        Image(
                            painterResource(R.drawable.icono2),
                            contentDescription = "",
                            contentScale = ContentScale.FillHeight,
                            modifier = Modifier
                                .fillMaxWidth(),
                        )
                    }
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(0.6f)
                        .padding(bottom = 5.dp, top = 5.dp),
                    verticalAlignment = Alignment.Top,
                    horizontalArrangement = Arrangement.Center
                ) {
                    if (selected) {
                        Text(
                            text = match.team1.name,
                            fontSize = 17.sp,
                            color = Color(0xFF000000),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(bottom = 5.dp),
                            textAlign = TextAlign.Center,
                            fontWeight = FontWeight.Bold
                        )
                    } else {
                        match.team2?.let {
                            Text(
                                text = it.name,
                                fontSize = 17.sp,
                                color = Color(0xFF000000),
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(bottom = 5.dp),
                                textAlign = TextAlign.Center,
                                fontWeight = FontWeight.Bold
                            )
                        }
                    }
                }
            }
        }

    }
    Spacer(Modifier.height(12.0.dp))
}
