package com.uniandes.soccer.ui.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.uniandes.soccer.ui.maps.LocationLiveData

class LocationViewModel(application:Application) : AndroidViewModel(application) {
    private val locationLiveData = LocationLiveData(application)
    fun getLocationLiveDate() = locationLiveData

    internal fun startLocationUpdates(){
        locationLiveData.startLocationUpdates()
    }
}