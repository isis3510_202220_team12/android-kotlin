package com.uniandes.soccer.ui.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.uniandes.soccer.R
import com.uniandes.soccer.ui.view.Reusable.BottomNavBar
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun matchDetail(
    navController: NavHostController,
    matchInfo: String?
){
    var date:Long = 0L
    var referee = ""
    var location = ""
    var temp = ""
    var temp2 = ""
    try{
        var list = matchInfo?.split("|")
        if (list != null) {
            location = list[0]
            referee = list[2]
            temp = list[1].split("=")[1]
            temp2 = temp.split(",")[0]
            date = temp2.toLong()*1000
        }
    }
    catch (e:Exception){
    }

    val df: DateFormat = SimpleDateFormat("yyyy/MM/dd hh:mm a")

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Column(
            modifier = Modifier
                .background(Color.White)
                .verticalScroll(rememberScrollState())
                .weight(weight = 1f, fill = false),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Spacer(Modifier.height(22.0.dp))
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = 22.dp)
                    .height(50.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.Top
            ) {
                Spacer(modifier = Modifier.weight(1f))
                Column(horizontalAlignment = Alignment.End) {
                    Image(
                        painterResource(R.drawable.menu),
                        contentDescription = "",
                        contentScale = ContentScale.FillHeight,
                        modifier = Modifier
                            .fillMaxHeight()
                    )
                }

            }
            Image(
                painterResource(R.drawable.fubol),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.size(width = 239.dp, height = 144.dp)
            )
            Spacer(Modifier.height(19.0.dp))
            Text(
                text = location,                                    //Reemplazar por name
                fontSize = 18.0.sp,
                fontWeight = FontWeight.Bold,
                color = Color(0xFF121C2B)
            )
            Spacer(Modifier.height(18.0.dp))
            Divider(
                color = Color(0xFFFF524A), thickness = 2.dp,
                modifier = Modifier
                    .padding(end = 40.0.dp, start = 40.0.dp)
                    .background(Color(0xFFFF524A), RoundedCornerShape(16.dp))
            )
            Spacer(Modifier.height(28.0.dp))
            Text(
                text = "date and time",
                fontSize = 16.0.sp,
                fontWeight = FontWeight.Bold,
                color = Color(0xFF121C2B)
            )
            Text(
                text = df.format(date),                         //Reemplazar por desc
                fontSize = 14.0.sp,
                color = Color(0xFF121C2B),
                modifier = Modifier
                    .padding(start = 40.dp, end = 40.dp)
                    .background(color = Color(0xFFFFE5E5), RoundedCornerShape(10.dp))
                    .fillMaxWidth()
                    .height(30.dp)
                    .padding(5.dp)
            )
            Spacer(Modifier.height(16.0.dp))
            Text(
                text = "Referee",
                fontSize = 16.0.sp,
                fontWeight = FontWeight.Bold,
                color = Color(0xFF121C2B)
            )
            Text(
                text = referee,                         //Reemplazar por desc
                fontSize = 14.0.sp,
                color = Color(0xFF121C2B),
                modifier = Modifier
                    .padding(start = 40.dp, end = 40.dp)
                    .background(color = Color(0xFFFFE5E5), RoundedCornerShape(10.dp))
                    .fillMaxWidth()
                    .height(30.dp)
                    .padding(5.dp)
            )
            Spacer(Modifier.height(26.0.dp))
            Button(
                onClick = {
                    /*
                    navController.popBackStack()
                    */
                },
                shape = RoundedCornerShape(50),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0XFF003893),
                    contentColor = Color(0xFFFFFBFF)
                ),
                modifier = Modifier.width(250.dp)

            ) {
                Text(
                    text = "show location in map",
                    fontSize = 20.sp,
                )
            }
        }
        BottomNavBar(modifier = Modifier.padding(top = 0.dp), navController)
    }
}