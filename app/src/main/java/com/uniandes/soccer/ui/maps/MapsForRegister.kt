package com.uniandes.soccer.ui.maps

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavHostController
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.MapView
import com.google.android.libraries.maps.model.LatLng
import com.uniandes.soccer.ui.viewModel.CreateMatchViewModel
import com.uniandes.soccer.ui.viewModel.RegisterViewModel

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun MapAddressPickerViewForRegister(navController: NavHostController, registerViewModel: RegisterViewModel){
    Surface(color = MaterialTheme.colors.background) {
        val mapView = rememberMapViewWithLifecycle()
        val currentLocation = registerViewModel.location.collectAsState()
        var text by remember { registerViewModel.addressText }
        val context = LocalContext.current
        val keyboardController = LocalSoftwareKeyboardController.current


        Column(Modifier.fillMaxWidth()) {

            Box{
                TextField(
                    value = text,
                    onValueChange = {
                        text = it
                        if(!registerViewModel.isMapEditable.value)
                            registerViewModel.onTextChanged(context, text)
                    },
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                    keyboardActions = KeyboardActions(
                        onDone = {keyboardController?.hide()}),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(end = 80.dp),
                    enabled = !registerViewModel.isMapEditable.value,
                    colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.Transparent)
                )

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp)
                        .padding(bottom = 20.dp),
                    horizontalAlignment = Alignment.End
                ){
                    Button(
                        onClick = {
                            registerViewModel.isMapEditable.value = !registerViewModel.isMapEditable.value
                        }
                    ) {
                        Text(text = if(registerViewModel.isMapEditable.value) "Edit" else "Save")
                    }
                }
            }

            Box(modifier = Modifier.fillMaxHeight()){

                currentLocation.value.let {
                    if(registerViewModel.isMapEditable.value) {
                        try {
                            text = registerViewModel.getAddressFromLocation(context)
                        } catch (e: Exception) {
                            Toast.makeText(
                                context,
                                "There is not internet connection",
                                Toast.LENGTH_SHORT
                            ).show()
                            navController.navigate("createMatch")
                        }
                    }
                    MapViewContainerForRegister(registerViewModel.isMapEditable.value, mapView, registerViewModel, navController)
                }

                MapPinOverlay()
            }
        }
    }
}


@Composable
private fun MapViewContainerForRegister(
    isEnabled: Boolean,
    mapView: MapView,
    registerViewModel: RegisterViewModel,
    navController: NavHostController
) {
    val context = LocalContext.current
    AndroidView(
        factory = { mapView }
    ) {
        try {

            mapView.getMapAsync { map ->

                map.uiSettings.setAllGesturesEnabled(isEnabled)

                val location = registerViewModel.location.value
                val position = LatLng(location.latitude, location.longitude)
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(position,  15f))

                map.setOnCameraIdleListener {
                    val cameraPosition = map.cameraPosition
                    registerViewModel.updateLocation(cameraPosition.target.latitude, cameraPosition.target.longitude)
                }
            }

        } catch (e: Exception) {
            Toast.makeText(
                context,
                "There is not internet connection",
                Toast.LENGTH_SHORT
            ).show()
            navController.navigate("createMatch")
        }
    }

}
