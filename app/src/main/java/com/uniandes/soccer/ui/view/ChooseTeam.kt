package com.uniandes.soccer.ui.view

import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.ui.view.Reusable.BottomNavBar
import com.uniandes.soccer.ui.view.Reusable.ConnectionAlertDialog
import com.uniandes.soccer.ui.view.Reusable.LoadingScreen
import com.uniandes.soccer.ui.view.Reusable.TeamItem
import com.uniandes.soccer.ui.viewModel.ChooseTeamViewModel


@Composable
fun ChooseTeam(navController: NavHostController, chooseTeamListViewModel: ChooseTeamViewModel) {
    LaunchedEffect(Unit) {
        if(!isConnected() ){
            chooseTeamListViewModel.onOpenDialog()
        }
        chooseTeamListViewModel.getTeamsByUserId()
    }
    val myTeamsList: List<Team> by chooseTeamListViewModel.myTeamsList
    val isCharging: Boolean by chooseTeamListViewModel.isCharging.observeAsState(initial = true)
    val showDialogVisible: Boolean by chooseTeamListViewModel.showDialog.collectAsState()


    Column(
        modifier = Modifier
            .background(Color.White)
            .fillMaxSize()
            .fillMaxHeight(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Spacer(Modifier.height(45.0.dp))
        Text(
            text = "Select one of your teams to continue:",
            fontSize = 20.sp,
            color = Color(0xFF000000),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 10.dp),
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold
        )

        Spacer(Modifier.height(15.0.dp))


        if (!isCharging) {
                LazyColumn() {

                    items(myTeamsList) { team ->
                        var modifier = Modifier
                            .border(
                                BorderStroke(1.dp, Color(0xFFD1D1D1)),
                                shape = RoundedCornerShape(5.dp)
                            )
                            .padding(start = 5.0.dp, end = 5.0.dp)
                            .height(60.dp)
                            .fillMaxWidth(0.9f)
                        TeamItem(navController, modifier = modifier, team = team)

                    }

                }
            } else {
                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .padding(bottom = 20.dp),
                    verticalArrangement = Arrangement.Center
                ) {
                    LoadingScreen()
                }
            }


        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(bottom = 70.dp, end = 20.dp),
            verticalArrangement = Arrangement.Bottom,
            horizontalAlignment = Alignment.End
        ) {
            FloatingActionButton(
                onClick = { navController.navigate("createTeam") },
                modifier = Modifier
                    .size(60.dp),
                backgroundColor = Color(0xFFFFE5E5),

                elevation = FloatingActionButtonDefaults.elevation(3.dp)

            ) {
                Icon(
                    modifier = Modifier
                        .fillMaxSize(0.6F),
                    imageVector = Icons.Default.Add,
                    contentDescription = "Create team",
                    tint = Color(0xFF950000),
                )
            }
        }
    }

    ConnectionAlertDialog(
        title = "Connection failure",
        contentText= "This list is outdated, please connect to update it",
        show = showDialogVisible,
        onDismiss = chooseTeamListViewModel::onDialogDismiss,
        onConfirm = chooseTeamListViewModel::onDialogConfirm
    )

    Column(modifier = Modifier
        .fillMaxSize(),
        verticalArrangement = Arrangement.Bottom,
    ) {
        BottomNavBar(modifier = Modifier.weight(1f), navController)
    }
}



