package com.uniandes.soccer.ui.view

import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.google.firebase.analytics.FirebaseAnalytics
import com.uniandes.soccer.R
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.ui.view.Reusable.BottomNavBar
import com.uniandes.soccer.ui.view.Reusable.ConnectionAlertDialog
import com.uniandes.soccer.ui.view.Reusable.LoadingScreen
import com.uniandes.soccer.ui.viewModel.ChooseRefereeViewModel


private lateinit var toast1: Toast

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ChooseReferee(
    navController: NavHostController, chooseRefereeViewModel: ChooseRefereeViewModel,
    analytics: FirebaseAnalytics, lat: Float?, lng: Float?
) {

    LaunchedEffect(Unit) {
        chooseRefereeViewModel.addAnalytics(analytics)
        if (navController.currentBackStackEntry!!.savedStateHandle.contains("refereeId")) {
            navController.previousBackStackEntry
                ?.savedStateHandle
                ?.set("refereeId", navController.currentBackStackEntry!!.savedStateHandle.get<String>(
                    "refereeId"
                ) ?: "")

            navController.popBackStack()
        }
        if (lat != null && lng != null) {
            chooseRefereeViewModel.setLocation(lat,lng)
        }
        chooseRefereeViewModel.getReferees()
        chooseRefereeViewModel.onSearchBarChanged("")
    }

    val search: String by chooseRefereeViewModel.search.observeAsState(initial = "")
    val refereesList: List<User> by chooseRefereeViewModel.refereesList
    val context = LocalContext.current
    val keyboardController = LocalSoftwareKeyboardController.current
    val isCharging: Boolean by chooseRefereeViewModel.isCharging.observeAsState(initial = true)
    val focusRequester = remember { FocusRequester() }
    val focusManager = LocalFocusManager.current
    val contentTextDialog: String by chooseRefereeViewModel._contentText
    val showDialogVisible: Boolean by chooseRefereeViewModel.showDialog



    Column(
        modifier = Modifier
            .background(Color.White)
            .fillMaxSize()
            .fillMaxHeight()
            .padding(bottom=55.dp)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = Color.White),
            ) { focusManager.clearFocus() },
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Surface(
            elevation= 3.dp
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(86.dp)
                    .focusRequester(focusRequester)
                    .padding(start = 10.dp, top = 7.dp, bottom = 7.dp)
            ) {
                TextField(
                    modifier = Modifier
                        .fillMaxWidth(0.8f)
                        .fillMaxHeight()
                        .padding(10.dp),
                    singleLine = true,
                    value = search,
                    onValueChange = {
                        if (it.length <= 20) chooseRefereeViewModel.onSearchBarChanged(it)
                        else {
                            try {
                                toast1.cancel()
                            } catch (e: Exception) {         // invisible if exception
                                toast1 = Toast.makeText(
                                    context,
                                    "Cannot be more than 20 Characters",
                                    Toast.LENGTH_SHORT
                                );
                            }
                            toast1.show();
                        }
                    },
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
                    keyboardActions = KeyboardActions(
                        onSearch = {
                            keyboardController?.hide(); chooseRefereeViewModel.onSearchBarChanged(
                            search
                        )
                        }),
                    label = { Text(text = "Search") },
                    leadingIcon = {
                        Icon(Icons.Filled.Search, "")
                    },
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = Color(0xFFFCF0F0),
                        unfocusedIndicatorColor = Color.Transparent
                    ),
                    shape = RoundedCornerShape(8.dp)
                )

                Image(
                    painterResource(R.drawable.menu),
                    contentDescription = "",
                    contentScale = ContentScale.FillHeight,
                    modifier = Modifier
                        .fillMaxHeight(0.8f)
                        .padding(top = 11.dp, start = 12.dp)
                        .clickable(
                            enabled = true,
                            onClickLabel = "Clickable image",
                            onClick = {
                                navController.navigate("mainMenu")
                            }
                        ),


                    )

            }
        }

        Spacer(Modifier.height(10.0.dp))
        if(isConnected()) {
            if (!isCharging) {
                LazyColumn(
                    modifier = Modifier
                ) {
                    items(refereesList) { referee ->
                        chooseRefereeItem(referee = referee, navController = navController)
                    }
                }
            } else {
                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .padding(bottom = 20.dp),
                    verticalArrangement = Arrangement.Center
                ) {
                    LoadingScreen()
                }
            }
        } else {
            try {
                toast1.cancel()
                toast1 = Toast.makeText(
                    context,
                    "There is not internet connection",
                    Toast.LENGTH_SHORT
                )
            } catch (e: Exception) {
                toast1 = Toast.makeText(
                    context,
                    "There is not internet connection",
                    Toast.LENGTH_SHORT
                )
            }
            toast1.show()
        }

    }

    ConnectionAlertDialog(
        title = "Referee List",
        contentText=contentTextDialog,
        show = showDialogVisible,
        onDismiss = chooseRefereeViewModel::onDialogDismiss,
        onConfirm = chooseRefereeViewModel::onDialogConfirm
    )

    Column(modifier = Modifier
        .fillMaxSize(),
        verticalArrangement = Arrangement.Bottom,
    ) {
        BottomNavBar(modifier = Modifier.weight(1f), navController)
    }


}




@Composable
fun chooseRefereeItem(referee: User, navController: NavHostController){
    Row(modifier = Modifier
        .border(BorderStroke(1.dp, Color(0xFFD1D1D1)), shape = RoundedCornerShape(5.dp))
        .padding(top = 5.0.dp, start = 5.0.dp, end = 5.0.dp)
        .height(60.dp)
        .fillMaxWidth(0.9f),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center)
    {
        Column(modifier = Modifier
            .weight(1f)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = Color.Red),
            ) { navController.navigate("refereeDetail/${referee.id}")},
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center){
            Image(
                painterResource(R.drawable.referee),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .padding(start = 5.dp)
                    .fillMaxHeight(0.9f)
            )
        }

        Column(modifier = Modifier
            .weight(1f)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = Color.Red),
            ) {
                navController.navigate("refereeDetail/${referee.id}")
              },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center) {
            Text(
                text = "${referee.name}",
                fontSize = 18.sp,
                color = Color.Black,
                modifier= Modifier
                    .fillMaxWidth(),
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold
            )
        }

        Column(modifier = Modifier.weight(1f),
            horizontalAlignment = Alignment.End,
            verticalArrangement = Arrangement.Center) {
            Image(
                painterResource(R.drawable.plus),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .padding(end = 5.dp)
                    .fillMaxHeight(0.8f)
                    .clickable {  navController.previousBackStackEntry
                        ?.savedStateHandle
                        ?.set("refereeId", referee.id)

                        navController.popBackStack()
                    }
            )
        }

    }
    Spacer(Modifier.height(12.0.dp))
}