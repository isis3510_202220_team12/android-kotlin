package com.uniandes.soccer.ui.viewModel.utilities

interface Tokenizer {
    fun split(text: String): List<String>

    fun batchSplit(texts: List<String>): List<List<String>> = texts.map(this::split)
}