package com.uniandes.soccer.ui.viewModel.utilities

import android.location.Location
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.match.Match
import javax.inject.Inject

class LocationComparatorMatch @Inject constructor(
): Comparator<Match>{
    private var actualLocation: GeoPoint = GeoPoint(0.0,0.0)
    override fun compare(m1: Match?, m2: Match?): Int {
        if (m1==null||m2==null){
            return 0
        }else {
            var dis1 = FloatArray(1)
            var dis2 = FloatArray(1)
            Location.distanceBetween(
                actualLocation.latitude, actualLocation.longitude,
                m1.location.latitude, m1.location.longitude, dis1
            )
            Location.distanceBetween(
                actualLocation.latitude, actualLocation.longitude,
                m2.location.latitude, m2.location.longitude, dis2
            )
            return dis1[0].compareTo(dis2[0])
        }
    }

    fun setActualLocation(actualLocation: GeoPoint){
        this.actualLocation = actualLocation
    }

}