package com.uniandes.soccer

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController

@Composable
fun Test(navController: NavHostController) {
    Column()
    {
        Button(
            onClick = { navController.navigate("main") },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF4D61C4),
                contentColor = Color(0xFFFFFBFF)
            )
            //colors = ButtonDefaults.outlinedButtonColors(contentColor = Color(0xFF4D61C4))

        ) {
            Text(
                text = "main",
                fontSize = 20.sp,
            )
        }
        Button(
            onClick = { navController.navigate("referee") },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF4D61C4),
                contentColor = Color(0xFFFFFBFF)
            )
            //colors = ButtonDefaults.outlinedButtonColors(contentColor = Color(0xFF4D61C4))

        ) {
            Text(
                text = "referee",
                fontSize = 20.sp,
            )
        }
        Button(
            onClick = { navController.navigate("createTeam") },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF4D61C4),
                contentColor = Color(0xFFFFFBFF)
            )
            //colors = ButtonDefaults.outlinedButtonColors(contentColor = Color(0xFF4D61C4))

        ) {
            Text(
                text = "createTeam",
                fontSize = 20.sp,
            )
        }
        Button(
            onClick = { navController.navigate("teamList") },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF4D61C4),
                contentColor = Color(0xFFFFFBFF)
            )
            //colors = ButtonDefaults.outlinedButtonColors(contentColor = Color(0xFF4D61C4))

        ) {
            Text(
                text = "teamList",
                fontSize = 20.sp,
            )
        }
        Button(
            onClick = { navController.navigate("refereeDetail") },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF4D61C4),
                contentColor = Color(0xFFFFFBFF)
            )
            //colors = ButtonDefaults.outlinedButtonColors(contentColor = Color(0xFF4D61C4))

        ) {
            Text(
                text = "refereeDetail",
                fontSize = 20.sp,
            )
        }
        Button(
            onClick = { navController.navigate("playerDetail") },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF4D61C4),
                contentColor = Color(0xFFFFFBFF)
            )
            //colors = ButtonDefaults.outlinedButtonColors(contentColor = Color(0xFF4D61C4))

        ) {
            Text(
                text = "playerDetail",
                fontSize = 20.sp,
            )
        }
        Button(
            onClick = { navController.navigate("createMatch") },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF4D61C4),
                contentColor = Color(0xFFFFFBFF)
            )
            //colors = ButtonDefaults.outlinedButtonColors(contentColor = Color(0xFF4D61C4))

        ) {
            Text(
                text = "createMatch",
                fontSize = 20.sp,
            )
        }
        Button(
            onClick = { navController.navigate("register") },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF4D61C4),
                contentColor = Color(0xFFFFFBFF)
            )
            //colors = ButtonDefaults.outlinedButtonColors(contentColor = Color(0xFF4D61C4))

        ) {
            Text(
                text = "register",
                fontSize = 20.sp,
            )
        }
    }
}
