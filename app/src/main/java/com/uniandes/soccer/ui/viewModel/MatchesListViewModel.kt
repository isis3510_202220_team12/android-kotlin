package com.uniandes.soccer.ui.viewModel

import android.os.Bundle
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.*
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.ktx.Firebase
import com.uniandes.soccer.data.cache.MyMatchesList
import com.uniandes.soccer.data.cache.UserCache
import com.uniandes.soccer.data.model.match.Match
import com.uniandes.soccer.domain.match.GetJoinableMatches
import com.uniandes.soccer.domain.match.GetTeamsMatches
import com.uniandes.soccer.ui.view.isConnected
import com.uniandes.soccer.ui.viewModel.utilities.LocationComparator
import com.uniandes.soccer.ui.viewModel.utilities.LocationComparatorMatch
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import org.koin.core.parameter.parametersOf
import java.text.SimpleDateFormat
import java.util.ArrayList
import javax.inject.Inject

@HiltViewModel
class MatchesListViewModel @Inject constructor(
    private val myTeamMatchesUseCase: GetTeamsMatches,
    private val joinableMatchesUseCase: GetJoinableMatches,
    private val locationComparatorMatch: LocationComparatorMatch,
    private val userCache: UserCache,
    private val myMatches: MyMatchesList
    ):ViewModel(){
    lateinit var firebaseAnalytics: FirebaseAnalytics

    val myMatchesList: MutableState<List<Match>> = mutableStateOf(listOf())
    private val _myInitialList: MutableState<List<Match>> = mutableStateOf(listOf())
    private val _selected = MutableLiveData<Boolean>()
    val selected: LiveData<Boolean> = _selected
    private val _isCharging = MutableLiveData<Boolean>(true)
    val isCharging: LiveData<Boolean> = _isCharging
    private val _search = MutableLiveData <String>()
    val search : LiveData<String> = _search
    private val _initialList: MutableState<List<Match>> = mutableStateOf(listOf())
    val matchesList:  MutableState<List<Match>> = mutableStateOf(listOf())
    private val _contentText = MutableLiveData <String>()
    val contentText : LiveData<String> = _contentText
    private val _showDialog = MutableStateFlow(false)
    val showDialog: StateFlow<Boolean> = _showDialog.asStateFlow()
    private val _teamId = MutableLiveData<String>()
    val teamId: LiveData<String> = _teamId

    init {
        _selected.value = true
    }
    fun getJoinableMatches(){
        viewModelScope.launch {

            try {
                val matches = _teamId.value?.let { joinableMatchesUseCase(it) }
                locationComparatorMatch.setActualLocation(userCache.location)
                if (matches != null) {
                    _initialList.value =
                        matches.sortedWith(locationComparatorMatch.thenBy { it.dateMatch })
                }
                matchesList.value = _initialList.value
            } catch (e: Exception) {
                val error = e.toString().split(":").toTypedArray()
                Log.d("Matches list", "get the matches list failed: ${error[1]}")
                e.printStackTrace()
            }
            _isCharging.value = false
        }
    }

    fun getMyMatches() {
        viewModelScope.launch {
            val parameters = Bundle().apply {
                this.putString("user_id", userCache.id)
                this.putString("user_name", userCache.name)
                this.putString("team_id", _teamId.value)
            }
            if (isConnected()) {
                try {
                    val matches = _teamId.value?.let { myTeamMatchesUseCase(it) }
                    var m = ""
                    if (matches != null) {
                        myMatches.saveMatchToCache(userCache.id+"-"+_teamId.value, matches)
                        _myInitialList.value =
                            matches.sortedWith(locationComparatorMatch.thenBy { it.dateMatch })
                    }
                    if (myMatchesList.value.isNotEmpty()) {
                        val a = myMatchesList.value[0]
                        m = "Id: " + a.id + " / Date: " +
                                SimpleDateFormat("yyyy/MM/dd hh:mm a").format(a.dateMatch.toDate()) +
                                " / Rival_name: " + (a.team2?.name ?: "")
                        parameters.putString("team_name", a.team1.name)
                    }
                    parameters.putString("match", m)
                    firebaseAnalytics.logEvent("Closest_match", parameters)

                    myMatchesList.value = _myInitialList.value
                } catch (e: Exception) {
                    val error = e.toString().split(":").toTypedArray()
                    Log.d("My matches list", "get the team's matches list failed: ${error[1]}")
                }
            } else {
                val matches=myMatches.retrieveMatchesFromCache(userCache.id+"-"+_teamId.value)
                if(matches != null){
                    _myInitialList.value =
                        matches.sortedWith(locationComparatorMatch.thenBy { it.dateMatch })
                } else {
                    _myInitialList.value = listOf()
                    myMatchesList.value = listOf()
                }
            }
            _isCharging.value = false
        }
    }

    fun setTeamId(teamId: String){
        _teamId.value = teamId
    }

    fun onSearchBarChanged(search:String) {

        _search.value = search
        if(_selected.value == true){
            searchMatch(search)
        }else if(_selected.value == false){
            searchMyMatch(search)
        }
    }

    fun onSelectChanged(select:Boolean) {

        _selected.value = select
    }

    private fun searchMatch(search:String){
        matchesList.value = _initialList.value.filter {
            it.team1.name.contains(search, ignoreCase = true) ||
                    search.contains(it.team1.name, ignoreCase = true) ||
                    it.locationName.contains(search, ignoreCase = true) ||
                    search.contains(it.locationName, ignoreCase = true)
        }
    }

    fun onOpenDialog() {
        _showDialog.value = true
    }

    fun onDialogConfirm() {
        _showDialog.value = false
    }

    fun onContentTextChange(text: String) {
        _contentText.value = text
    }

    fun onDialogDismiss() {
        _showDialog.value = false
    }


    fun refresh(){
        _search.value = ""
        myMatchesList.value=_myInitialList.value
        matchesList.value=_initialList.value
    }

    private fun searchMyMatch(search:String){
        myMatchesList.value = _myInitialList.value.filter {
            it.team1.name.contains(search, ignoreCase = true) ||
                    search.contains(it.team1.name, ignoreCase = true) ||
                    it.locationName.contains(search, ignoreCase = true) ||
                    search.contains(it.locationName, ignoreCase = true) ||
                    it.team2?.let { it1 -> search.contains(it1.name, ignoreCase = true) } == true ||
                    it.team2?.name?.contains(search, ignoreCase = true) ?: true
        }
    }

    fun addAnalytics(analytics: FirebaseAnalytics) {
        firebaseAnalytics = analytics
    }





}