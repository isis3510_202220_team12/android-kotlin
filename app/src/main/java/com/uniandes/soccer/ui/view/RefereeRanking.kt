package com.uniandes.soccer.ui.view

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.uniandes.soccer.R
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import com.uniandes.soccer.ui.viewModel.RefereeRankingViewModel

private lateinit var toast1: Toast

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun RefereeRank(
    navController: NavHostController,
    refereeRankingViewModel: RefereeRankingViewModel
) {
    val search: String by refereeRankingViewModel.search.observeAsState(initial = "")
    val context = LocalContext.current
    val keyboardController = LocalSoftwareKeyboardController.current

    Column(
        modifier = Modifier
            .background(Color.White)
            .fillMaxSize().padding(top = 10.0.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        Row(
            modifier = Modifier
                .background(Color.White),
            horizontalArrangement = Arrangement.Center
        ){
            Spacer(Modifier.width(5.0.dp))
            TextField(
                value = search,
                onValueChange = {
                    if (it.length <= 20) refereeRankingViewModel.onRefereeRankingChanged(it)
                    else {
                        try{
                            toast1.cancel()
                        } catch (e:Exception) {         // invisible if exception
                            toast1 = Toast.makeText(context, "Cannot be more than 20 Characters", Toast.LENGTH_SHORT);
                        }
                        toast1.show();
                    }

                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(
                    onDone = {keyboardController?.hide()}),
                leadingIcon = {
                    Image(
                        painterResource(R.drawable.lupa),
                        contentDescription = "",
                        contentScale = ContentScale.FillHeight,
                        modifier = Modifier.size(width = 35.dp, height = 28.dp)
                    )
                },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color(0xFFFBF4F4),
                    textColor = Color(0xFF858585),
                    unfocusedIndicatorColor = Color.Transparent),
                modifier = Modifier.shadow(2.dp, shape = RoundedCornerShape(5.dp))
                    .size(width = 280.dp, height = 32.dp)
                    .border(border = ButtonDefaults.outlinedBorder, shape = RoundedCornerShape(8.dp)
                    )
            )
            Spacer(Modifier.width(25.0.dp))
            Image(
                painterResource(R.drawable.menu),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.size(width = 35.dp, height = 35.dp)
                    .clickable(
                        enabled = true,
                        onClickLabel = "Clickable image",
                        onClick = {
                            navController.navigate("mainMenu")
                        }
                    )
            )

        }
        Spacer(Modifier.height(15.0.dp))

        for (i in 1..12) {

            Spacer(Modifier.height(10.0.dp))
            Row(modifier = Modifier
                .border(BorderStroke(1.dp, Color(0xFFC4C4C4)), shape = RoundedCornerShape(5.dp))
                .padding(top = 5.0.dp, start = 20.0.dp, end = 20.0.dp)
                .size(width = 300.dp, height = 43.dp),
                horizontalArrangement = Arrangement.Center)
            {
                Image(
                    painterResource(R.drawable.iconuser),
                    contentDescription = "",
                    contentScale = ContentScale.FillHeight,
                    modifier = Modifier.size(width = 35.dp, height = 35.dp)
                )
                Spacer(Modifier.width(9.0.dp))
                Text(text = "El bicho si" + "u".repeat(i),
                    fontSize = 16.sp,
                    color = Color(0xFF424B54),
                    modifier = Modifier.padding(top = 7.0.dp)
                )
                Spacer(Modifier.weight(1f))
                Image(
                    painterResource(R.drawable.plus),
                    contentDescription = "",
                    contentScale = ContentScale.FillHeight,
                    modifier = Modifier.size(width = 35.dp, height = 35.dp)
                )
            }
        }
        Spacer(Modifier.weight(1f))
        Row(modifier = Modifier
            .background(Color.White)
            .padding(bottom=5.0.dp)){
            Spacer(Modifier.weight(1f))
            Image(
                painterResource(R.drawable.trophy),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.size(width = 35.dp, height = 35.dp)
            )
            Spacer(Modifier.weight(1f))
            Image(
                painterResource(R.drawable.play),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.size(width = 35.dp, height = 35.dp)
            )
            Spacer(Modifier.weight(1f))
            Image(
                painterResource(R.drawable.house),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.size(width = 35.dp, height = 35.dp)
            )
            Spacer(Modifier.weight(1f))
            Image(
                painterResource(R.drawable.bell),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.size(width = 35.dp, height = 35.dp)
            )
            Spacer(Modifier.weight(1f))
            Image(
                painterResource(R.drawable.user),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.size(width = 35.dp, height = 35.dp)
            )
            Spacer(Modifier.weight(1f))
        }
    }
}