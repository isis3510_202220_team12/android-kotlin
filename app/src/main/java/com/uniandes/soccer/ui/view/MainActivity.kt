package com.uniandes.soccer.ui.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.rememberNavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.uniandes.soccer.ui.maps.MapAddressPickerView
import androidx.activity.viewModels
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.google.firebase.analytics.FirebaseAnalytics
import com.uniandes.soccer.Test
import com.uniandes.soccer.data.localDB.LocationDBHandler
import com.uniandes.soccer.data.model.location.LocationModel
import com.uniandes.soccer.ui.maps.MapAddressPickerViewForCreateTeam
import com.uniandes.soccer.ui.maps.MapAddressPickerViewForRegister
import com.uniandes.soccer.ui.viewModel.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    //private val locationViewModel : LocationViewModel by viewModel<LocationViewModel>()
    private val analytics: FirebaseAnalytics = FirebaseAnalytics.getInstance(this@MainActivity)
    private val match: CreateMatchViewModel by viewModels()
    private val login: LoginViewModel by viewModels()
    private val register: RegisterViewModel by viewModels()
    private val refereeList: RefereeRankingViewModel by viewModels()
    private val matchesList: MatchesListViewModel by viewModels()
    private val chooseTeam: ChooseTeamViewModel by viewModels()
    private val createTeam: CreateTeamViewModel by viewModels()
    private val chooseReferee: ChooseRefereeViewModel by viewModels()
    private val choosePlayer: ChoosePlayerViewModel by viewModels()
    private val refereeDetail: RefereeDetailViewModel by viewModels()
    private val playerDetail: PlayerDetailViewModel by viewModels()
    private val teamsListViewModel: TeamsListViewModel by viewModels()
    private val teamDetail: TeamDetailViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            match.setUpValues(this@MainActivity)
            createTeam.setUpValues(this@MainActivity)
            val navController = rememberNavController()
            NavHost(navController = navController, startDestination = "login") {
                composable("login"){
                    Log(navController, login, analytics)
                }
                composable("referee"){
                    RefereeRank(navController, RefereeRankingViewModel())
                }
                composable("createTeam"){
                    CreateTeam(navController, createTeam, this@MainActivity)
                }
                composable("teamsList"){
                    TeamsList(navController, teamsListViewModel )
                }
                composable("chooseTeam"){
                    ChooseTeam(navController,chooseTeam)
                }
                composable("matchesList/{teamId}",
                    arguments = listOf(
                        navArgument("teamId"){
                         type= NavType.StringType
                        }
                    )
                    ){
                    MatchesList(navController, matchesList, analytics, teamId= it.arguments?.getString("teamId"))
                }
                composable("choosePlayers?lat={lat}&lng={lng}",
                    arguments = listOf(
                        navArgument("lat"){
                            defaultValue = 0.0F
                            type= NavType.FloatType
                        }, navArgument("lng"){
                            defaultValue = 0.0F
                            type= NavType.FloatType
                        }
                    )
                ){
                    ChoosePlayer(navController, choosePlayer, analytics, lat= it.arguments?.getFloat("lat"),
                        lng= it.arguments?.getFloat("lng"))
                }
                composable("chooseReferee?lat={lat}&lng={lng}",
                    arguments = listOf(
                        navArgument("lat"){
                            defaultValue = 0.0F
                            type= NavType.FloatType
                        }, navArgument("lng"){
                            defaultValue = 0.0F
                            type= NavType.FloatType
                        }
                    )
                ){
                    ChooseReferee(navController, chooseReferee, analytics, lat= it.arguments?.getFloat("lat"),
                        lng= it.arguments?.getFloat("lng"))
                }
                composable("createMatch/teamId={teamId}",
                    arguments = listOf(
                        navArgument("teamId"){
                            type= NavType.StringType
                        }
                    )
                ){
                    CreateMatch(navController, match, this@MainActivity,
                        teamId = it.arguments?.getString("teamId"))
                }
                composable("location"){
                    MapAddressPickerView(navController, match)
                }
                composable("locationForRegister"){
                    MapAddressPickerViewForRegister(navController, register)
                }
                composable("locationForCreateTeam"){
                    MapAddressPickerViewForCreateTeam(navController, createTeam)
                }
                composable("test"){
                    Test(navController)
                }
                composable("mainMenu"){
                    MainMenu(navController, login)
                }
                composable("register"){
                    Register(navController, register, analytics, this@MainActivity)
                }
                composable("registerSelection"){
                    RegisterSelection(navController, register, this@MainActivity)
                }
                composable("referees"){
                    RefereeRank(navController, refereeList)
                }
                composable("refereeDetail/{refereeId}",
                    arguments = listOf(
                        navArgument("refereeId"){
                            type= NavType.StringType
                        }
                    )
                ){
                    RefereeDetail(navController, refereeDetail, refereeId= it.arguments?.getString("refereeId"))
                }
                composable("playerDetail/{playerId}",
                    arguments = listOf(
                        navArgument("playerId"){
                            type= NavType.StringType
                        }
                    )
                ){
                    PlayerDetail(navController, playerDetail, playerId= it.arguments?.getString("playerId"))
                }
                composable("teamDetail/{teamId}",
                    arguments = listOf(
                        navArgument("teamId"){
                            type= NavType.StringType
                        }
                    )
                ){
                    teamDetail(navController, teamDetail, teamId= it.arguments?.getString("teamId"))
                }
                composable("matchDetail/{matchInfo}",
                    arguments = listOf(
                        navArgument("matchInfo"){
                            type= NavType.StringType
                        }
                    )
                ){
                    matchDetail(navController, matchInfo= it.arguments?.getString("matchInfo"))
                }
            }
        }
    }
}