package com.uniandes.soccer.ui.view

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.location.LocationManager
import android.util.Patterns
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import com.google.firebase.analytics.FirebaseAnalytics
import com.uniandes.soccer.R
import com.uniandes.soccer.ui.viewModel.RegisterViewModel


private lateinit var toast1: Toast
@OptIn(ExperimentalComposeUiApi::class, ExperimentalPermissionsApi::class)
@Composable
fun Register(
    navController: NavHostController,
    viewModel: RegisterViewModel,
    analytics: FirebaseAnalytics,
    mainActivity: MainActivity
) {
    LaunchedEffect(Unit){
        viewModel.addAnalytics(analytics)
    }
    val sharedPref: SharedPreferences = mainActivity.getPreferences(Context.MODE_PRIVATE)

    val email: String by viewModel.email.observeAsState(initial = "")
    val pass: String by viewModel.pass.observeAsState(initial = "")
    val name: String by viewModel.name.observeAsState(initial = "")
    val age: String by viewModel.age.observeAsState(initial = "")
    val loginEnable: Boolean by viewModel.loginEnable.observeAsState(initial = false)
    val descPlayer: String by viewModel.descPlayer.observeAsState(initial = "")
    val sportsReferee: String by viewModel.sportsReferee.observeAsState(initial = "")
    val isReferee: Boolean by viewModel.isReferee.observeAsState(initial = false)
    val isPlayer: Boolean by viewModel.isPlayer.observeAsState(initial = false)
    val locAcc: Boolean by viewModel.locAcc.observeAsState(initial = false)
    val addressForRegister: String by viewModel.addressForRegister.observeAsState(initial = "")

    val registerMessage: String by viewModel.registerResult
    val logged: Boolean by viewModel.logged.observeAsState(initial = false)


    val context = LocalContext.current
    val keyboardController = LocalSoftwareKeyboardController.current


    var isErrorPass by rememberSaveable { mutableStateOf(false) }
    var isErrorEmail by rememberSaveable { mutableStateOf(false) }
    var isErrorAge by rememberSaveable { mutableStateOf(false) }
    var isErrorName by rememberSaveable { mutableStateOf(false) }
    var isErrorDesc by rememberSaveable { mutableStateOf(false) }
    var isErrorSports by rememberSaveable { mutableStateOf(false) }

    fun validatePass(text: String) {
        isErrorPass = text.length<=6
    }

    fun validateEmail(text: String) {
        isErrorEmail  = !(Patterns.EMAIL_ADDRESS.matcher(text).matches())
    }

    fun validateAge(text: String) {
        try {
            var age:Int = text.toInt()
            isErrorAge = age <= 10
        } catch (ex: Exception) {
            isErrorAge = true
        }
    }

    fun validateName(text: String) {
        isErrorName  = text.length<=6
    }

    fun validateSports(text: String) {
        isErrorSports  = text.length<=6
    }

    fun validateDesc(text: String) {
        isErrorDesc  = text.length<=10
    }

    if(email != ""){
        validateEmail(email)
    }
    if(pass != ""){
        validatePass(pass)
    }
    if(age != ""){
        validateAge(age)
    }
    if(name != ""){
        validateName(name)
    }
    if(sportsReferee != ""){
        validateSports(sportsReferee)
    }
    if(descPlayer != ""){
        validateDesc(descPlayer)
    }


    val fineLocationPermissionState = rememberPermissionState(
        permission = Manifest.permission.ACCESS_FINE_LOCATION
    )

    val lm = LocalContext.current.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    val focusRequester = remember { FocusRequester() }
    val focusManager = LocalFocusManager.current

    fun login(){
        if(registerMessage!=""){
            try {
                toast1.cancel()
                toast1 = Toast.makeText(
                    context,
                    registerMessage,
                    Toast.LENGTH_SHORT
                )
            } catch (e: Exception) {
                toast1 = Toast.makeText(
                    context,
                    registerMessage,
                    Toast.LENGTH_SHORT
                )
            }
            toast1.show()
            viewModel.cleanRegisterMessage()
        }
        if(logged){
            viewModel.clearLoggedStatus()
            navController.navigate("mainMenu"){
                popUpTo(0)
            }

        }
    }
    login()

    if(isReferee && isPlayer){
        viewModel.onChangeRefereePlayer(false,true)
    }
    else if(isReferee){
        Column(

            modifier = Modifier
                .background(Color.White)
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally
        ){

            Spacer(Modifier.weight(1f))
            Image(
                painterResource(R.drawable.fubol),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.size(width = 239.dp, height = 144.dp)
            )
            Spacer(Modifier.height(26.0.dp))
            Text(text = "Email",
                fontSize = 16.0.sp,
                color = Color(0xFF424B54),
                modifier = Modifier
                    .padding(end = 160.dp)
            )
            Spacer(Modifier.height(10.0.dp))
            TextField(
                singleLine = true,
                value = email,
                onValueChange = {
                    if (it.length <= 35) viewModel.onChangeReferee(it, pass, name, age, sportsReferee, locAcc)
                    else {

                        try {
                            toast1.cancel()
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Cannot be more than 35 Characters",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    if(it != "")
                        validateEmail(it)
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(
                    onDone = {keyboardController?.hide()}),
                textStyle = TextStyle.Default.copy(fontSize = 16.sp),
                modifier = Modifier
                    .size(width = 200.dp, height = 56.dp)
                    .border(
                        border = ButtonDefaults.outlinedBorder,
                        shape = RoundedCornerShape(4.dp)
                    )
                    .fillMaxSize()
                    .focusRequester(focusRequester)
                    .onFocusChanged {
                        if (!it.isFocused) {
                            with(sharedPref.edit()) {
                                putString("register/email", email)
                                commit()
                            }
                        }
                    },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                    unfocusedIndicatorColor = Color.Transparent)
            )
            if (isErrorEmail) {
                Text(
                    text = "Incorrect format of email",
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.padding(start = 16.dp)
                )
            }
            Spacer(Modifier.height(10.0.dp))


            Text(text = "Password",
                fontSize = 16.0.sp,
                color = Color(0xFF424B54),
                modifier = Modifier
                    .padding(end = 127.dp)
            )
            Spacer(Modifier.height(10.0.dp))
            TextField(
                singleLine = true,
                value = pass,
                onValueChange = {
                    if (it.length <= 20) viewModel.onChangeReferee(email, it, name, age, sportsReferee, locAcc)
                    else {
                        try {
                            toast1.cancel()
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Cannot be more than 20 Characters",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    if(it != "")
                        validatePass(it)
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done, keyboardType = KeyboardType.Password),
                keyboardActions = KeyboardActions(
                    onDone = {keyboardController?.hide()}),
                visualTransformation = PasswordVisualTransformation(),
                textStyle = TextStyle.Default.copy(fontSize = 16.sp),
                modifier = Modifier
                    .size(width = 200.dp, height = 56.dp)
                    .border(
                        border = ButtonDefaults.outlinedBorder,
                        shape = RoundedCornerShape(4.dp)
                    )
                    .focusRequester(focusRequester)
                    .onFocusChanged {
                        if (!it.isFocused) {
                            with(sharedPref.edit()) {
                                putString("register/pass", pass)
                                commit()
                            }
                        }
                    },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                    unfocusedIndicatorColor = Color.Transparent)
            )
            if (isErrorPass) {
                Text(
                    text = "Password too short",
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.padding(start = 16.dp)
                )
            }
            Spacer(Modifier.height(10.0.dp))
            Text(text = "Name",
                fontSize = 16.0.sp,
                color = Color(0xFF424B54),
                modifier = Modifier
                    .padding(end = 160.dp)
            )
            Spacer(Modifier.height(10.0.dp))


            TextField(
                singleLine = true,
                value = name,
                onValueChange = {
                    if (it.length <= 30) viewModel.onChangeReferee(email, pass, it, age, sportsReferee, locAcc)
                    else {
                        try {
                            toast1.cancel()
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Cannot be more than 30 Characters",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    if(it != "")
                        validateName(it)
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(
                    onDone = {keyboardController?.hide()}),
                textStyle = TextStyle.Default.copy(fontSize = 16.sp),
                modifier = Modifier
                    .size(width = 200.dp, height = 56.dp)
                    .border(
                        border = ButtonDefaults.outlinedBorder,
                        shape = RoundedCornerShape(4.dp)
                    )
                    .focusRequester(focusRequester)
                    .onFocusChanged {
                        if (!it.isFocused) {
                            with(sharedPref.edit()) {
                                putString("register/name", name)
                                commit()
                            }
                        }
                    },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                    unfocusedIndicatorColor = Color.Transparent)
            )
            if (isErrorName) {
                Text(
                    text = "Name too short",
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.padding(start = 16.dp)
                )
            }

            Spacer(Modifier.height(10.0.dp))


            Text(text = "Age",
                fontSize = 16.0.sp,
                color = Color(0xFF424B54),
                modifier = Modifier
                    .padding(end = 170.dp)

            )
            Spacer(Modifier.height(10.0.dp))


            TextField(
                singleLine = true,
                value = age,
                onValueChange = {
                    if (it.length <= 2) viewModel.onChangeReferee(email, pass, name, it, sportsReferee, locAcc)
                    else {
                        try {
                            toast1.cancel()
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Age cannot be more than 99",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    if(it != "")
                        validateAge(it)
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done, keyboardType = KeyboardType.Number),
                keyboardActions = KeyboardActions(
                    onDone = {keyboardController?.hide()}),
                textStyle = TextStyle.Default.copy(fontSize = 16.sp),
                modifier = Modifier
                    .size(width = 200.dp, height = 56.dp)
                    .border(
                        border = ButtonDefaults.outlinedBorder,
                        shape = RoundedCornerShape(4.dp)
                    )
                    .focusRequester(focusRequester)
                    .onFocusChanged {
                        if (!it.isFocused) {
                            with(sharedPref.edit()) {
                                putString("register/age", age)
                                commit()
                            }
                        }
                    },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                    unfocusedIndicatorColor = Color.Transparent)
            )
            if (isErrorAge) {
                Text(
                    text = "Age must be at least 11",
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.padding(start = 16.dp)
                )
            }
            Spacer(Modifier.height(10.0.dp))

            Text(text = "Sports",
                fontSize = 16.0.sp,
                color = Color(0xFF424B54),
                modifier = Modifier
                    .padding(end = 160.dp)
            )
            Spacer(Modifier.height(10.0.dp))

            TextField(
                singleLine = true,
                value = sportsReferee,
                onValueChange = {
                    if (it.length <= 20) viewModel.onChangeReferee(email, pass, name, age, it, locAcc)
                    else {
                        try {
                            toast1.cancel()
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Cannot be more than 20 Characters",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    if(it != "")
                        validateSports(it)
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(
                    onDone = {keyboardController?.hide()}),
                textStyle = TextStyle.Default.copy(fontSize = 16.sp),
                modifier = Modifier
                    .size(width = 200.dp, height = 56.dp)
                    .border(
                        border = ButtonDefaults.outlinedBorder,
                        shape = RoundedCornerShape(4.dp)
                    )
                    .focusRequester(focusRequester)
                    .onFocusChanged {
                        if (!it.isFocused) {
                            with(sharedPref.edit()) {
                                putString("register/sportsReferee", sportsReferee)
                                commit()
                            }
                        }
                    },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                    unfocusedIndicatorColor = Color.Transparent)
            )

            if (isErrorSports) {
                Text(
                    text = "Sports description too short",
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.padding(start = 16.dp)
                )
            }
            Spacer(Modifier.height(10.0.dp))
            Button(
                onClick = {
                    focusManager.clearFocus()

                    if ( !lm.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                        try {
                            toast1.cancel()
                            toast1 = Toast.makeText(
                                context,
                                "Please activate your gps",
                                Toast.LENGTH_SHORT
                            )
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Please activate your gps",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    else{

                        if (fineLocationPermissionState.hasPermission) {
                            if (isConnected()) {
                                viewModel.updateLocation(context, sharedPref)
                                viewModel.onChangeReferee(email, pass, name, age, sportsReferee, true)
                                navController.navigate("locationForRegister")
                            } else {
                                try {
                                    toast1.cancel()
                                    toast1 = Toast.makeText(
                                        context,
                                        "There is not internet connection",
                                        Toast.LENGTH_SHORT
                                    )
                                } catch (e: Exception) {
                                    toast1 = Toast.makeText(
                                        context,
                                        "There is not internet connection",
                                        Toast.LENGTH_SHORT
                                    )
                                }
                                toast1.show()
                            }
                        } else {
                            fineLocationPermissionState.launchPermissionRequest()
                        }
                    }

                },
                shape = RoundedCornerShape(50),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0XFF003893),
                    contentColor = Color(0xFFFFFBFF)
                ),
            ) {
                Text(
                    text = "Location",
                    fontSize = 20.sp,
                )
            }
            Spacer(Modifier.height(41.0.dp))

            Button(
                onClick = {
                    if(isConnected()){
                        viewModel.resetValues(sharedPref)
                        viewModel.signUpUser();login()
                    }
                    else{
                        val dlgAlert: AlertDialog.Builder = AlertDialog.Builder(context)
                        dlgAlert.setMessage("There is not connection but your forms is saved for later :)")
                        dlgAlert.setTitle("Connection error")
                        dlgAlert.setPositiveButton("OK", null)
                        dlgAlert.setCancelable(true)
                        dlgAlert.create().show()
                    }
                },
                shape = RoundedCornerShape(50),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0xFF4D61C4),
                    contentColor = Color(0xFFFFFBFF)),
                enabled = loginEnable

            ){Text(
                text = "Register",
                fontSize = 20.sp,
            )}

            Spacer(Modifier.weight(1f))
        }
    }
    else if(isPlayer){
        Column(

            modifier = Modifier
                .background(Color.White)
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally
        ){

            Spacer(Modifier.weight(1f))
            Image(
                painterResource(R.drawable.fubol),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.size(width = 239.dp, height = 144.dp)
            )
            Spacer(Modifier.height(26.0.dp))
            Text(text = "Email",
                fontSize = 16.0.sp,
                color = Color(0xFF424B54),
                modifier = Modifier
                    .padding(end = 160.dp)
            )
            Spacer(Modifier.height(10.0.dp))
            TextField(
                singleLine = true,
                value = email,
                onValueChange = {
                    if (it.length <= 35) viewModel.onChangePlayer(it, pass, name, age, descPlayer, locAcc)
                    else {
                        try {
                            toast1.cancel()
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Cannot be more than 35 Characters",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    if(it != "")
                        validateEmail(it)
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(
                    onDone = {keyboardController?.hide()}),
                textStyle = TextStyle.Default.copy(fontSize = 16.sp),
                modifier = Modifier
                    .size(width = 200.dp, height = 56.dp)
                    .border(
                        border = ButtonDefaults.outlinedBorder,
                        shape = RoundedCornerShape(4.dp)
                    )
                    .fillMaxSize()
                    .focusRequester(focusRequester)
                    .onFocusChanged {
                        if (!it.isFocused) {
                            with(sharedPref.edit()) {
                                putString("register/email", email)
                                commit()
                            }
                        }
                    },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                    unfocusedIndicatorColor = Color.Transparent)
            )
            if (isErrorEmail) {
                Text(
                    text = "Incorrect format of email",
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.padding(start = 16.dp)
                )
            }
            Spacer(Modifier.height(10.0.dp))


            Text(text = "Password",
                fontSize = 16.0.sp,
                color = Color(0xFF424B54),
                modifier = Modifier
                    .padding(end = 127.dp)
            )
            Spacer(Modifier.height(10.0.dp))
            TextField(
                singleLine = true,
                value = pass,
                onValueChange = {
                    if (it.length <= 20) viewModel.onChangePlayer(email, it, name, age, descPlayer, locAcc)
                    else {
                        try {
                            toast1.cancel()
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Cannot be more than 20 Characters",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    if(it != "")
                        validatePass(it)
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done, keyboardType = KeyboardType.Password),
                keyboardActions = KeyboardActions(
                    onDone = {keyboardController?.hide()}),
                visualTransformation = PasswordVisualTransformation(),
                textStyle = TextStyle.Default.copy(fontSize = 16.sp),
                modifier = Modifier
                    .size(width = 200.dp, height = 56.dp)
                    .border(
                        border = ButtonDefaults.outlinedBorder,
                        shape = RoundedCornerShape(4.dp)
                    )
                    .focusRequester(focusRequester)
                    .onFocusChanged {
                        if (!it.isFocused) {
                            with(sharedPref.edit()) {
                                putString("register/pass", pass)
                                commit()
                            }
                        }
                    },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                    unfocusedIndicatorColor = Color.Transparent)
            )
            if (isErrorPass) {
                Text(
                    text = "Password too short",
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.padding(start = 16.dp)
                )
            }
            Spacer(Modifier.height(10.0.dp))
            Text(text = "Name",
                fontSize = 16.0.sp,
                color = Color(0xFF424B54),
                modifier = Modifier
                    .padding(end = 160.dp)
            )
            Spacer(Modifier.height(10.0.dp))


            TextField(
                singleLine = true,
                value = name,
                onValueChange = {
                    if (it.length <= 30) viewModel.onChangePlayer(email, pass, it, age, descPlayer, locAcc)
                    else {
                        try {
                            toast1.cancel()
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Cannot be more than 30 Characters",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    if(it != "")
                        validateName(it)
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(
                    onDone = {keyboardController?.hide()}),
                textStyle = TextStyle.Default.copy(fontSize = 16.sp),
                modifier = Modifier
                    .size(width = 200.dp, height = 56.dp)
                    .border(
                        border = ButtonDefaults.outlinedBorder,
                        shape = RoundedCornerShape(4.dp)
                    )
                    .focusRequester(focusRequester)
                    .onFocusChanged {
                        if (!it.isFocused) {
                            with(sharedPref.edit()) {
                                putString("register/name", name)
                                commit()
                            }
                        }
                    },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                    unfocusedIndicatorColor = Color.Transparent)
            )
            if (isErrorName) {
                Text(
                    text = "Name too short",
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.padding(start = 16.dp)
                )
            }

            Spacer(Modifier.height(10.0.dp))


            Text(text = "Age",
                fontSize = 16.0.sp,
                color = Color(0xFF424B54),
                modifier = Modifier
                    .padding(end = 170.dp)

            )
            Spacer(Modifier.height(10.0.dp))


            TextField(
                singleLine = true,
                value = age,
                onValueChange = {
                    if (it.length <= 2) viewModel.onChangePlayer(email, pass, name, it, descPlayer, locAcc)
                    else {
                        try {
                            toast1.cancel()
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Age can not be more than 99",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    if(it != "")
                        validateAge(it)
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done, keyboardType = KeyboardType.Number),
                keyboardActions = KeyboardActions(
                    onDone = {keyboardController?.hide()}),
                textStyle = TextStyle.Default.copy(fontSize = 16.sp),
                modifier = Modifier
                    .size(width = 200.dp, height = 56.dp)
                    .border(
                        border = ButtonDefaults.outlinedBorder,
                        shape = RoundedCornerShape(4.dp)
                    )
                    .focusRequester(focusRequester)
                    .onFocusChanged {
                        if (!it.isFocused) {
                            with(sharedPref.edit()) {
                                putString("register/age", age)
                                commit()
                            }
                        }
                    },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                    unfocusedIndicatorColor = Color.Transparent)
            )
            if (isErrorAge) {
                Text(
                    text = "Age must be at least 11",
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.padding(start = 16.dp)
                )
            }
            Spacer(Modifier.height(10.0.dp))

            Text(text = "Description",
                fontSize = 16.0.sp,
                color = Color(0xFF424B54),
                modifier = Modifier
                    .padding(end = 170.dp)

            )
            Spacer(Modifier.height(10.0.dp))
            TextField(
                singleLine = true,
                value = descPlayer,
                onValueChange = {
                    if (it.length <= 150) viewModel.onChangePlayer(email, pass, name, age, it, locAcc)
                    else {
                        try {
                            toast1.cancel()
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Cannot be more than 150 Characters",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    if(it != "")
                        validateDesc(it)
                },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(
                    onDone = {keyboardController?.hide()}),
                textStyle = TextStyle.Default.copy(fontSize = 16.sp),
                label ={ Text(text="tell us something")},
                modifier = Modifier
                    .padding(start = 60.dp, end = 60.dp)
                    .background(color = Color.White)
                    .fillMaxWidth()
                    .height(150.dp)
                    .padding(5.dp)
                    .border(
                        border = ButtonDefaults.outlinedBorder,
                        shape = RoundedCornerShape(4.dp)
                    )
                    .focusRequester(focusRequester)
                    .onFocusChanged {
                        if (!it.isFocused) {
                            with(sharedPref.edit()) {
                                putString("register/descPlayer", descPlayer)
                                commit()
                            }
                        }
                    },
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                    unfocusedIndicatorColor = Color.Transparent)
            )
            if (isErrorDesc) {
                Text(
                    text = "Description too short",
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.padding(start = 16.dp)
                )
            }
            Spacer(Modifier.height(10.0.dp))
            Button(
                onClick = {
                    focusManager.clearFocus()

                    if ( !lm.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                        try {
                            toast1.cancel()
                            toast1 = Toast.makeText(
                                context,
                                "Please activate your gps",
                                Toast.LENGTH_SHORT
                            )
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Please activate your gps",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    else{

                        if (fineLocationPermissionState.hasPermission) {
                            if (isConnected()) {
                                viewModel.onChangePlayer(email, pass, name, age, descPlayer, true)
                                viewModel.updateLocation(context, sharedPref)
                                navController.navigate("locationForRegister")
                            } else {
                                try {
                                    toast1.cancel()
                                    toast1 = Toast.makeText(
                                        context,
                                        "There is not internet connection",
                                        Toast.LENGTH_SHORT
                                    )
                                } catch (e: Exception) {
                                    toast1 = Toast.makeText(
                                        context,
                                        "There is not internet connection",
                                        Toast.LENGTH_SHORT
                                    )
                                }
                                toast1.show()
                            }
                        } else {
                            fineLocationPermissionState.launchPermissionRequest()
                        }
                    }

                },
                shape = RoundedCornerShape(50),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0XFF003893),
                    contentColor = Color(0xFFFFFBFF)
                ),
            ) {
                Text(
                    text = "Location",
                    fontSize = 20.sp,
                )
            }
            Text(text = addressForRegister, //TODO: Only example, needs to be removed
                fontSize = 16.0.sp,
                color = Color(0xFF424B54),
                modifier = Modifier
                    .padding(end = 170.dp)

            )

            Spacer(Modifier.height(41.0.dp))

            Button(
                onClick = {
                    if(isConnected()){
                        viewModel.resetValues(sharedPref)
                        viewModel.signUpUser();login()
                        try {
                            toast1.cancel()
                            toast1 = Toast.makeText(
                                context,
                                "registering...",
                                Toast.LENGTH_SHORT
                            )
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "registering...",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    else{
                        val dlgAlert: AlertDialog.Builder = AlertDialog.Builder(context)
                        dlgAlert.setMessage("There is not connection but your forms is saved for later :)")
                        dlgAlert.setTitle("Connection error")
                        dlgAlert.setPositiveButton("OK", null)
                        dlgAlert.setCancelable(true)
                        dlgAlert.create().show()
                    }
                },
                shape = RoundedCornerShape(50),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0xFF4D61C4),
                    contentColor = Color(0xFFFFFBFF)),
                enabled = loginEnable

            ){Text(
                text = "Register",
                fontSize = 20.sp,
            )}

            Spacer(Modifier.weight(1f))
        }
    }
}