package com.uniandes.soccer.ui.viewModel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.*
import com.uniandes.soccer.data.cache.MyTeamsList
import com.uniandes.soccer.data.cache.UserCache
import com.uniandes.soccer.data.model.match.Match
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.domain.team.GetTeams
import com.uniandes.soccer.domain.user.GetReferees
import com.uniandes.soccer.ui.view.isConnected
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TeamsListViewModel @Inject constructor(
    private val getTeams: GetTeams,

): ViewModel() {

    private val _search = MutableLiveData<String>()
    val search : LiveData<String> = _search
    private val _initialList: MutableState<List<Team>> = mutableStateOf(listOf())
    val teamsList:  MutableState<List<Team>> = mutableStateOf(listOf())
    private val _isCharging = MutableLiveData<Boolean>()
    val isCharging: LiveData<Boolean> = _isCharging
    private val _showDialog = MutableStateFlow(false)
    val showDialog: StateFlow<Boolean> = _showDialog.asStateFlow()



    fun getTeamsByUserId(){
        viewModelScope.launch {
            try {
                val teams = getTeams()
                _initialList.value = teams
                teamsList.value = _initialList.value

            } catch (e: Exception) {
                val error = e.toString().split(":").toTypedArray()
                Log.d("Teams list", "get the teams list failed: ${error[1]}")
                e.printStackTrace()
            }
        _isCharging.value = false
        }
    }

    fun onSearchBarChanged(search:String) {

        _search.value = search
        searchReferee(search)
    }


    private fun searchReferee(search:String){
        teamsList.value = _initialList.value.filter {
            it.name.contains(search, ignoreCase = true) ||
                    search.contains(it.name, ignoreCase = true)
        }
    }

    fun onOpenDialog() {
        _showDialog.value = true
    }

    fun onDialogConfirm() {
        _showDialog.value = false
    }

    fun onDialogDismiss() {
        _showDialog.value = false
    }
    

}
