package com.uniandes.soccer.ui.viewModel

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.CountDownTimer
import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.core.app.ActivityCompat
import androidx.core.app.ComponentActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.cache.LocationCache
import com.uniandes.soccer.data.localDB.LocationDBHandler
import com.uniandes.soccer.data.model.location.LocationModel
import com.uniandes.soccer.data.model.match.Match
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.domain.match.CreateMatch
import com.uniandes.soccer.domain.team.GetTeamById
import com.uniandes.soccer.domain.user.GetUserById
import com.uniandes.soccer.ui.view.MainActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject

@HiltViewModel
class CreateMatchViewModel @Inject constructor(
    application: Application,
    private val createMatch: CreateMatch,
    private val getTeamById: GetTeamById,
    private val locationCache: LocationCache,
) : AndroidViewModel(application) {


    private val app = application

    val dataRecovered: MutableState<Boolean> = mutableStateOf(false)

    private val _date1 = MutableLiveData<String>("")
    val date1: LiveData<String> = _date1

    private val _time = MutableLiveData<String>("")
    val time: LiveData<String> = _time

    private val _referee: MutableState<String?> = mutableStateOf(null)

    private val _addressForRegister: MutableState<String?> = mutableStateOf("")

    private val _teamId: MutableState<String?> = mutableStateOf(null)

    private val _createMatchEnable = MutableLiveData<Boolean>()
    val createMatchEnable: LiveData<Boolean> = _createMatchEnable

    private val _locationEnable = MutableLiveData<Boolean>()
    val locationEnable: LiveData<Boolean> = _locationEnable

    private val _url = MutableLiveData<String>()
    val url: LiveData<String> = _url

    private val _entered = MutableLiveData<Boolean>()
    val entered: LiveData<Boolean> = _entered

    val location = MutableStateFlow<Location>(getInitialLocation())

    var comp: Boolean = false

    fun setUpValues(mainActivity: MainActivity){
        val sharedPref: SharedPreferences = mainActivity.getPreferences(Context.MODE_PRIVATE)

        val dat = sharedPref.getString("createMatch/date", "")
        if(dat != null)
        {
            _date1.value = dat
        }
        else{
            _date1.value = ""
        }

        val tim = sharedPref.getString("createMatch/time", "")
        if(tim != null)
        {
            _time.value = tim
        }
        else{
            _time.value = ""
        }
        var lat = sharedPref.getString("createMatch/latitude", "4.6011")
        var lon = sharedPref.getString("createMatch/longitude", "-74.065")
        if(lat != null && lat != "")
        {
            location.value.latitude = lat.toDouble()
        }
        else{
            location.value.latitude = "4.6011".toDouble()
        }
        if(lon != null && lon != "")
        {
            location.value.longitude = lon.toDouble()
        }
        else{
            location.value.longitude = "-74.065".toDouble()
        }
        _entered.value = false
        _createMatchEnable.value = false
    }

    fun resetValues(sharedPref: SharedPreferences){
        with(sharedPref.edit()) {
            putString("createMatch/referee", "")
            putString("createMatch/selectedText", "")
            putString("createMatch/date", "")
            putString("createMatch/time", "")
            commit()
        }
    }

    fun onChangeEntered(){
        _entered.value = true
        try{
            _createMatchEnable.value =
                isValidReferee() && isValidTeam() && isValidDate(_date1.value!!) && isValidTime(
                    _time.value!!)
        }
        catch (e:Exception){

        }
    }


    fun onCreateMatchChanged(
        date1: String,
        time: String,
        latitude: Double,
        longitude: Double
    ) {
        if (latitude != location.value.latitude) {
            val plocation = Location("")
            plocation.latitude = latitude
            plocation.longitude = longitude
            location.value = plocation
            comp = true
        }
        _date1.value = date1
        _time.value = time
        _createMatchEnable.value =
            isValidReferee() && isValidDate(date1) && isValidTime(
                time
            ) && _entered.value == true
    }

    fun onCreateLocationChanged(
        locationEnable: Boolean
    ) {
        _locationEnable.value = locationEnable
    }

    fun updateLocation(latitude: Double, longitude: Double) {
        if (latitude != location.value.latitude) {
            val plocation = Location("")
            plocation.latitude = latitude
            plocation.longitude = longitude
            location.value = plocation
        }
    }

    private fun isValidReferee(): Boolean = _referee.value != null && _referee.value != ""

    private fun isValidTeam(): Boolean =_teamId.value != null && _teamId.value != ""

    private fun isValidDate(date: String): Boolean = date != ""

    private fun isValidTime(time: String): Boolean = time != ""

    val addressText = mutableStateOf("")
    var isMapEditable = mutableStateOf(true)
    var timer: CountDownTimer? = null

    private fun getInitialLocation(): Location {
        val initialLocation = Location("")
        initialLocation.latitude = 4.6011
        initialLocation.longitude = -74.065
        return initialLocation
    }

    /*
    fun getApi(url: String) {

        var urlIntent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(url)
        )
        print(urlIntent)
    }
    */
    fun getAddressFromLocation(context: Context): String {
        val geocoder = Geocoder(context, Locale.getDefault())
        var addresses: List<Address>? = null
        val address: Address?
        var addressText = ""

        try {
            addresses =
                geocoder.getFromLocation(location.value.latitude, location.value.longitude, 1)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        address = addresses?.get(0)
        addressText = address?.getAddressLine(0) ?: ""


        return addressText
    }

    fun onTextChanged(context: Context, text: String) {
        if (text == "")
            return
        timer?.cancel()
        timer = object : CountDownTimer(1000, 1500) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                location.value = getLocationFromAddress(context, text)
            }
        }.start()
    }

    fun getLocationFromAddress(context: Context, strAddress: String): Location {

        val geocoder = Geocoder(context, Locale.getDefault())
        val address: Address?

        val addresses: List<Address>? = geocoder.getFromLocationName(strAddress, 1)

        if (addresses!!.isNotEmpty()) {
            address = addresses[0]

            var loc = Location("")
            loc.latitude = address.latitude
            loc.longitude = address.longitude
            return loc
        }

        return location.value
    }

    private fun setAddressFromLocation(context: Context): String {

        val geocoder = Geocoder(context, Locale.getDefault())
        var addresses: List<Address>? = null
        var addressText = ""

        try {
            addresses =
                geocoder.getFromLocation(location.value.latitude, location.value.longitude, 1)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        val address: Address? = addresses?.get(0)
        addressText = address?.getAddressLine(0) ?: ""

        if (addresses != null) {
            var ag = addresses[0].subLocality
            if(ag != null)
            {
                _addressForRegister!!.value = "$ag, "
            }
            _addressForRegister!!.value += addresses[0].locality
        }

        return addressText
    }


    fun createAMatch() = viewModelScope.launch {
        try {
            val team = getTeamById(_teamId.value!!)
            val date = _date1.value?.split('-')?.toTypedArray()
            println("DATE CREATE"+_date1.value)
            val time = _time.value?.split(':')?.toTypedArray()
            createMatch(Timestamp(Date(
                date!![0].toInt()-1900,
                date[1].toInt()-1,
                date[2].toInt(),
                time!![0].toInt(), time[1].toInt())),
                _referee.value!!, GeoPoint(location.value.latitude, location.value.longitude),
                team, _addressForRegister.value!!
            )

        }catch(e:Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d("Create Match", "the creation of the match failed: ${error[1]}")
            e.printStackTrace()
        }
    }

    fun updateLocation(context: Context, sharedPref: SharedPreferences){
        lateinit var locationList: List<LocationModel>
        val dbHandler: LocationDBHandler = LocationDBHandler(context);
        locationList = ArrayList<LocationModel>()
        if(locationCache.latitude == 0.0)
        {
            locationList = dbHandler.readLocation()!!
        }
        viewModelScope.launch {
            val mFusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(app)
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                mFusedLocationClient.lastLocation
                    .addOnSuccessListener { pLocation ->
                        if (pLocation != null) {
                            location.value.latitude = pLocation.latitude
                            location.value.longitude = pLocation.longitude
                            launch(Dispatchers.IO) {
                                dbHandler.addLocation(
                                    pLocation.latitude,
                                    pLocation.longitude,
                                )
                                with(sharedPref.edit()) {
                                    putString("createMatch/latitude", pLocation.latitude.toString())
                                    putString("createMatch/longitude", pLocation.longitude.toString())
                                    commit()
                                }
                            }
                        }
                        else{
                            if(locationCache.latitude == 0.0) {
                                if (locationList.isNotEmpty()) {
                                    var loc = locationList.last()

                                    location.value.latitude = loc.latitude
                                    location.value.longitude = loc.longitude
                                }
                            }
                            else{
                                location.value.latitude = locationCache.latitude
                                location.value.longitude = locationCache.longitude
                            }
                        }

                    }
            }
            setAddressFromLocation(context)
        }


    }

    fun setReferee(refereeSelectedId: String) {
        _referee.value= refereeSelectedId
        onChangeEntered()
    }

    fun setTeamId(teamId: String) {
        _teamId.value= teamId
    }

    fun setDataRecovered(b: Boolean) {
        dataRecovered.value=b
    }


}
