package com.uniandes.soccer.ui.viewModel

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.util.Patterns
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.core.app.ActivityCompat
import androidx.lifecycle.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.cache.LocationCache
import com.uniandes.soccer.data.cache.MyMatchesList
import com.uniandes.soccer.data.cache.MyTeamsList
import com.uniandes.soccer.data.cache.UserCache
import com.uniandes.soccer.data.localDB.LocationDBHandler
import com.uniandes.soccer.data.model.location.LocationModel
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.domain.authentication.SignUp
import com.uniandes.soccer.domain.team.GetUsersTeams
import com.uniandes.soccer.domain.team.InsertUsersTeamsRoom
import com.uniandes.soccer.domain.user.InsertUserRoom
import com.uniandes.soccer.domain.user.createUser
import com.uniandes.soccer.ui.view.MainActivity
import com.uniandes.soccer.ui.view.isConnected
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val signUp: SignUp,
    private val createUser: createUser,
    private val userCache: UserCache,
    private val locationCache: LocationCache,
    private val insertUserRoom: InsertUserRoom,
    private val myTeamsList: MyTeamsList,
    private val getUsersTeams: GetUsersTeams,
    private val insertUsersTeamsRoom: InsertUsersTeamsRoom,
    application: Application
): AndroidViewModel(application) {
    lateinit var firebaseAnalytics: FirebaseAnalytics

    private val app = application
    var registerResult: MutableState<String> = mutableStateOf("")

    private val _logged = MutableLiveData(false)
    val logged : LiveData<Boolean> = _logged

    private val _email = MutableLiveData<String>()
    val email : LiveData<String> = _email

    private val _pass = MutableLiveData<String>()
    val pass : LiveData<String> = _pass

    private val _name = MutableLiveData<String>()
    val name : LiveData<String> = _name

    private val _age = MutableLiveData<String>()
    val age : LiveData<String> = _age

    private val _loginEnable = MutableLiveData<Boolean>()
    val loginEnable : LiveData<Boolean> = _loginEnable

    val location= MutableStateFlow<Location>(getInitialLocation())

    private val _descPlayer = MutableLiveData<String>()
    val descPlayer : LiveData<String> = _descPlayer

    private val _sportsReferee = MutableLiveData<String>()
    val sportsReferee : LiveData<String> = _sportsReferee

    private val _isReferee = MutableLiveData(false)
    val isReferee : LiveData<Boolean> = _isReferee

    private val _isPlayer = MutableLiveData(false)
    val isPlayer : LiveData<Boolean> = _isPlayer

    private val _locAcc = MutableLiveData(false)
    val locAcc : LiveData<Boolean> = _locAcc

    private val _addressForRegister = MutableLiveData<String>()
    val addressForRegister : LiveData<String> = _addressForRegister

    fun onChangePlayer(email: String, pass: String, name: String, age: String, desc:String, loc:Boolean){
        _email.value = email
        _pass.value = pass
        _name.value = name
        _age.value = age
        _descPlayer.value = desc
        _locAcc.value = loc
        try {
            _loginEnable.value =
                isValidEmail(email) && isValidPassword(pass) && isValidName(name) && isValidAge(
                    age)&& isValidDesc(desc) && isPlayer.value!! && locAcc.value!!
        }
        catch(e:Exception){
            _loginEnable.value = false
        }
    }

    fun onChangeReferee(email: String, pass: String, name: String, age: String, sports:String, loc:Boolean){
        _email.value = email
        _pass.value = pass
        _name.value = name
        _age.value = age
        _sportsReferee.value = sports
        _locAcc.value = loc
        try {
            _loginEnable.value =
                isValidEmail(email) && isValidPassword(pass) && isValidName(name) && isValidAge(
                    age)&& isValidSports(sports) && isReferee.value!! && locAcc.value!!
        }
        catch(e:Exception){
            _loginEnable.value = false
        }
    }

    fun onChangeRefereePlayer(ref: Boolean, pla: Boolean){
        _isReferee.value = ref
        _isPlayer.value = pla
    }


    private fun isValidPassword(pass: String): Boolean = pass.length > 6

    private fun isValidEmail(email: String): Boolean = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    private fun isValidName(name: String): Boolean = name.length > 6

    private fun isValidAge(age: String): Boolean = age.toInt() > 10

    private fun isValidDesc(descPlayer: String): Boolean = descPlayer.length > 10

    private fun isValidSports(sportsReferee: String): Boolean = sportsReferee.length > 5

    private fun getInitialLocation(): Location {
        val initialLocation = Location("")
        initialLocation.latitude = 4.6011
        initialLocation.longitude = -74.065
        return initialLocation
    }
    fun updateLocation(latitude: Double, longitude: Double) {
        //TODO change for local storage
        if (latitude != location.value.latitude) {
            val plocation = Location("")
            plocation.latitude = latitude
            plocation.longitude = longitude
            location.value = plocation
        }
    }

    fun updateLocation(context: Context, sharedPref: SharedPreferences){
        lateinit var locationList: List<LocationModel>
        val dbHandler: LocationDBHandler = LocationDBHandler(context);
        locationList = ArrayList<LocationModel>()
        if(locationCache.latitude == 0.0)
        {
            locationList = dbHandler.readLocation()!!
        }
        viewModelScope.launch {
            val mFusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(app)
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                mFusedLocationClient.lastLocation
                    .addOnSuccessListener { pLocation ->
                        if (pLocation != null) {
                            location.value.latitude = pLocation.latitude
                            location.value.longitude = pLocation.longitude
                            launch(Dispatchers.IO) {
                            dbHandler.addLocation(
                                pLocation.latitude,
                                pLocation.longitude,
                            )
                            with(sharedPref.edit()) {
                                putString("register/latitude", pLocation.latitude.toString())
                                putString("register/longitude", pLocation.longitude.toString())
                                commit()
                                }
                            locationCache.latitude = pLocation.latitude
                            locationCache.longitude = pLocation.longitude
                            }
                        }
                        else{
                            if(locationCache.latitude == 0.0) {
                                if (locationList.isNotEmpty()) {
                                    var loc = locationList.last()

                                    location.value.latitude = loc.latitude
                                    location.value.longitude = loc.longitude
                                }
                            }
                            else{
                                location.value.latitude = locationCache.latitude
                                location.value.longitude = locationCache.longitude
                            }
                        }

                    }
            }
            getAddressFromLocation(context)
        }


    }

    fun setUpValues(mainActivity: MainActivity){
        val sharedPref: SharedPreferences = mainActivity.getPreferences(Context.MODE_PRIVATE)
        val email = sharedPref.getString("register/email", "")
        if(email != null)
        {
            _email.value = email
        }
        else{
            _email.value = ""
        }

        val name = sharedPref.getString("register/name", "")
        if(name != null)
        {
            _name.value = name
        }
        else{
            _name.value = ""
        }

        val pass = sharedPref.getString("register/pass", "")
        if(pass != null)
        {
            _pass.value = pass
        }
        else{
            _pass.value = ""
        }
        val age = sharedPref.getString("register/age", "")
        if(age != null)
        {
            _age.value = age
        }
        else{
            _age.value = ""
        }
        val desc = sharedPref.getString("register/descPlayer", "")
        if(desc != null)
        {
            _descPlayer.value = desc
        }
        else{
            _descPlayer.value = ""
        }
        val sport = sharedPref.getString("register/sportsReferee", "")
        if(sport != null)
        {
            _sportsReferee.value = sport
        }
        else{
            _sportsReferee.value = ""
        }
        var lat = sharedPref.getString("register/latitude", "4.6011")
        var lon = sharedPref.getString("register/longitude", "-74.065")
        if(lat != null && lat != "")
        {
            location.value.latitude = lat.toDouble()
        }
        else{
            location.value.latitude = "4.6011".toDouble()
        }
        if(lon != null && lon != "")
        {
            location.value.longitude = lon.toDouble()
        }
        else{
            location.value.longitude = "-74.065".toDouble()
        }
        val isref = sharedPref.getBoolean("register/isReferee", false)
        if(isref != null)
        {
            _isReferee.value = isref
        }
        else{
            _isReferee.value = false
        }
        val ispla = sharedPref.getBoolean("register/isPlayer", false)
        if(ispla != null)
        {
            _isPlayer.value = ispla
        }
        else{
            _isPlayer.value = false
        }
        _loginEnable.value = false
        _logged.value = false
    }

    fun resetValues(sharedPref: SharedPreferences){
        with(sharedPref.edit()) {
            putBoolean("register/loginEnable", false)
            putBoolean("register/isReferee", false)
            putBoolean("register/isPlayer", false)
            putString("register/email", "")
            putString("register/name", "")
            putString("register/pass", "")
            putString("register/age", "")
            putString("register/descPlayer", "")
            putString("register/sportsReferee", "")
            commit()
        }
    }

    val addressText = mutableStateOf("")
    var isMapEditable = mutableStateOf(true)
    var timer: CountDownTimer? = null

    fun getAddressFromLocation(context: Context): String {

        val geocoder = Geocoder(context, Locale.getDefault())
        var addresses: List<Address>? = null
        val address: Address?
        var addressText = ""

        try {
            addresses =
                geocoder.getFromLocation(location.value.latitude, location.value.longitude, 1)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        address = addresses?.get(0)
        addressText = address?.getAddressLine(0) ?: ""

        if (addresses != null) {
            var ag = addresses[0].subLocality
            if(ag != null)
            {
                _addressForRegister!!.value = "$ag, "
            }
            _addressForRegister!!.value += addresses[0].locality
        }

        return addressText
    }

    fun onTextChanged(context: Context, text: String) {
        if (text == "")
            return
        timer?.cancel()
        timer = object : CountDownTimer(1000, 1500) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                location.value = getLocationFromAddress(context, text)
            }
        }.start()
    }

    fun getLocationFromAddress(context: Context, strAddress: String): Location {

        val geocoder = Geocoder(context, Locale.getDefault())
        val addresses: List<Address>?
        val address: Address?

        addresses = geocoder.getFromLocationName(strAddress, 1)

        if (addresses!!.isNotEmpty()) {
            address = addresses[0]

            var loc = Location("")
            loc.latitude = address.getLatitude()
            loc.longitude = address.getLongitude()
            return loc
        }

        return location.value
    }






    fun signUpUser() = viewModelScope.launch {
        if(isConnected()) {
            try {
                _loginEnable.value = false
                var user: User? = null

                signUp(_email.value!!, _pass.value!!)

                if (_isReferee.value == false) {
                    user = createUser(
                        _email.value!!,
                        _pass.value!!,
                        _name.value!!,
                        _age.value?.toInt()!!,
                        GeoPoint(location.value.latitude, location.value.longitude),
                        false,
                        null,
                        null,
                        null,
                        _descPlayer.value!!,
                        _addressForRegister.value!!
                    )
                } else {
                    user = createUser(
                        _email.value!!,
                        _pass.value!!,
                        _name.value!!,
                        _age.value?.toInt()!!,
                        GeoPoint(location.value.latitude, location.value.longitude),
                        true,
                        null,
                        null,
                        4.5,
                        _sportsReferee.value!!,
                        _addressForRegister.value!!
                    )
                }
                user?.let {
                    userCache.id = user.id
                    userCache.name = user.name
                    userCache.age = user.age
                    userCache.email = user.email
                    userCache.location = user.location
                    userCache.password = user.password
                    userCache.isReferee = user.isReferee
                    userCache.photo = user.photo
                    userCache.locationName = user.locationName
                }

                if (user != null) {
                    insertUserRoom(user)
                    val teams = getUsersTeams(user.id)
                    myTeamsList.saveTeamsToCache(user.id, teams)
                    insertUsersTeamsRoom(teams)
                }

                val parameters = Bundle().apply {
                    this.putString("user_id", userCache.id)
                    this.putString("user_name", userCache.name)
                    this.putInt("user_age", userCache.age)
                    this.putString("user_email", userCache.email)
                    this.putString("isReferee", userCache.isReferee.toString())
                    this.putString("user_locationName", userCache.locationName)

                }
                firebaseAnalytics.logEvent("Sign_Up", parameters)
                registerResult.value = "Register success"
                _logged.postValue(true)
                _loginEnable.value = true
            } catch (e: Exception) {
                registerResult.value = "Register failed, an user with that email already exists"
                val error = e.toString().split(":").toTypedArray()
                Log.d("Register", "signUpUser: ${error[1]}")
            }
        } else {
            registerResult.value = "There's no internet connection, please connect and try again"
        }
    }

    fun cleanRegisterMessage() {
        registerResult.value = ""
    }

    fun clearLoggedStatus() {
        _logged.value = false
    }

    fun addAnalytics(analytics: FirebaseAnalytics) {
        firebaseAnalytics = analytics
    }


}
