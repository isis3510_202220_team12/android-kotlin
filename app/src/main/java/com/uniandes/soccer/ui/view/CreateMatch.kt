package com.uniandes.soccer.ui.view

import android.Manifest
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.location.LocationManager
import android.widget.DatePicker
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.toSize
import androidx.navigation.NavHostController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import com.uniandes.soccer.R
import com.uniandes.soccer.ui.viewModel.CreateMatchViewModel
import java.util.*

    private lateinit var toast1: Toast

    @OptIn(ExperimentalComposeUiApi::class, ExperimentalPermissionsApi::class)
    @Composable
    fun CreateMatch(
        navController: NavHostController,
        createMatchViewModel: CreateMatchViewModel,
        mainActivity: MainActivity,
        teamId: String?,
    ) {

        LaunchedEffect(Unit){

            if(!createMatchViewModel.dataRecovered.value){
                createMatchViewModel.setUpValues(mainActivity)
                createMatchViewModel.setDataRecovered(true)
            }
            if(teamId!= null){
                createMatchViewModel.setTeamId(teamId)
            }
            if (navController.currentBackStackEntry!!.savedStateHandle.contains("refereeId")) {
                createMatchViewModel.setReferee(navController.currentBackStackEntry!!.savedStateHandle.get<String>(
                        "refereeId"
                    ) ?: "")
            }
        }

        val sharedPref: SharedPreferences = mainActivity.getPreferences(Context.MODE_PRIVATE)
        val currentLocation = createMatchViewModel.location.collectAsState()
        val enableButton: Boolean by createMatchViewModel.createMatchEnable.observeAsState(initial = false)
        val lm = LocalContext.current.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        val focusRequester = remember { FocusRequester() }
        val focusManager = LocalFocusManager.current

        val context = LocalContext.current
        val keyboardController = LocalSoftwareKeyboardController.current



        val fineLocationPermissionState = rememberPermissionState(
            permission = Manifest.permission.ACCESS_FINE_LOCATION
        )



        /*
    val multiplePermissionsState = rememberMultiplePermissionsState(
        listOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION
        )
    )

    if (multiplePermissionsState.allPermissionsGranted) {
        createMatchViewModel.onCreateLocationChanged(true)
    }
    else {
        showPer = true
    }
    */




        Column(

            modifier = Modifier
                .background(Color.White)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(Modifier.weight(1f))
            Image(
                painterResource(R.drawable.field),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier.size(width = 191.dp, height = 144.dp)
            )
            Spacer(Modifier.height(39.0.dp))


            Spacer(Modifier.height(10.0.dp))

            Spacer(Modifier.height(10.0.dp))
            DatePicker(
                createMatchViewModel,
                currentLocation.value.latitude,
                currentLocation.value.longitude,
                sharedPref,
                focusManager
            )
            Spacer(Modifier.height(5.0.dp))
            TimePicker(
                createMatchViewModel,
                currentLocation.value.latitude,
                currentLocation.value.longitude,
                sharedPref,
                focusManager
            )

            Spacer(Modifier.height(10.0.dp))

            Spacer(Modifier.height(10.0.dp))
            Button(
                onClick = {
                    focusManager.clearFocus()

                    if ( !lm.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                        try {
                            toast1.cancel()
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "Please activate your gps",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                    else{

                        if (fineLocationPermissionState.hasPermission) {
                            if (isConnected()) {
                                createMatchViewModel.updateLocation(context, sharedPref)
                                createMatchViewModel.onChangeEntered()
                                navController.navigate("location")
                            } else {
                                try {
                                    toast1.cancel()
                                    toast1 = Toast.makeText(
                                        context,
                                        "There is not internet connection",
                                        Toast.LENGTH_SHORT
                                    )
                                } catch (e: Exception) {
                                    toast1 = Toast.makeText(
                                        context,
                                        "There is not internet connection",
                                        Toast.LENGTH_SHORT
                                    )
                                }
                                toast1.show()
                            }
                        } else {
                            fineLocationPermissionState.launchPermissionRequest()
                        }
                    }

                },
                shape = RoundedCornerShape(50),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0XFF003893),
                    contentColor = Color(0xFFFFFBFF)
                ),
            ) {
                Text(
                    text = "Location",
                    fontSize = 20.sp,
                )
            }

            Button(onClick = {
                focusManager.clearFocus()
                navController.navigate("chooseReferee?lat=${currentLocation.value.latitude}&lng=${currentLocation.value.longitude}")
            }, colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF4D61C4))) {
                Text(text = "Select referee", color = Color.White)
            }

            Spacer(Modifier.height(10.0.dp))
            Button(
                onClick = {
                    focusManager.clearFocus()
                    createMatchViewModel.createAMatch()
                    createMatchViewModel.resetValues(sharedPref)
                    navController.navigate("mainMenu"){
                        popUpTo("mainMenu")
                    }
                },
                shape = RoundedCornerShape(50),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0XFF950000),
                    contentColor = Color(0xFFFFFBFF)
                ),
                enabled = enableButton
                //colors = ButtonDefaults.outlinedButtonColors(contentColor = Color(0xFF4D61C4))

            ) {
                Text(
                    text = "Create",
                    fontSize = 20.sp,
                )
            }

            Spacer(Modifier.weight(1f))
        }
    }

    fun isConnected(): Boolean {
        val command = "ping -c 1 google.com"
        return Runtime.getRuntime().exec(command).waitFor() == 0
    }

    @Composable
    fun DatePicker(
        createMatchViewModel: CreateMatchViewModel,
        latitude: Double,
        longitude: Double,
        sharedPref: SharedPreferences,
        focusManager: FocusManager
    ) {
        val mDate: String by createMatchViewModel.date1.observeAsState(initial = "")
        val mTime: String by createMatchViewModel.time.observeAsState(initial = "")
        val mContext = LocalContext.current
        val mYear: Int
        val mMonth: Int
        val mDay: Int
        val mCalendar = Calendar.getInstance()

        mYear = mCalendar.get(Calendar.YEAR)
        mMonth = mCalendar.get(Calendar.MONTH)
        mDay = mCalendar.get(Calendar.DAY_OF_MONTH)
        mCalendar.time = Date()


        val mDatePickerDialog = DatePickerDialog(
            mContext,
            { _: DatePicker, mYear: Int, mMonth: Int, mDayOfMonth: Int ->
                createMatchViewModel.onCreateMatchChanged(
                    "$mYear-${mMonth + 1}-$mDayOfMonth",
                    mTime,
                    latitude,
                    longitude
                )

                with(sharedPref.edit()) {
                    putString("createMatch/date", "$mYear-${mMonth}-$mDayOfMonth")
                    apply()
                }
            }, mYear, mMonth, mDay
        )

        Row {
            Spacer(Modifier.weight(1f))
            Button(onClick = {
                focusManager.clearFocus()
                mDatePickerDialog.show()
            }, colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF4D61C4))) {
                Text(text = "Open Date Picker", color = Color.White)
            }

            Spacer(Modifier.width(20.dp))

            Text(
                text = mDate, fontSize = 16.sp, textAlign = TextAlign.Center,
                modifier = Modifier
                    .padding(top = 10.dp)
            )
            Spacer(Modifier.weight(1f))
        }
    }

    @Composable
    fun TimePicker(
        createMatchViewModel: CreateMatchViewModel,
        latitude: Double,
        longitude: Double,
        sharedPref: SharedPreferences,
        focusManager: FocusManager
    ) {
        val mDate: String by createMatchViewModel.date1.observeAsState(initial = "")
        val mTime: String by createMatchViewModel.time.observeAsState(initial = "")
        val mContext = LocalContext.current

        val mCalendar = Calendar.getInstance()
        val mHour = mCalendar[Calendar.HOUR_OF_DAY]
        val mMinute = mCalendar[Calendar.MINUTE]

        val mTimePickerDialog = TimePickerDialog(
            mContext,
            { _, mHour: Int, mMinute: Int ->
                createMatchViewModel.onCreateMatchChanged(
                    mDate,
                    "$mHour:$mMinute",
                    latitude,
                    longitude
                )
                with(sharedPref.edit()) {
                    putString("createMatch/time", "$mHour:$mMinute")
                    apply()
                }
            }, mHour, mMinute, false
        )


        Row {
            Spacer(Modifier.weight(1f))
            Button(
                onClick = {
                    focusManager.clearFocus()
                    mTimePickerDialog.show() },
                colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF4D61C4))
            ) {
                Text(text = "Open Time Picker", color = Color.White)
            }
            Spacer(Modifier.width(20.dp))

            Text(
                text = mTime, fontSize = 16.sp,
                modifier = Modifier
                    .padding(top = 10.dp)
            )
            Spacer(Modifier.weight(1f))
        }
    }