package com.uniandes.soccer.ui.maps

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavHostController
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.MapView
import com.google.android.libraries.maps.model.LatLng
import com.uniandes.soccer.ui.viewModel.CreateTeamViewModel

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun MapAddressPickerViewForCreateTeam(navController: NavHostController, createTeamViewModel: CreateTeamViewModel){
    Surface(color = MaterialTheme.colors.background) {
        val mapView = rememberMapViewWithLifecycle()
        val currentLocation = createTeamViewModel.location.collectAsState()
        var text by remember { createTeamViewModel.addressText }
        val context = LocalContext.current
        val keyboardController = LocalSoftwareKeyboardController.current


        Column(Modifier.fillMaxWidth()) {

            Box{
                TextField(
                    value = text,
                    onValueChange = {
                        text = it
                        if(!createTeamViewModel.isMapEditable.value)
                            createTeamViewModel.onTextChanged(context, text)
                    },
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                    keyboardActions = KeyboardActions(
                        onDone = {keyboardController?.hide()}),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(end = 80.dp),
                    enabled = !createTeamViewModel.isMapEditable.value,
                    colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.Transparent)
                )

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp)
                        .padding(bottom = 20.dp),
                    horizontalAlignment = Alignment.End
                ){
                    Button(
                        onClick = {
                            createTeamViewModel.isMapEditable.value = !createTeamViewModel.isMapEditable.value
                        }
                    ) {
                        Text(text = if(createTeamViewModel.isMapEditable.value) "Edit" else "Save")
                    }
                }
            }

            Box(modifier = Modifier.fillMaxHeight()){

                currentLocation.value.let {
                    if(createTeamViewModel.isMapEditable.value) {
                        try {
                            text = createTeamViewModel.getAddressFromLocation(context)
                        } catch (e: Exception) {
                            Toast.makeText(
                                context,
                                "There is not internet connection",
                                Toast.LENGTH_SHORT
                            ).show()
                            navController.navigate("createMatch")
                        }
                    }
                    MapViewContainerForCreateTeam(createTeamViewModel.isMapEditable.value, mapView, createTeamViewModel, navController)
                }

                MapPinOverlay()
            }
        }
    }
}

@Composable
private fun MapViewContainerForCreateTeam(
    isEnabled: Boolean,
    mapView: MapView,
    createTeamViewModel: CreateTeamViewModel,
    navController: NavHostController
) {
    val context = LocalContext.current
    AndroidView(
        factory = { mapView }
    ) {
        try {

            mapView.getMapAsync { map ->

                map.uiSettings.setAllGesturesEnabled(isEnabled)

                val location = createTeamViewModel.location.value
                val position = LatLng(location.latitude, location.longitude)
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(position,  15f))

                map.setOnCameraIdleListener {
                    val cameraPosition = map.cameraPosition
                    createTeamViewModel.updateLocation(cameraPosition.target.latitude, cameraPosition.target.longitude)
                }
            }

        } catch (e: Exception) {
            Toast.makeText(
                context,
                "There is not internet connection",
                Toast.LENGTH_SHORT
            ).show()
            navController.navigate("createMatch")
        }
    }

}
