package com.uniandes.soccer.ui.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RefereeRankingViewModel @Inject constructor(): ViewModel() {
    private val _search = MutableLiveData<String>()
    val search: LiveData<String> = _search

    fun onRefereeRankingChanged(search: String) {

        _search.value = search
    }
}
