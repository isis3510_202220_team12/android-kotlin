package com.uniandes.soccer.ui.view

import android.content.Context
import android.content.SharedPreferences
import androidx.compose.foundation.*
import androidx.compose.material.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ContentAlpha.medium
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.uniandes.soccer.ui.viewModel.RegisterViewModel
import androidx.compose.material.ContentAlpha.medium
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import com.uniandes.soccer.R

@OptIn(ExperimentalComposeUiApi::class, ExperimentalPermissionsApi::class)
@Composable
fun RegisterSelection(navController: NavHostController, viewModel: RegisterViewModel, mainActivity: MainActivity) {
    val sharedPref: SharedPreferences = mainActivity.getPreferences(Context.MODE_PRIVATE)
    viewModel.setUpValues(mainActivity)

    val isReferee: Boolean by viewModel.isReferee.observeAsState(initial = false)
    val isPlayer: Boolean by viewModel.isPlayer.observeAsState(initial = false)


    val context = LocalContext.current

    if(!isReferee || !isPlayer){
        Column(modifier = Modifier.fillMaxSize().background(Color.White)){
            Spacer(Modifier.weight(1.5f))
            Text(text = "Select your profile",
                fontSize = 22.0.sp,
                color = Color(0xFF121C2B),
                textAlign = TextAlign.End,
                modifier = Modifier
                    .padding(top = 40.dp, start = 40.dp)

            )
            Column(
                modifier = Modifier
                    .background(Color.White)
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState()),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.SpaceEvenly
            ){
                Surface(
                    elevation = 10.dp,
                    color = MaterialTheme.colors.surface,
                    modifier = Modifier
                        .height(200.dp)
                        .width(200.dp)
                ) {
                    Column(
                        modifier = Modifier
                            .clip(RoundedCornerShape(4.dp))
                            .fillMaxSize()
                            .clickable {
                                viewModel.onChangeRefereePlayer(true, false)
                                with(sharedPref.edit()) {
                                    putBoolean("register/isReferee", true)
                                    putBoolean("register/isPlayer", false)
                                    commit()
                                }
                                navController.navigate("register")
                            },
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.SpaceEvenly
                    ) {
                        Image(
                            painterResource(R.drawable.fubol),
                            contentDescription = "",
                            contentScale = ContentScale.FillHeight,
                            modifier = Modifier.size(width = 239.dp, height = 144.dp)
                        )
                        Text(
                            text = "I am a referee",
                            fontSize = 25.0.sp,
                            color = Color(0xFF121C2B),
                            textAlign = TextAlign.Center,
                            modifier = Modifier.fillMaxWidth()

                        )
                    }
                }
                Surface(
                    elevation = 10.dp,
                    color = MaterialTheme.colors.surface,
                    modifier = Modifier
                        .height(200.dp)
                        .width(200.dp)
                ) {
                    Column(modifier = Modifier
                        .clip(RoundedCornerShape(4.dp))
                        .fillMaxSize()
                        .clickable {
                            viewModel.onChangeRefereePlayer(false, true)
                            with(sharedPref.edit()) {
                                putBoolean("register/isReferee", false)
                                putBoolean("register/isPlayer", true)
                                commit()
                            }
                            navController.navigate("register")
                        },
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.SpaceEvenly
                    ){
                        Image(
                            painterResource(R.drawable.fubol),
                            contentDescription = "",
                            contentScale = ContentScale.FillHeight,
                            modifier = Modifier.size(width = 239.dp, height = 144.dp)
                        )
                        Text(text = "I am a player",
                            fontSize = 25.0.sp,
                            color = Color(0xFF121C2B),
                            textAlign = TextAlign.Center,
                            modifier = Modifier.fillMaxWidth()

                        )
                    }
                }
            }
            Spacer(Modifier.weight(1f))
        }
    }
    else if(isReferee && isPlayer){
        viewModel.onChangeRefereePlayer(false,true)
    }
}

@Preview
@OptIn(ExperimentalComposeUiApi::class, ExperimentalPermissionsApi::class)
@Composable
fun RegisterSelection(){
    Column(modifier = Modifier.fillMaxSize().background(Color.White)){
        Spacer(Modifier.weight(1f))
        Text(text = "Select your profile",
            fontSize = 20.0.sp,
            color = Color(0xFF121C2B),
            textAlign = TextAlign.End,
            modifier = Modifier
                .padding(top = 40.dp, start = 40.dp)

        )
        Column(
            modifier = Modifier
                .background(Color.White)
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceEvenly
        ){
            Surface(
                elevation = 10.dp,
                color = MaterialTheme.colors.surface,
                modifier = Modifier
                    .height(200.dp)
                    .width(200.dp)
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .border(
                            shape = RoundedCornerShape(4.dp),
                            border = ButtonDefaults.outlinedBorder
                        )
                        .clickable {
                            /*viewModel.onChangeRefereePlayer(false, true)
                    with(sharedPref.edit()) {
                        putBoolean("register/isReferee", false)
                        putBoolean("register/isPlayer", true)
                        commit()
                    }
                    navController.navigate("register")*/
                        },
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.SpaceEvenly
                ) {
                    Image(
                        painterResource(R.drawable.fubol),
                        contentDescription = "",
                        contentScale = ContentScale.FillHeight,
                        modifier = Modifier.size(width = 239.dp, height = 144.dp)
                    )
                    Text(
                        text = "I am a referee",
                        fontSize = 25.0.sp,
                        color = Color(0xFF121C2B),
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth()

                    )
                }
            }
            Surface(
                    elevation = 10.dp,
                    color = MaterialTheme.colors.surface,
                    modifier = Modifier
                    .height(200.dp)
                    .width(200.dp)
            ) {
                Column(modifier = Modifier
                    .clip(RoundedCornerShape(4.dp))
                    .fillMaxSize()
                    .clickable {
                        /*viewModel.onChangeRefereePlayer(false, true)
                        with(sharedPref.edit()) {
                            putBoolean("register/isReferee", false)
                            putBoolean("register/isPlayer", true)
                            commit()
                        }
                        navController.navigate("register")*/
                    },
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.SpaceEvenly
                ){
                    Image(
                        painterResource(R.drawable.fubol),
                        contentDescription = "",
                        contentScale = ContentScale.FillHeight,
                        modifier = Modifier.size(width = 239.dp, height = 144.dp)
                    )
                    Text(text = "I am a player",
                        fontSize = 25.0.sp,
                        color = Color(0xFF121C2B),
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth()

                    )
                }
            }
        }
        Spacer(Modifier.weight(1f))
    }
}