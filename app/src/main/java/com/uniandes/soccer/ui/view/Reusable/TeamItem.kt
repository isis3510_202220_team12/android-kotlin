package com.uniandes.soccer.ui.view.Reusable

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.uniandes.soccer.R
import com.uniandes.soccer.data.model.team.Team


@Composable
fun TeamItem(navController:NavHostController,modifier:Modifier, team: Team){
    Spacer(Modifier.height(12.0.dp))
    Row(modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center)
    {
        Column(modifier = Modifier.weight(1f)
            .clickable { navController.navigate("matchesList/${team.id}") },
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center
        ){
            Image(
                painterResource(R.drawable.icono0),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .padding(start = 5.dp)
                    .fillMaxHeight(0.9f)
            )
        }

        Column(modifier = Modifier.weight(1f)
            .clickable { navController.navigate("matchesList/${team.id}") },
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = "${team.name}",
                fontSize = 18.sp,
                color = Color.Black,
                modifier= Modifier
                    .fillMaxWidth(),
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold
            )
        }
        Column(modifier = Modifier.weight(1f)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = Color.Red),
            ) {
                navController.navigate("teamDetail/${team.name+"|"+team.ranking+"|"+team.commentaries}")
                },
            horizontalAlignment = Alignment.End,
            verticalArrangement = Arrangement.Center
        ) {
            Image(
                painterResource(R.drawable.plus),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .padding(start = 5.dp)
                    .fillMaxHeight(0.9f)
            )
        }


    }
}