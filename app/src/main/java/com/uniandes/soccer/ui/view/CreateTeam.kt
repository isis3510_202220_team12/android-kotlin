package com.uniandes.soccer.ui.view

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.location.LocationManager
import androidx.compose.foundation.Image
import androidx.compose.material.*
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import android.widget.Toast
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import com.uniandes.soccer.R
import com.uniandes.soccer.ui.viewModel.CreateTeamViewModel

private lateinit var toast1: Toast

@OptIn(ExperimentalComposeUiApi::class, ExperimentalPermissionsApi::class)
@Composable
fun CreateTeam(
    navController: NavHostController,
    createTeamViewModel: CreateTeamViewModel,
    mainActivity: MainActivity
) {
    LaunchedEffect(Unit){

        if(!createTeamViewModel.dataRecovered.value){
            createTeamViewModel.setUpValues(mainActivity)
            createTeamViewModel.setDataRecovered(true)
        }
        if (navController.currentBackStackEntry?.savedStateHandle?.contains("playerId") == true) {
            createTeamViewModel.addPlayer(
                navController.currentBackStackEntry!!.savedStateHandle.get<String>(
                "playerId"
            ) ?: "")
        }
    }
    val lm = LocalContext.current.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    val focusRequester = remember { FocusRequester() }
    val focusManager = LocalFocusManager.current
    val fineLocationPermissionState = rememberPermissionState(
        permission = Manifest.permission.ACCESS_FINE_LOCATION
    )
    val sharedPref: SharedPreferences = mainActivity.getPreferences(Context.MODE_PRIVATE)
    val currentLocation = createTeamViewModel.location.collectAsState()
    val name: String by createTeamViewModel.name.observeAsState(initial = "")
    val contact: String by createTeamViewModel.ranking.observeAsState(initial = "")
    val desc: String by createTeamViewModel.desc.observeAsState(initial = "")
    val enableButton: Boolean by createTeamViewModel.createEnable.observeAsState(initial = false)
    val locAcc: Boolean by createTeamViewModel.locAcc.observeAsState(initial = false)
    val context = LocalContext.current
    val keyboardController = LocalSoftwareKeyboardController.current

    var isErrorName by rememberSaveable { mutableStateOf(false) }
    var isErrorDesc by rememberSaveable { mutableStateOf(false) }
    var isErrorContact by rememberSaveable { mutableStateOf(false) }


    fun validateName(text: String) {
        isErrorName  = text.length<=6
    }

    fun validateContact(text: String) {
        isErrorContact  = text.length<=5
    }

    fun validateDesc(text: String) {
        isErrorDesc  = text.length<=10
    }

    if(name != ""){
        validateName(name)
    }
    if(contact != ""){
        validateContact(contact)
    }
    if(desc != ""){
        validateDesc(desc)
    }
    Column(

        modifier = Modifier
            .background(Color.White)
            .fillMaxSize()
            .padding(top = 80.0.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        Image(
            painterResource(R.drawable.addimagei),
            contentDescription = "",
            contentScale = ContentScale.FillHeight,
            modifier = Modifier.size(width = 100.dp, height = 100.dp)
        )
        Spacer(Modifier.height(26.0.dp))
        Text(text = "Name:",
            fontSize = 16.0.sp,
            color = Color(0xFF424B54),
            modifier = Modifier
                .padding(end = 152.dp)
        )
        Spacer(Modifier.height(10.0.dp))
        TextField(
            value = name,
            onValueChange = { if (it.length <= 20) createTeamViewModel.onCreateTeamChanged(it, contact, desc, locAcc)
            else {
                try{
                    toast1.cancel()
                } catch (e:Exception) {         // invisible if exception
                    toast1 = Toast.makeText(context, "Cannot be more than 20 Characters", Toast.LENGTH_SHORT);
                }
                toast1.show();
            }
            if(it != "")
                validateName(it)
            },
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(
                onDone = {keyboardController?.hide()}),
            textStyle = TextStyle.Default.copy(fontSize = 16.sp),
            modifier = Modifier
                .size(width = 200.dp, height = 56.dp)
                .border(
                    border = ButtonDefaults.outlinedBorder,
                    shape = RoundedCornerShape(4.dp)
                )
                .fillMaxSize()
                .focusRequester(focusRequester)
                .onFocusChanged {
                    if (!it.isFocused) {
                        with(sharedPref.edit()) {
                            putString("createTeam/name", name)
                            commit()
                        }
                    }
                },
            colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                unfocusedIndicatorColor = Color.Transparent)
        )
        if (isErrorName) {
            Text(
                text = "Name too short",
                color = MaterialTheme.colors.error,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.padding(start = 16.dp)
            )
        }
        Spacer(Modifier.height(10.0.dp))
        Text(text = "ranking:",
            fontSize = 16.0.sp,
            color = Color(0xFF424B54),
            modifier = Modifier
                .padding(end = 142.dp)
        )
        Spacer(Modifier.height(10.0.dp))
        TextField(
            value = contact,
            onValueChange = {
                if (it.length <= 5) createTeamViewModel.onCreateTeamChanged(name, it, desc, locAcc)
                else {
                    try{
                        toast1.cancel()
                    } catch (e:Exception) {         // invisible if exception
                        toast1 = Toast.makeText(context, "Cannot be more than 5 Characters", Toast.LENGTH_SHORT);
                    }
                    toast1.show();
                }
            },
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done, keyboardType = KeyboardType.Number),
            keyboardActions = KeyboardActions(
                onDone = {keyboardController?.hide()}),
            textStyle = TextStyle.Default.copy(fontSize = 16.sp),
            modifier = Modifier
                .size(width = 200.dp, height = 56.dp)
                .border(
                    border = ButtonDefaults.outlinedBorder,
                    shape = RoundedCornerShape(4.dp)
                )
                .focusRequester(focusRequester)
                .onFocusChanged {
                    if (!it.isFocused) {
                        with(sharedPref.edit()) {
                            putString("createTeam/ranking", contact)
                            commit()
                        }
                    }
                },
            colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                unfocusedIndicatorColor = Color.Transparent)
        )

        Spacer(Modifier.height(10.0.dp))
        Text(text = "Description",
            fontSize = 16.0.sp,
            color = Color(0xFF424B54),
            modifier = Modifier
                .padding(end = 170.dp)

        )
        Spacer(Modifier.height(10.0.dp))
        TextField(
            singleLine = true,
            value = desc,
            onValueChange = {
                if (it.length <= 150) createTeamViewModel.onCreateTeamChanged(name, contact, it, locAcc)
                else {
                    try {
                        toast1.cancel()
                    } catch (e: Exception) {
                        toast1 = Toast.makeText(
                            context,
                            "Cannot be more than 150 Characters",
                            Toast.LENGTH_SHORT
                        )
                    }
                    toast1.show()
                }
                if(it != "")
                    validateDesc(it)
            },
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(
                onDone = {keyboardController?.hide()}),
            textStyle = TextStyle.Default.copy(fontSize = 16.sp),
            label ={ Text(text="tell us something")},
            modifier = Modifier
                .padding(start = 60.dp, end = 60.dp)
                .background(color = Color.White)
                .fillMaxWidth()
                .height(150.dp)
                .padding(5.dp)
                .border(
                    border = ButtonDefaults.outlinedBorder,
                    shape = RoundedCornerShape(4.dp)
                ).focusRequester(focusRequester)
                .onFocusChanged {
                    if (!it.isFocused) {
                        with(sharedPref.edit()) {
                            putString("createTeam/desc", desc)
                            commit()
                        }
                    }
                },
            colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                unfocusedIndicatorColor = Color.Transparent)
        )
        Spacer(Modifier.height(10.0.dp))
        if (isErrorDesc) {
            Text(
                text = "Description too short",
                color = MaterialTheme.colors.error,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.padding(start = 16.dp)
            )
        }
        Button(
            onClick = {
                focusManager.clearFocus()

                if ( !lm.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                    try {
                        toast1.cancel()
                    } catch (e: Exception) {
                        toast1 = Toast.makeText(
                            context,
                            "Please activate your gps",
                            Toast.LENGTH_SHORT
                        )
                    }
                    toast1.show()
                }
                else{

                    if (fineLocationPermissionState.hasPermission) {
                        if (isConnected()) {
                            createTeamViewModel.onCreateTeamChanged(name, contact, desc, true)
                            createTeamViewModel.updateLocation(context, sharedPref)
                            navController.navigate("locationForCreateTeam")
                        } else {
                            try {
                                toast1.cancel()
                                toast1 = Toast.makeText(
                                    context,
                                    "There is not internet connection",
                                    Toast.LENGTH_SHORT
                                )
                            } catch (e: Exception) {
                                toast1 = Toast.makeText(
                                    context,
                                    "There is not internet connection",
                                    Toast.LENGTH_SHORT
                                )
                            }
                            toast1.show()
                        }
                    } else {
                        fineLocationPermissionState.launchPermissionRequest()
                    }
                }

            },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0XFF003893),
                contentColor = Color(0xFFFFFBFF)
            ),
        ) {
            Text(
                text = "Location",
                fontSize = 20.sp,
            )
        }
        Button(onClick = {
            focusManager.clearFocus()
            navController.navigate("choosePlayers?lat=${currentLocation.value.latitude}&lng=${currentLocation.value.longitude}")
        }, colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF4D61C4))) {
            Text(text = "Add a player", color = Color.White)
        }

        Spacer(Modifier.height(10.0.dp))
        Spacer(Modifier.height(41.0.dp))
        Button(
            onClick = {
                createTeamViewModel.resetValues(sharedPref)
                createTeamViewModel.setUpValues(mainActivity)
                createTeamViewModel.createATeam()
                navController.navigate("mainMenu"){
                    popUpTo(0)
                }
                      },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF950000),
                contentColor = Color(0xFFFFFBFF)),
            enabled = enableButton

            //colors = ButtonDefaults.outlinedButtonColors(contentColor = Color(0xFF4D61C4))

        ){Text(text = "Create",
            fontSize = 20.sp,
        )}

    }


}