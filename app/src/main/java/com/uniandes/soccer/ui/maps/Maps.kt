package com.uniandes.soccer.ui.maps

import android.location.Location
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asAndroidBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavHostController
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.MapView
import com.google.android.libraries.maps.model.LatLng
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.uniandes.soccer.R
import com.uniandes.soccer.ui.viewModel.CreateMatchViewModel

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun MapAddressPickerView(navController: NavHostController, createMatchViewModel: CreateMatchViewModel){
    Surface(color = MaterialTheme.colors.background) {
        val mapView = rememberMapViewWithLifecycle()
        val currentLocation = createMatchViewModel.location.collectAsState()
        var text by remember { createMatchViewModel.addressText }
        val context = LocalContext.current
        val keyboardController = LocalSoftwareKeyboardController.current


        Column(Modifier.fillMaxWidth()) {

            Box{
                TextField(
                    value = text,
                    onValueChange = {
                        text = it
                        if(!createMatchViewModel.isMapEditable.value)
                            createMatchViewModel.onTextChanged(context, text)
                    },
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                    keyboardActions = KeyboardActions(
                        onDone = {keyboardController?.hide()}),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(end = 80.dp),
                    enabled = !createMatchViewModel.isMapEditable.value,
                    colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.Transparent)
                )

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp)
                        .padding(bottom = 20.dp),
                    horizontalAlignment = Alignment.End
                ){
                    Button(
                        onClick = {
                            createMatchViewModel.isMapEditable.value = !createMatchViewModel.isMapEditable.value
                        }
                    ) {
                        Text(text = if(createMatchViewModel.isMapEditable.value) "Edit" else "Save")
                    }
                }
            }

            Box(modifier = Modifier.fillMaxHeight()){

                currentLocation.value.let {
                    if(createMatchViewModel.isMapEditable.value) {
                        try {
                            text = createMatchViewModel.getAddressFromLocation(context)
                        } catch (e: Exception) {
                            Toast.makeText(
                                context,
                                "There is not internet connection",
                                Toast.LENGTH_SHORT
                            ).show()
                            navController.navigate("createMatch")
                        }
                    }
                    MapViewContainer(createMatchViewModel.isMapEditable.value, mapView, createMatchViewModel, navController)
                }

                MapPinOverlay()
            }
        }
    }
}

@Composable
fun MapPinOverlay(){
    Column(modifier = Modifier.fillMaxWidth()) {
        Box(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(),
            contentAlignment = Alignment.BottomCenter
        ){
            Image(
                modifier = Modifier.size(50.dp),
                bitmap = ImageBitmap.imageResource(id = R.drawable.pin).asAndroidBitmap().asImageBitmap(),
                contentDescription = "Pin Image"
            )
        }
        Box(
            Modifier.weight(1f)
        ){}
    }
}

@Composable
private fun MapViewContainer(
    isEnabled: Boolean,
    mapView: MapView,
    createMatchViewModel: CreateMatchViewModel,
    navController: NavHostController,
) {
    val myTrace = Firebase.performance.newTrace("map_charging_time")
    myTrace.start()
    val context = LocalContext.current
    AndroidView(
        factory = { mapView }
    ) {
        try {

            mapView.getMapAsync { map ->
                myTrace.stop()
                map.uiSettings.setAllGesturesEnabled(isEnabled)

                val location = createMatchViewModel.location.value
                val position = LatLng(location.latitude, location.longitude)
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(position,  15f))

                map.setOnCameraIdleListener {
                    val cameraPosition = map.cameraPosition
                    createMatchViewModel.updateLocation(cameraPosition.target.latitude, cameraPosition.target.longitude)
                }
            }

        } catch (e: Exception) {
            Toast.makeText(
                context,
                "There is not internet connection",
                Toast.LENGTH_SHORT
            ).show()
            navController.navigate("createMatch")
        }
    }

}
