package com.uniandes.soccer.ui.viewModel

import android.location.Location
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.uniandes.soccer.data.cache.UserCache
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.domain.user.GetReferees
import com.uniandes.soccer.domain.user.GetUserById
import com.uniandes.soccer.ui.viewModel.utilities.LocationComparator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RefereeDetailViewModel @Inject constructor(
    private val getUserById: GetUserById,
): ViewModel() {

    val referee: MutableState<User?> = mutableStateOf(null)
    private val _isCharging = MutableLiveData<Boolean>()
    val isCharging: LiveData<Boolean> = _isCharging

    fun getRefereeById(id: String){
        viewModelScope.launch {
            try {
                referee.value = getUserById(id)
            }
            catch (e:Exception){
                val error = e.toString().split(":").toTypedArray()
                Log.d("Referee detail", "get the referee detail failed: ${error[1]}")
                e.printStackTrace()
            }

            _isCharging.value = false

        }
    }
}