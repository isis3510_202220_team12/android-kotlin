package com.uniandes.soccer.ui.view

import android.Manifest
import android.annotation.SuppressLint
import android.util.Patterns
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.material.*
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.uniandes.soccer.R
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import com.google.firebase.analytics.FirebaseAnalytics
import com.uniandes.soccer.ui.viewModel.LoginViewModel


private lateinit var toast1: Toast
@SuppressLint("PermissionLaunchedDuringComposition")
@OptIn(ExperimentalComposeUiApi::class, ExperimentalPermissionsApi::class)
@Composable
fun Log(navController: NavHostController, viewModel: LoginViewModel, analytics: FirebaseAnalytics) {

    LaunchedEffect(Unit){
        if (!viewModel.signIn.value!!) {
            viewModel.autoSignInUser()
        }
        if(viewModel.email.value!=null&&viewModel.pass.value!=null) {
            viewModel.onLoginChanged(viewModel.email.value!!, viewModel.pass.value!!)
        }
        viewModel.addAnalytics(analytics)
    }

    var isErrorPass by rememberSaveable { mutableStateOf(false) }
    var isErrorEmail by rememberSaveable { mutableStateOf(false) }

    fun validatePass(text: String) {
        isErrorPass = text.length<=6
    }

    fun validateEmail(text: String) {
        isErrorEmail  = !(Patterns.EMAIL_ADDRESS.matcher(text).matches())
    }

    val email: String by viewModel.email.observeAsState(initial = "")
    val pass: String by viewModel.pass.observeAsState(initial = "")
    val loginEnable: Boolean by viewModel.loginEnable.observeAsState(initial = false)
    val context = LocalContext.current
    val loginMessage: String by viewModel.loginResult
    val keyboardController = LocalSoftwareKeyboardController.current
    val logged: Boolean by viewModel.logged.observeAsState(initial = false)

    val externalStoragePermissionState = rememberPermissionState(
        permission = Manifest.permission.READ_EXTERNAL_STORAGE
    )

    fun login(){
        if(loginMessage!=""){
            try {
                toast1.cancel()
                toast1 = Toast.makeText(
                    context,
                    loginMessage,
                    Toast.LENGTH_SHORT
                )
            } catch (e: Exception) {
                toast1 = Toast.makeText(
                    context,
                    loginMessage,
                    Toast.LENGTH_SHORT
                )
            }
            toast1.show()
            viewModel.cleanLoginMessage()
        }
        if(logged){
            viewModel.clearLoggedStatus()
            navController.navigate("mainMenu"){
                popUpTo(0)
            }
        }
    }

    login()


    Column(

        modifier = Modifier
            .background(Color.White)
            .fillMaxSize().padding(top = 80.0.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        Image(
            painterResource(R.drawable.fubol),
            contentDescription = "",
            contentScale = ContentScale.FillHeight,
            modifier = Modifier.size(width = 239.dp, height = 144.dp)
        )
        Spacer(Modifier.height(26.0.dp))
        Text(text = "Email",
            fontSize = 16.0.sp,
            color = Color(0xFF424B54),
            modifier = Modifier
                .padding(end = 160.dp)
        )
        Spacer(Modifier.height(10.0.dp))
        TextField(
            singleLine = true,
            value = email,
            onValueChange = {
                if (it.length <= 35) viewModel.onLoginChanged(it, pass)
                else {
                    try {
                        toast1.cancel()
                    } catch (e: Exception) {
                        toast1 = Toast.makeText(
                            context,
                            "Cannot be more than 35 Characters",
                            Toast.LENGTH_SHORT
                        )
                    }
                    toast1.show()
                }
                if(it != "")
                    validateEmail(it)
            },
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(
                onDone = {keyboardController?.hide()}),
            textStyle = TextStyle.Default.copy(fontSize = 16.sp),
            modifier = Modifier.size(width = 200.dp, height = 56.dp).border(
                border = ButtonDefaults.outlinedBorder,
                shape = RoundedCornerShape(4.dp)
            ).fillMaxSize(),
            colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                unfocusedIndicatorColor = Color.Transparent)
        )
        if (isErrorEmail) {
            Text(
                text = "Incorrect format of email",
                color = MaterialTheme.colors.error,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.padding(start = 16.dp)
            )
        }
        Spacer(Modifier.height(10.0.dp))
        Text(text = "Password",
            fontSize = 16.0.sp,
            color = Color(0xFF424B54),
            modifier = Modifier
                .padding(end = 127.dp)
        )
        Spacer(Modifier.height(10.0.dp))
        TextField(
            singleLine = true,
            value = pass,
            onValueChange = {
                if (it.length <= 20) viewModel.onLoginChanged(email, it)
                else {
                    try {
                        toast1.cancel()
                    } catch (e: Exception) {
                        toast1 = Toast.makeText(
                            context,
                            "Cannot be more than 35 Characters",
                            Toast.LENGTH_SHORT
                        )
                    }
                    toast1.show()

                }
                if(it != "")
                    validatePass(it)
            },
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done, keyboardType = KeyboardType.Password),
            keyboardActions = KeyboardActions(
                onDone = {keyboardController?.hide()}),
            visualTransformation = PasswordVisualTransformation(),
            textStyle = TextStyle.Default.copy(fontSize = 16.sp),
            modifier = Modifier.size(width = 200.dp, height = 56.dp).border(
                border = ButtonDefaults.outlinedBorder,
                shape = RoundedCornerShape(4.dp)
            ),
            colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White,
                unfocusedIndicatorColor = Color.Transparent)
        )
        if (isErrorPass) {
            Text(
                text = "Password too short",
                color = MaterialTheme.colors.error,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.padding(start = 16.dp)
            )
        }
        Spacer(Modifier.height(41.0.dp))

        Button(
            onClick = {
                if (externalStoragePermissionState.hasPermission) {
                    if (isConnected()) {
                        viewModel.signInUser()
                        login()
                        try {
                            toast1.cancel()
                            toast1 = Toast.makeText(
                                context,
                                "loggin in...",
                                Toast.LENGTH_SHORT
                            )
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "loggin in...",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    } else {
                        try {
                            toast1.cancel()
                            toast1 = Toast.makeText(
                                context,
                                "There is not internet connection",
                                Toast.LENGTH_SHORT
                            )
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "There is not internet connection",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                    }
                }
                else{
                    externalStoragePermissionState.launchPermissionRequest()
                }
            },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF4D61C4),
                contentColor = Color(0xFFFFFBFF)),
            enabled = loginEnable

        ){Text(text = "Login",
            fontSize = 20.sp,
        )}

        Spacer(Modifier.height(48.0.dp))

        Text(text = "OR",
            fontSize = 20.sp,
            color = Color(0xFF424B54)
        )

        Spacer(Modifier.height(48.0.dp))

        Button(
            onClick = {
                if (externalStoragePermissionState.hasPermission) {
                    navController.navigate("registerSelection")
                }
                else{
                    externalStoragePermissionState.launchPermissionRequest()
                }
                      },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF003893),
                contentColor = Color(0xFFFFFBFF),)
        ){Text(text = "Register",
            fontSize = 20.sp,
        )}

        Spacer(Modifier.height(53.0.dp))

        Text(text = "I forgot my password",
            fontSize = 14.sp,
            color = Color(0xFF23395B)
        )
    }
}