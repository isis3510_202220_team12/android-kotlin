package com.uniandes.soccer.ui.view

import AutoSizeText
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.gowtham.ratingbar.RatingBar
import com.uniandes.soccer.R
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.ui.view.Reusable.BottomNavBar
import com.uniandes.soccer.ui.view.Reusable.LoadingScreen
import com.uniandes.soccer.ui.viewModel.RefereeDetailViewModel

@Composable
fun RefereeDetail(
    navController: NavHostController,
    viewModel: RefereeDetailViewModel,
    refereeId: String?
) {

    LaunchedEffect(Unit) {
        if (refereeId != null) {
            viewModel.getRefereeById(refereeId)
        }
    }
    val referee: User? by viewModel.referee
    val isCharging: Boolean by viewModel.isCharging.observeAsState(initial = true)




    if (!isCharging) {

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.White),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Column(
                modifier = Modifier
                    .background(Color.White)
                    .verticalScroll(rememberScrollState())
                    .weight(weight = 1f, fill = false),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Spacer(Modifier.height(22.0.dp))
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(end = 22.dp)
                        .height(50.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.Top
                ) {
                    Spacer(modifier = Modifier.weight(1f))
                    Column(horizontalAlignment = Alignment.End) {
                        Image(
                            painterResource(R.drawable.menu),
                            contentDescription = "",
                            contentScale = ContentScale.FillHeight,
                            modifier = Modifier
                                .fillMaxHeight()
                        )
                    }

                }
                Image(
                    painterResource(R.drawable.fubol),
                    contentDescription = "",
                    contentScale = ContentScale.FillHeight,
                    modifier = Modifier.size(width = 239.dp, height = 144.dp)
                )
                Spacer(Modifier.height(19.0.dp))
                Text(
                    text = referee?.name.toString(),                                    //Reemplazar por name
                    fontSize = 18.0.sp,
                    fontWeight = FontWeight.Bold,
                    color = Color(0xFF121C2B)
                )
                Spacer(Modifier.height(18.0.dp))
                Divider(
                    color = Color(0xFFFF524A), thickness = 2.dp,
                    modifier = Modifier.padding(end = 40.0.dp, start = 40.0.dp)
                        .background(Color(0xFFFF524A), RoundedCornerShape(16.dp))
                )
                Spacer(Modifier.height(36.0.dp))
                Row {
                    Spacer(modifier = Modifier.weight(1.2f))
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {

                        Text(
                            text = "Age",
                            fontSize = 16.0.sp,
                            fontWeight = FontWeight.Bold
                        )
                        Spacer(Modifier.height(5.0.dp))
                        Box(
                            modifier = Modifier.background(
                                Color(0xFFFFE5E5),
                                RoundedCornerShape(6.dp)
                            )
                                .height(60.dp)
                                .widthIn(75.dp, 100.dp)
                        ) {
                            AutoSizeText(
                                text = referee?.age.toString(),                                    //Reemplazar por age
                                fontSize = 14.0.sp,
                                color = Color(0xFF950000),
                                modifier = Modifier.align(Alignment.Center)
                            )
                        }
                    }
                    Spacer(modifier = Modifier.weight(1f))
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {

                        Text(
                            text = "Location",
                            fontSize = 16.0.sp,
                            fontWeight = FontWeight.Bold,
                            color = Color(0xFF121C2B)
                        )
                        Spacer(Modifier.height(5.0.dp))
                        Box(
                            modifier = Modifier.background(
                                Color(0xFFFFE5E5),
                                RoundedCornerShape(6.dp)
                            )
                                .height(60.dp)
                                .widthIn(75.dp, 100.dp)
                        ) {
                            AutoSizeText(
                                text = referee?.locationName.toString(),                     //Reemplazar por location
                                fontSize = 14.0.sp,
                                textAlign = TextAlign.Center,
                                color = Color(0xFF950000),
                                modifier = Modifier.align(Alignment.Center)
                            )
                        }
                    }
                    Spacer(modifier = Modifier.weight(1f))
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {

                        Text(
                            text = "Sports",
                            fontSize = 16.0.sp,
                            fontWeight = FontWeight.Bold,
                            color = Color(0xFF121C2B)
                        )
                        Spacer(Modifier.height(5.0.dp))
                        Box(
                            modifier = Modifier.background(
                                Color(0xFFFFE5E5),
                                RoundedCornerShape(6.dp)
                            )
                                .height(60.dp)
                                .widthIn(75.dp, 100.dp)
                        ) {
                            AutoSizeText(
                                text = referee?.description.toString(),                               //Reemplazar por sports
                                fontSize = 14.0.sp,
                                color = Color(0xFF950000),
                                modifier = Modifier.align(Alignment.Center)
                            )
                        }
                    }
                    Spacer(modifier = Modifier.weight(1.2f))
                }
                Spacer(Modifier.height(39.0.dp))
                Text(
                    text = "Rating",
                    fontSize = 16.0.sp,
                    fontWeight = FontWeight.Bold,
                    color = Color(0xFF121C2B)
                )
                Spacer(Modifier.height(8.0.dp))
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 40.dp, end = 40.dp)
                        .background(Color(0xFFFFE5E5), RoundedCornerShape(6.dp))
                        .padding(top = 15.dp, bottom = 15.dp, start = 30.dp, end = 30.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    var rate =
                        referee?.rating?.toFloat()                                                //Reemplazar por rating
                    if (rate != null) {
                        RatingBar(
                            value = rate,
                            onValueChange = {},
                            onRatingChanged = {},
                            modifier = Modifier.height(25.dp)

                        )
                    }
                    Text(
                        text = "$rate out of 5",
                        fontSize = 14.0.sp,
                        color = Color(0xFF950000)
                    )
                }
                Spacer(Modifier.height(52.0.dp))
                Button(
                    onClick = {
                        navController.previousBackStackEntry
                            ?.savedStateHandle
                            ?.set("refereeId", referee?.id)

                        navController.popBackStack()
                    },
                    shape = RoundedCornerShape(50),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Color(0xFF950000),
                        contentColor = Color(0xFFFFFBFF)
                    ),
                    modifier = Modifier.width(128.dp)

                ) {
                    Text(
                        text = "Add",
                        fontSize = 20.sp,
                    )
                }
            }
            BottomNavBar(modifier = Modifier.padding(top = 0.dp), navController)
        }
    }else{
        Column(modifier= Modifier
            .fillMaxHeight()
            .padding(bottom = 20.dp),
            verticalArrangement = Arrangement.Center
        ) {
            LoadingScreen()
        }
    }
}