package com.uniandes.soccer.ui.viewModel.utilities
import android.annotation.SuppressLint
import android.provider.UserDictionary
import android.util.Log
import com.londogard.nlp.stemmer.Stemmer
import com.londogard.nlp.stopwords.Stopwords
import com.londogard.nlp.tokenizer.SimpleTokenizer
import com.londogard.nlp.utils.LanguageSupport
import javax.inject.Inject


class CompareStringsAlgorithm @Inject constructor(

){
    fun calculateSimilarity(
        text1: String,
        text2: String
        ): Int {
        var result = 0
        if(text1 != "" && text2!=""){
            val tokensText1 = stemTokens(filterStopWords(tokenizeText(text1.lowercase())))
            Log.d("search", tokensText1.toString())
            val tokensText2 = stemTokens(filterStopWords(tokenizeText(text2.lowercase())))
            Log.d("user description", tokensText2.toString())
            tokensText1.forEach { toCompare ->
                if(tokensText2.contains(toCompare)) result++
            }
        }
        return result
    }

    private fun stemTokens(
        tokenList: List<String>,
    ): List<String> {
        val stemmedTokens: MutableList<String> = ArrayList()
        tokenList.forEach { token->
                stemmedTokens.add(Stemmer.stem(token, LanguageSupport.en))
            }
        return stemmedTokens
    }


    private fun filterStopWords(
        tokenList: List<String>,
    ): List<String> {
        val stopWords = listOf<String>("ourselves", "hers", "between", "yourself", "but", "again", "there", "about", "once", "during", "out", "very", "having", "with",
            "they", "own", "an", "be", "some", "for", "do", "its", "yours", "such", "into", "of", "most", "itself", "other", "off", "is", "s", "am", "or", "who", "as",
            "from", "him", "each", "the", "themselves", "until", "below", "are", "we", "these", "your", "his", "through", "don", "nor", "me", "were", "her", "more", "himself",
            "this", "down", "should", "our", "their", "while", "above", "both", "up", "to", "ours", "had", "she", "all", "no", "when", "at", "any", "before", "them", "same", "and",
            "been", "have", "in", "will", "on", "does", "yourselves", "then", "that", "because", "what", "over", "why", "so", "can", "did", "not", "now", "under", "he", "you", "herself",
            "has", "just", "where", "too", "only", "myself", "which", "those", "i", "after", "few", "whom", "t", "being", "if", "theirs", "my", "against", "a", "by", "doing", "it", "how",
            "further", "was", "here", "than")
        val filteredSentenceWords: MutableList<String> = ArrayList()
        for (wordToken in tokenList) {
            if (!stopWords.contains(wordToken)) {
                filteredSentenceWords.add(wordToken)
            }
        }
        return filteredSentenceWords
    }

    private fun tokenizeText(
        text1: String
    ): List<String> {
        val wordTokens: MutableList<String> = ArrayList()
        SimpleTokenizerFubol().split(text1).forEach { word ->
                wordTokens.add(word)
        }
        return wordTokens.distinct().toList()
    }



}