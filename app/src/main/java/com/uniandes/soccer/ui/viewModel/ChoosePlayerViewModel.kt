package com.uniandes.soccer.ui.viewModel

import android.os.Bundle
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.cache.UserCache
import com.uniandes.soccer.data.model.match.Match
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.domain.user.GetPlayers
import com.uniandes.soccer.domain.user.GetReferees
import com.uniandes.soccer.ui.viewModel.utilities.LocationComparator
import com.uniandes.soccer.ui.viewModel.utilities.TextsComparator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.util.ArrayList
import javax.inject.Inject

@HiltViewModel
class ChoosePlayerViewModel @Inject constructor(
 private val getPlayersUseCase: GetPlayers,
 private val locationComparator: LocationComparator,
 private val textsComparator: TextsComparator,
 private val userCache: UserCache,
): ViewModel() {
    lateinit var firebaseAnalytics: FirebaseAnalytics

    val playersList: MutableState<List<User>> = mutableStateOf(listOf())
    val selectedPlayers: MutableState<List<String>> = mutableStateOf(listOf())
    private val _isCharging = MutableLiveData<Boolean>()
    val isCharging: LiveData<Boolean> = _isCharging
    private val _search = MutableLiveData<String>()
    val search : LiveData<String> = _search
    private val _aSearch = MutableLiveData<String>("")
    val aSearch : LiveData<String> = _aSearch
    val location: MutableState<GeoPoint> = mutableStateOf(userCache.location)
    private val _initialList: MutableState<List<User>> = mutableStateOf(listOf())
    val _contentText: MutableState <String> = mutableStateOf("")

    val showDialog: MutableState <Boolean> = mutableStateOf(false)


    fun getPlayers(){
        viewModelScope.launch {
            try {
                _initialList.value = getPlayersUseCase().sortedWith(textsComparator.then(locationComparator))
                playersList.value = _initialList.value
            }
            catch (e:Exception){
                val error = e.toString().split(":").toTypedArray()
                Log.d("Players list", "get the players list failed: ")
                e.printStackTrace()
            }

            _isCharging.value = false

        }
    }

    fun setLocation(lat: Float, lng: Float){
        location.value = GeoPoint(lat.toDouble(), lng.toDouble())
    }

    fun orderPlayers() {
        viewModelScope.launch {
            launch(Dispatchers.Default) {
                val parameters = Bundle().apply {
                    this.putString("user_id", userCache.id)
                    this.putString("user_name", userCache.name)
                    this.putString("user_search_text", _aSearch.value)
                }
                println(_aSearch.value)
                textsComparator.setSearch(_aSearch.value!!)
                locationComparator.setActualLocation(location.value!!)

                playersList.value =
                    playersList.value.sortedWith(textsComparator.then(locationComparator))
                _contentText.value = "The player that fits with your description and is closest to you is: " +
                        playersList.value[0].name
                onOpenDialog()
                var p = ""
                if (playersList.value.isNotEmpty()) {
                    val a = playersList.value[0]
                    p = "Id: " + a.id + " / Player_name: " + a.name
                }

                parameters.putString("player", p)
                firebaseAnalytics.logEvent("Best_fit_player", parameters)
            }
        }
    }


    fun onSearchBarChanged(search:String) {
        _search.value = search
        searchPlayer(search)
    }

    fun onAdvancedSearchBarChanged(search:String) {
        _aSearch.value = search
    }

    private fun searchPlayer(search:String){
        playersList.value = _initialList.value.filter {
            it.name.contains(search, ignoreCase = true) ||
                    search.contains(it.name, ignoreCase = true)
        }
    }

    fun addAnalytics(analytics: FirebaseAnalytics) {
        firebaseAnalytics= analytics
    }

    fun addPlayer(playerId: String) {
        if(!selectedPlayers.value.contains(playerId)) {
            val aux = mutableListOf<String>()
            aux.addAll(selectedPlayers.value)
            aux.add(playerId)
            selectedPlayers.value = aux
        }
    }

    fun onOpenDialog() {
        showDialog.value = true
    }

    fun onDialogConfirm() {
        showDialog.value = false
    }

    fun onContentTextChange(text: String) {
        _contentText.value = text
    }

    fun onDialogDismiss() {
        showDialog.value = false
    }

}