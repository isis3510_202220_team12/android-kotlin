package com.uniandes.soccer.ui.view

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.google.firebase.analytics.FirebaseAnalytics
import com.uniandes.soccer.R
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.ui.view.Reusable.BottomNavBar
import com.uniandes.soccer.ui.view.Reusable.ConnectionAlertDialog
import com.uniandes.soccer.ui.view.Reusable.LoadingScreen
import com.uniandes.soccer.ui.viewModel.ChoosePlayerViewModel


private lateinit var toast1: Toast

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ChoosePlayer(
    navController: NavHostController, choosePlayerViewModel: ChoosePlayerViewModel,
    analytics: FirebaseAnalytics, lat: Float?, lng: Float?
) {

    LaunchedEffect(Unit) {
        if (navController.currentBackStackEntry?.savedStateHandle?.contains("playerId") == true) {
            navController.previousBackStackEntry
                ?.savedStateHandle
                ?.set("playerId", navController.currentBackStackEntry!!.savedStateHandle.get<String>(
                    "playerId"
                ) ?: "")

            navController.popBackStack()
        }
        if (lat != null && lng != null) {
            choosePlayerViewModel.setLocation(lat,lng)
        }
        choosePlayerViewModel.addAnalytics(analytics)
        choosePlayerViewModel.getPlayers()
        choosePlayerViewModel.onSearchBarChanged("")
    }

    val search: String by choosePlayerViewModel.search.observeAsState(initial = "")
    val aSearch: String by choosePlayerViewModel.aSearch.observeAsState(initial = "")
    val playersList: List<User> by choosePlayerViewModel.playersList
    val context = LocalContext.current
    val keyboardController = LocalSoftwareKeyboardController.current
    val isCharging: Boolean by choosePlayerViewModel.isCharging.observeAsState(initial = true)
    val focusRequester = remember { FocusRequester() }
    val focusManager = LocalFocusManager.current
    val contentTextDialog: String by choosePlayerViewModel._contentText
    val showDialogVisible: Boolean by choosePlayerViewModel.showDialog



    Column(
        modifier = Modifier
            .background(Color.White)
            .fillMaxSize()
            .fillMaxHeight()
            .padding(bottom = 55.dp)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = Color.White),
            ) { focusManager.clearFocus() },
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Surface(
            elevation= 3.dp
        ) {
            Column(verticalArrangement = Arrangement.SpaceBetween)
            {

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(86.dp)
                        .focusRequester(focusRequester)
                        .padding(start = 10.dp, top = 7.dp, bottom = 7.dp)
                ) {
                    TextField(
                        modifier = Modifier
                            .fillMaxWidth(0.8f)
                            .fillMaxHeight()
                            .padding(10.dp),
                        singleLine = true,
                        value = search,
                        onValueChange = {
                            if (it.length <= 20) choosePlayerViewModel.onSearchBarChanged(it)
                            else {
                                try {
                                    toast1.cancel()
                                } catch (e: Exception) {         // invisible if exception
                                    toast1 = Toast.makeText(
                                        context,
                                        "Cannot be more than 20 Characters",
                                        Toast.LENGTH_SHORT
                                    );
                                }
                                toast1.show();
                            }
                        },
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
                        keyboardActions = KeyboardActions(
                            onSearch = {
                                keyboardController?.hide(); choosePlayerViewModel.onSearchBarChanged(
                                search
                            )
                            }),
                        label = { Text(text = "Search") },
                        leadingIcon = {
                            Icon(Icons.Filled.Search, "")
                        },
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = Color(0xFFFCF0F0),
                            unfocusedIndicatorColor = Color.Transparent
                        ),
                        shape = RoundedCornerShape(8.dp)
                    )

                    Image(
                        painterResource(R.drawable.menu),
                        contentDescription = "",
                        contentScale = ContentScale.FillHeight,
                        modifier = Modifier
                            .fillMaxHeight(0.8f)
                            .padding(top = 11.dp, start = 12.dp)
                            .clickable(
                                enabled = true,
                                onClickLabel = "Clickable image",
                                onClick = {
                                    navController.navigate("mainMenu")
                                }
                            ),


                        )

                }
                Spacer(Modifier.height(10.0.dp))
                Text(
                    text = "Advanced Search",
                    fontSize = 16.0.sp,
                    fontWeight = FontWeight.Bold,
                    color = Color(0xFF003893),
                    modifier = Modifier.padding(start=8.dp)
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(106.dp)
                        .focusRequester(focusRequester)
                        .padding(start = 10.dp, top = 7.dp, bottom = 7.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    TextField(
                        modifier = Modifier
                            .fillMaxWidth(0.8f)
                            .fillMaxHeight()
                            .padding(10.dp),
                        singleLine = false,
                        value = aSearch,
                        onValueChange = {
                            if (it.length <= 200) choosePlayerViewModel.onAdvancedSearchBarChanged(it)
                            else {
                                try {
                                    toast1.cancel()
                                } catch (e: Exception) {         // invisible if exception
                                    toast1 = Toast.makeText(
                                        context,
                                        "Cannot be more than 200 Characters",
                                        Toast.LENGTH_SHORT
                                    );
                                }
                                toast1.show();
                            }
                        },
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
                        keyboardActions = KeyboardActions(
                            onSearch = {
                                keyboardController?.hide()
                                choosePlayerViewModel.onAdvancedSearchBarChanged(aSearch)
                                choosePlayerViewModel.orderPlayers()
                            }),
                        placeholder = { Text(text = "Describe the player that you want: 'a fast and tall striker'") },
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = Color(0xFFFCF0F0),
                            unfocusedIndicatorColor = Color.Transparent
                        ),
                        shape = RoundedCornerShape(8.dp)
                    )

                    Button(
                        onClick = {
                            choosePlayerViewModel.orderPlayers()
                        },
                        shape = RoundedCornerShape(30),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Color(0xFF4D61C4),
                            contentColor = Color(0xFFFFFBFF)
                        ),
                        enabled = true

                    ) {
                        Icon(
                            modifier = Modifier
                                .fillMaxSize(0.6F),
                            imageVector = Icons.Default.Search,
                            contentDescription = "",
                            tint = Color(0xFFFFFBFF),
                        )
                    }

                }
            }
        }

        Spacer(Modifier.height(10.0.dp))
        if(isConnected()) {
            if (!isCharging) {
                LazyColumn(
                    modifier = Modifier
                ) {
                    items(playersList) { player ->

                        playerItem(player = player, navController = navController, choosePlayerViewModel, context)
                    }
                }
            } else {
                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .padding(bottom = 20.dp),
                    verticalArrangement = Arrangement.Center
                ) {
                    LoadingScreen()
                }
            }
        } else {
            try {
                toast1.cancel()
                toast1 = Toast.makeText(
                    context,
                    "There is not internet connection",
                    Toast.LENGTH_SHORT
                )
            } catch (e: Exception) {
                toast1 = Toast.makeText(
                    context,
                    "There is not internet connection",
                    Toast.LENGTH_SHORT
                )
            }
            toast1.show()
        }

    }

    ConnectionAlertDialog(
        title = "Player List",
        contentText=contentTextDialog,
        show = showDialogVisible,
        onDismiss = choosePlayerViewModel::onDialogDismiss,
        onConfirm = choosePlayerViewModel::onDialogConfirm
    )

    Column(modifier = Modifier
        .fillMaxSize(),
        verticalArrangement = Arrangement.Bottom,
    ) {
        BottomNavBar(modifier = Modifier.weight(1f), navController)
    }


}




@Composable
fun playerItem(player: User, navController: NavHostController,
               choosePlayerViewModel: ChoosePlayerViewModel, context: Context)
{
    Row(modifier = Modifier
        .border(BorderStroke(1.dp, Color(0xFFD1D1D1)), shape = RoundedCornerShape(5.dp))
        .padding(top = 5.0.dp, start = 5.0.dp, end = 5.0.dp)
        .height(60.dp)
        .fillMaxWidth(0.9f),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center)
    {
        Column(modifier = Modifier
            .weight(1f)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = Color.Red),
            ) { navController.navigate("playerDetail/${player.id}") },
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center){
            Image(
                painterResource(R.drawable.player),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .padding(start = 5.dp)
                    .fillMaxHeight(1f)
            )
        }

        Column(modifier = Modifier
            .weight(1f)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = Color.Red),
            ) { navController.navigate("playerDetail/${player.id}") },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center) {
            Text(
                text = "${player.name}",
                fontSize = 18.sp,
                color = Color.Black,
                modifier= Modifier
                    .fillMaxWidth(),
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold
            )
        }

        Column(modifier = Modifier.weight(1f),
            horizontalAlignment = Alignment.End,
            verticalArrangement = Arrangement.Center) {
            Image(
                painterResource(R.drawable.plus),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .padding(end = 5.dp)
                    .fillMaxHeight(0.8f)
                    .clickable {
                        navController.previousBackStackEntry
                            ?.savedStateHandle
                            ?.set("playerId", player.id)

                        try {
                            toast1.cancel()
                            toast1 = Toast.makeText(
                                context,
                                "the player has been added",
                                Toast.LENGTH_SHORT
                            )
                        } catch (e: Exception) {
                            toast1 = Toast.makeText(
                                context,
                                "the player has been added",
                                Toast.LENGTH_SHORT
                            )
                        }
                        toast1.show()
                        navController.popBackStack()
                    }
            )
        }

    }
    Spacer(Modifier.height(12.0.dp))
}