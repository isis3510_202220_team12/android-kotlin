package com.uniandes.soccer.ui.viewModel.utilities

import android.location.Location
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.user.User
import javax.inject.Inject

class LocationComparator @Inject constructor(
): Comparator<User>{
    private var actualLocation: GeoPoint = GeoPoint(0.0,0.0)
    override fun compare(u1: User?, u2: User?): Int {
        if (u1==null||u2==null){
            return 0
        }else {
            var dis1 = FloatArray(1)
            var dis2 = FloatArray(1)
            Location.distanceBetween(
                actualLocation.latitude, actualLocation.longitude,
                u1.location.latitude, u1.location.longitude, dis1
            )
            Location.distanceBetween(
                actualLocation.latitude, actualLocation.longitude,
                u2.location.latitude, u2.location.longitude, dis2
            )
            return dis1[0].compareTo(dis2[0])
        }
    }

    fun setActualLocation(actualLocation: GeoPoint){
        this.actualLocation = actualLocation
    }

}