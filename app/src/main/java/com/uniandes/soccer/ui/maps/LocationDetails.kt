package com.uniandes.soccer.ui.maps

data class LocationDetails(val longitude : String, val latitude : String) {

}
