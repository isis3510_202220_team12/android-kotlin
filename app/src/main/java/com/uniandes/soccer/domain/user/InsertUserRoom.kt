package com.uniandes.soccer.domain.user

import com.uniandes.soccer.data.model.roomDB.toDatabase
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.data.repository.UserRepository
import javax.inject.Inject

class InsertUserRoom @Inject constructor(
    private val userRepository: UserRepository,
){
    suspend operator fun invoke(user: User) {
        userRepository.insertUserRoom(user.toDatabase())
    }
}