package com.uniandes.soccer.domain.authentication

import com.google.firebase.auth.FirebaseUser
import com.uniandes.soccer.data.repository.AuthRepository
import javax.inject.Inject

class GetUser @Inject constructor(
    private val authRepository: AuthRepository
){
    suspend operator fun invoke(): FirebaseUser? {
        return authRepository.getCurrentUser()
    }
}