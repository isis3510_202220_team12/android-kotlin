package com.uniandes.soccer.domain.match

import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.repository.MatchRepository
import javax.inject.Inject

class CreateMatch @Inject constructor(
    private val matchRepository: MatchRepository,
){
    suspend operator fun invoke( dateMatch: Timestamp, referee: String, location: GeoPoint, team1: Team, locationName:String) {
        return matchRepository.createMatch(dateMatch, referee, location, team1, locationName)
    }
}