package com.uniandes.soccer.domain.match

import com.uniandes.soccer.data.model.match.Match
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.repository.MatchRepository
import com.uniandes.soccer.data.repository.TeamRepository
import javax.inject.Inject

class GetJoinableMatches @Inject constructor(
    private val teamRepository: TeamRepository,
    private val matchRepository: MatchRepository
){
    suspend operator fun invoke(teamId: String): List<Match>{
        val team: Team =teamRepository.getTeamByID(teamId = teamId)
        return matchRepository.getJoinableMatches(team.id)
    }
}