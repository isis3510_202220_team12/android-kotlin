package com.uniandes.soccer.domain.team

import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.repository.TeamRepository
import javax.inject.Inject

class GetUsersTeams @Inject constructor(
    private val teamRepository: TeamRepository
){
    suspend operator fun invoke(userId:String): List<Team> {
        return teamRepository.getUsersTeams(userId)
    }
}