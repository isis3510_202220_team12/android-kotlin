package com.uniandes.soccer.domain.user

import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.data.repository.UserRepository
import javax.inject.Inject

class DeleteUserRoom @Inject constructor(
    private val userRepository: UserRepository
    ){
        suspend operator fun invoke(){
            userRepository.deleteUserRoom()
        }
    }