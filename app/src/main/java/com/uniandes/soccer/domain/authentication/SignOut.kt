package com.uniandes.soccer.domain.authentication

import com.uniandes.soccer.data.repository.AuthRepository
import javax.inject.Inject

class SignOut  @Inject constructor(
    private val authRepository: AuthRepository
) {
    suspend operator fun invoke() {
        return authRepository.signOut()
    }
}