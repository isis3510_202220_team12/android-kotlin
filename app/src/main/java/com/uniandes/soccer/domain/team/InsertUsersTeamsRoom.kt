package com.uniandes.soccer.domain.team

import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.roomDB.TeamRoom
import com.uniandes.soccer.data.model.roomDB.toDatabase
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.repository.TeamRepository
import javax.inject.Inject

class InsertUsersTeamsRoom @Inject constructor(
    private val teamRepository: TeamRepository,
){
    suspend operator fun invoke(teams: List<Team>) {
        val teamsRoom = mutableListOf<TeamRoom>()
        for (team in teams) {
            teamsRoom.add(team.toDatabase())
        }
        teamRepository.insertTeamsRoom(teamsRoom)
    }
}