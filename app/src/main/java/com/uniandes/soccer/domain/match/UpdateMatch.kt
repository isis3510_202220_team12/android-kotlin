package com.uniandes.soccer.domain.match

import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.repository.MatchRepository
import com.uniandes.soccer.data.repository.TeamRepository
import javax.inject.Inject

class UpdateMatch @Inject constructor(
    private val matchRepository: MatchRepository,
    private val teamRepository: TeamRepository
        ){
    suspend operator fun invoke(matchId: String, dateMatch: Timestamp, referee:String, location: GeoPoint, team1: String, team2:String, locationName:String) {
        val team1F: Team =teamRepository.getTeamByID(teamId = team1)
        val team2F: Team =teamRepository.getTeamByID(teamId = team2)
        return matchRepository.updateMatch(matchId, dateMatch, referee, location, team1F, team2F, locationName)
    }
}