package com.uniandes.soccer.domain.user

import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.data.repository.UserRepository
import javax.inject.Inject

class GetUserRoom  @Inject constructor(
    private val userRepository: UserRepository,
){
    suspend operator fun invoke(): User? {
        val userRoom = userRepository.getUserRoom()
        val aux = userRoom?.let {
            User(
                it.id, userRoom.email, userRoom.password, userRoom.locationName, userRoom.age,
                userRoom.location, userRoom.isReferee, userRoom.teams, userRoom.photo, userRoom.rating,
                userRoom.description, userRoom.locationName)
        }
        return aux
    }
}