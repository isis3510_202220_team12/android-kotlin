package com.uniandes.soccer.domain.user

import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.data.repository.UserRepository
import javax.inject.Inject

class createUser @Inject constructor(
    private val userRepository: UserRepository,
){
    suspend operator fun invoke(email:String,password: String, name: String, age: Int,
                                location: GeoPoint, isReferee: Boolean, teams:List<String>?, photo:String?, rating:Double?, description: String?, locationName: String): User? {
        return userRepository.getUserById(
            userRepository.createUser(email,password, name, age, location, isReferee, teams, photo, rating, description, locationName)
        )
    }
}