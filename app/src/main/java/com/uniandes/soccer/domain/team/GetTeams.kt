package com.uniandes.soccer.domain.team

import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.repository.TeamRepository
import javax.inject.Inject

class GetTeams @Inject constructor(
    private val teamRepository: TeamRepository,
){
    suspend operator fun invoke(): List<Team> {
        return teamRepository.getTeams()
    }
}