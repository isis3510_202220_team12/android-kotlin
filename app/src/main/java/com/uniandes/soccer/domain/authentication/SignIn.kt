package com.uniandes.soccer.domain.authentication

import com.google.firebase.auth.FirebaseUser
import com.uniandes.soccer.data.repository.AuthRepository
import javax.inject.Inject

class SignIn @Inject constructor(
    private val authRepository: AuthRepository
) {
    suspend operator fun invoke(email: String, password: String): FirebaseUser? {
        return authRepository.signInWithEmailPassword(email, password)
    }
}