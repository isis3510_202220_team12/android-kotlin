package com.uniandes.soccer.domain.user

import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.data.repository.UserRepository

class UpdateUser (
    private val userRepository: UserRepository,
    ){
        suspend operator fun invoke(id:String, email:String, password: String, name: String, age: Int,
                                    location: GeoPoint, isReferee: Boolean, teams:List<String>?, photo:String?, rating:Double?, description: String?, locationName: String): User? {
            userRepository.updateUser(id, name, password, name, age, location, isReferee, teams, photo, rating, description, locationName )
            return userRepository.getUserById(id)
        }
    }