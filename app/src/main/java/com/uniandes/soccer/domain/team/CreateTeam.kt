package com.uniandes.soccer.domain.team

import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.repository.TeamRepository
import javax.inject.Inject

class CreateTeam @Inject constructor(
    private val teamRepository: TeamRepository,
){
    suspend operator fun invoke(name:String,captain: String, commentaries: String, ranking: Int, players: List<String>, location: GeoPoint?) {
        return teamRepository.createTeam(name,captain, commentaries, ranking, players, location)
    }
}