package com.uniandes.soccer.domain.user

import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.data.repository.UserRepository
import javax.inject.Inject

class GetUserByEmail  @Inject constructor(
    private val userRepository: UserRepository,
){
    suspend operator fun invoke(email: String): User? {
        return userRepository.getUserByEmail(email)
    }
}