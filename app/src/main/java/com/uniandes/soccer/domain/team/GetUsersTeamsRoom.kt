package com.uniandes.soccer.domain.team

import com.uniandes.soccer.data.model.roomDB.UserRoom
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.data.repository.TeamRepository
import javax.inject.Inject

class GetUsersTeamsRoom @Inject constructor(
    private val teamRepository: TeamRepository
){
    suspend operator fun invoke(userId:String): List<Team> {
        val teamsRoom = teamRepository.getUsersTeamsRoom(userId)
        val teams = mutableListOf<Team>()
        if (teamsRoom != null) {
            for (team in teamsRoom) {
                val aux = Team(team.id, team.name, team.captain, team.commentaries, team.ranking, team.players, team.photo,
                    team.location)
                teams.add(aux)
            }
        }
        return teams
    }
}