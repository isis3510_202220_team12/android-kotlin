package com.uniandes.soccer

import android.app.Application
import com.uniandes.soccer.ui.viewModel.LocationViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FubolApp: Application()
/*
val appModule = module {
    viewModel { LocationViewModel(androidApplication()) }
}
 */
