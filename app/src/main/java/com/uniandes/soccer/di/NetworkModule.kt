package com.uniandes.soccer.di

import android.app.Application
import androidx.room.Room
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.uniandes.soccer.data.cache.LocationCache
import com.uniandes.soccer.data.cache.MyMatchesList
import com.uniandes.soccer.data.cache.MyTeamsList
import com.uniandes.soccer.data.cache.UserCache
import com.uniandes.soccer.data.repository.authentication.Authenticator
import com.uniandes.soccer.data.rooms.RoomDB
import com.uniandes.soccer.ui.viewModel.utilities.CompareStringsAlgorithm
import com.uniandes.soccer.ui.viewModel.utilities.LocationComparator
import com.uniandes.soccer.ui.viewModel.utilities.LocationComparatorMatch
import com.uniandes.soccer.ui.viewModel.utilities.TextsComparator
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideFirebaseStorage(): FirebaseStorage {
        return FirebaseStorage.getInstance()
    }

    @Singleton
    @Provides
    fun provideFireStoreDB(): FirebaseFirestore {
        var db = FirebaseFirestore.getInstance()
        return db
    }

    @Singleton
    @Provides
    fun provideUserCache(): UserCache {
        return UserCache
    }

    @Singleton
    @Provides
    fun locationCache(): LocationCache {
        return LocationCache
    }

    @Singleton
    @Provides
    fun provideMyMatchesCache(): MyMatchesList {
        return MyMatchesList
    }
    @Singleton
    @Provides
    fun provideMyTeamsCache(): MyTeamsList {
        return MyTeamsList
    }


    @Singleton
    @Provides
    fun provideAuthenticator(): Authenticator {
        return  Authenticator()
    }


    @Singleton
    @Provides
    fun provideLocationComparator(): LocationComparator {
        return  LocationComparator()
    }

    @Singleton
    @Provides
    fun provideLocationComparatorMatch(): LocationComparatorMatch {
        return  LocationComparatorMatch()
    }

    @Singleton
    @Provides
    fun provideTextsComparator(): TextsComparator {
        return  TextsComparator()
    }

    @Provides
    @Singleton
    fun provideDatabase(app: Application): RoomDB =
        Room.databaseBuilder(app, RoomDB::class.java, "user_database")
            .build()

}