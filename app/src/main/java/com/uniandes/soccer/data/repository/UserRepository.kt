package com.uniandes.soccer.data.repository

import android.util.Log
import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.roomDB.TeamRoom
import com.uniandes.soccer.data.model.roomDB.UserRoom
import com.uniandes.soccer.data.model.user.User
import com.uniandes.soccer.data.repository.user.UserRepositoryDB
import com.uniandes.soccer.data.rooms.RoomDB
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val userPersistence: UserRepositoryDB,
    roomDB: RoomDB
) {
    private val userDao = roomDB.userDao()

    suspend fun createUser(email:String,password: String, name: String, age: Int, location: GeoPoint,
                           isReferee: Boolean, teams:List<String>?, photo:String?, rating:Double?, description: String?, locationName: String): String {
        return userPersistence.createUser(email, password, name, age,location, isReferee, teams, photo, rating, description, locationName)
    }

    suspend fun getReferees(): List<User> {
        return userPersistence.getReferees()
    }

    suspend fun getPlayers(): List<User> {
        return userPersistence.getPlayers()
    }

    suspend fun getUserByEmail(email: String): User? {
        return userPersistence.getUserByEmail(email)
    }

    suspend fun getUserById(id: String): User? {
        return userPersistence.getUserById(id)
    }

    suspend fun getUserRoom(): UserRoom? {
        return withContext(Dispatchers.IO){
            val data = userDao.getUser()
            data[0]
        }
    }

    suspend fun insertUserRoom(user: UserRoom) {
        return withContext(Dispatchers.IO){
            userDao.insertUser(user)
        }
    }

    suspend fun deleteUserRoom(){
        return withContext(Dispatchers.IO){
            userDao.deleteUser()
        }
    }

    suspend fun updateUser(id: String, email:String,password: String, name: String, age: Int, location: GeoPoint,
                           isReferee: Boolean, teams:List<String>?, photo:String?, rating:Double?, description: String?, locationName: String) {
        userPersistence.updateUser(id, email, password, name, age,location, isReferee, teams, photo, rating, description, locationName)
    }

}
