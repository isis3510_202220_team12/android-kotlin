package com.uniandes.soccer.data.model.roomDB

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.team.Team

@Entity(tableName = "teams")
data class TeamRoom(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "name") val name: String = "",
    @ColumnInfo(name = "captain") val captain: String = "",
    @ColumnInfo(name = "password") val commentaries: String = "",
    @ColumnInfo(name = "age") val ranking: Int = 0,
    @ColumnInfo(name = "location") val location: GeoPoint? = GeoPoint(0.0,0.0),
    @ColumnInfo(name = "players") val players: List<String>? = listOf(),
    @ColumnInfo(name = "photo") val photo: Any?
)

fun Team.toDatabase() = TeamRoom(id=id, name = name, location = location, photo = photo, ranking = ranking,
players = players, commentaries = commentaries, captain = captain)