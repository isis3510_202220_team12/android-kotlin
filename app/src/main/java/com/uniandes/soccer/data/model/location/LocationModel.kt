package com.uniandes.soccer.data.model.location

data class LocationModel(
    var latitude:Double,
    var longitude:Double
)
