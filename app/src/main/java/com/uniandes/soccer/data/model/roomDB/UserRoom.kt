package com.uniandes.soccer.data.model.roomDB

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.user.User

@Entity(tableName = "users")
data class UserRoom(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "name") val name: String = "",
    @ColumnInfo(name = "email") val email: String = "",
    @ColumnInfo(name = "password") val password: String = "",
    @ColumnInfo(name = "age") val age: Int = 0,
    @ColumnInfo(name = "location") val location: GeoPoint = GeoPoint(0.0,0.0),
    @ColumnInfo(name = "isReferee") val isReferee: Boolean = false,
    @ColumnInfo(name = "teams") val teams: List<String>? = listOf(),
    @ColumnInfo(name = "description") val description: String? = "",
    @ColumnInfo(name = "locationName") val locationName: String = "",
    @ColumnInfo(name = "photo") val photo: Any?,
    @ColumnInfo(name = "rating") val rating: Double? = 0.0
)

fun User.toDatabase() = UserRoom(id=id, locationName = locationName, name = name, email = email,
password = password, age = age, location = location, isReferee = isReferee, description = description,
teams = teams, photo = photo, rating = rating)