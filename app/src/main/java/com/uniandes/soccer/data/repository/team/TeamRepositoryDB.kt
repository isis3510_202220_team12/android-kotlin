package com.uniandes.soccer.data.repository.team

import android.util.Log
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.SetOptions
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.model.user.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import javax.inject.Inject

class TeamRepositoryDB @Inject constructor (
    private val firestore: FirebaseFirestore
) {

     suspend fun createTeam(
        name: String,
        captain: String,
        commentaries: String,
        ranking: Int,
        players: List<String>,
        location: GeoPoint?
    ) {
        withContext(Dispatchers.IO) {
            val team = hashMapOf(
                "name" to name,
                "captain" to captain,
                "commentaries" to commentaries,
                "ranking" to ranking,
                "players" to players,
                "location" to location

            )
            val newTeam = firestore.collection("Teams").document()
            team["id"]=newTeam.id
            newTeam.set(team).addOnSuccessListener { documentReference ->
                Log.d("Create a team", "team created with ID: ${newTeam.id}")
            }
                .addOnFailureListener { e ->
                    Log.w("Create a team", "Error adding the team", e)
                }

        }
    }

     suspend fun getUsersTeams(userId: String): List<Team> {
        return withContext(Dispatchers.IO) {
            var list = mutableListOf<Team>()
            var user = firestore.collection("Users").document(userId).get().await()
            var userMap =user.data
            if(userMap?.get("teams") !=null){
                var teamIds:ArrayList<String> = userMap?.get("teams") as ArrayList<String>
                if(teamIds.size!=0){
                    for (id in teamIds){

                        var team = firestore.collection("Teams").document(id).get().await()

                        list.add(Team(id = team.data?.get("id") as String, name = team.data?.get("name") as String,
                            captain = team.data?.get("captain") as String, commentaries = team.data?.get("commentaries") as String,
                            ranking = team.data?.get("ranking").toString().toInt(),
                            players = user.data?.get("players") as List<String>?, photo = team.data?.get("photo") as String?,
                            location = user.data?.get("location") as GeoPoint?))
                    }

                }
                list
            }
            else{
                list
            }
        }
    }

     suspend fun getTeams(): List<Team> {
         return withContext(Dispatchers.IO) {
             val list = mutableListOf<Team>()
             val refs = firestore.collection("Teams").get().await()
             if(refs.size()>0) {
                 for (a in refs.documents) {

                     if (a != null) {

                         val id1 = if(a["id"] != null) a["id"] else ""
                         val name = if(a["name"] != null) a["name"] else ""
                         val captain = a["captain"]
                         val commentaries = if(a["commentaries"] != null) a["commentaries"] else ""
                         val ranking = if(a["ranking"] != null) a["ranking"].toString().toInt() else 0
                         val photo = a["photo"]
                         val locationT = a["location"]
                         val team1F = Team(id = id1 as String, name= name as String, captain= captain as String,
                             commentaries= commentaries as String, ranking= ranking as Int,photo=  photo,
                             players = null, location = locationT as GeoPoint?)
                         list.add(team1F)
                     }
                 }
             }
             list
         }
    }

     suspend fun getTeamByID(teamId: String): Team {
        return withContext(Dispatchers.IO){
            var list = mutableListOf<String>()
            var team: DocumentSnapshot = firestore.collection("Teams").document(teamId).get().await()
            var id:String = if(team.get("id")!= null) team.get("id") as String else ""
            var name:String = if(team.get("name")!= null) team.get("name") as String else ""
            var captain:String = if(team.get("captain")!= null) team.get("captain") as String else ""
            var commentaries:String = if(team.get("commentaries")!= null) team.get("commentaries") as String else ""
            var ranking:Int = if(team.get("ranking")!= null) team.get("ranking").toString().toInt() else 0
            var photo: Any? = team.get("photo")
            var location: GeoPoint? = team.get("location") as GeoPoint?
            Team(id = id, name = name, captain=captain,commentaries = commentaries,
                ranking = ranking, players = list, photo=photo, location = location )

        }
    }

}