package com.uniandes.soccer.data.model.user

import com.google.firebase.firestore.GeoPoint


data class User(
    val id:String="",
    val email:String="",
    val password:String="",
    val name:String="",
    val age: Int=0,
    val location: GeoPoint,
    val isReferee: Boolean=false,
    val teams: List<String>?,
    val photo: Any?,
    val rating: Double?,
    val description: String?,
    val locationName: String
)


