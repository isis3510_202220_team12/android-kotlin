package com.uniandes.soccer.data.cache

import android.util.LruCache
import com.uniandes.soccer.data.model.match.Match
import com.uniandes.soccer.data.model.team.Team

object MyTeamsList {
    val lru: LruCache<String, List<Team>> = LruCache(1024)
    fun saveTeamsToCache(key: String, teams: List<Team>){
        try {
            lru.put(key, teams)
        } catch (e: Exception){
            e.printStackTrace()
        }
    }
    fun retrieveTeamsFromCache(key: String): List<Team>?{
        try {
            return lru.get(key)
        } catch (e: Exception){
            e.printStackTrace()
        }
        return null
    }
}