package com.uniandes.soccer.data.rooms

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.uniandes.soccer.data.model.roomDB.TeamRoom
import com.uniandes.soccer.data.model.roomDB.UserRoom

@Database(entities = [UserRoom::class, TeamRoom::class], version = 1)
@TypeConverters(Converters::class)
abstract class RoomDB : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun teamsDao(): TeamsDao
}