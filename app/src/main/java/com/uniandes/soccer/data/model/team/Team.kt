package com.uniandes.soccer.data.model.team

import com.google.firebase.firestore.GeoPoint


data class Team(
    val id: String,
    val name: String,
    val captain: String,
    val commentaries: String,
    val ranking: Int,
    val players: List<String>?,
    val photo: Any?,
    val location: GeoPoint?
)