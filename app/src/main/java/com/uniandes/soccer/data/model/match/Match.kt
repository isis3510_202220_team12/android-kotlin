package com.uniandes.soccer.data.model.match

import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.team.Team

data class Match (
    val id:String,
    val dateMatch: Timestamp,
    val referee: String,
    val location: GeoPoint,
    val team1: Team,
    val team2: Team?,
    val locationName: String)