package com.uniandes.soccer.data.repository

import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.match.Match
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.repository.match.MatchRepositoryDB
import javax.inject.Inject

class MatchRepository @Inject constructor(
    private val matchPersistence: MatchRepositoryDB
) {
        suspend fun updateMatch(matchId:String, dateMatch: Timestamp, referee:String, location:GeoPoint, team1:Team, team2:Team, locationName:String) {
            return matchPersistence.updateMatch(matchId, dateMatch, referee, location, team1, team2, locationName)
        }

        suspend fun getTeamsMatches(teamId: String): List<Match> {
            return matchPersistence.getTeamsMatches(teamId)
        }


        suspend fun createMatch( dateMatch: Timestamp, referee: String, location: GeoPoint, team1: Team, locationName:String) {
            return matchPersistence.createMatch( dateMatch, referee, location, team1, locationName)
        }

        suspend fun getJoinableMatches(teamId: String): List<Match> {
            return matchPersistence.getJoinableMatches(teamId)
        }
}