package com.uniandes.soccer.data.repository

import com.google.firebase.auth.FirebaseUser
import com.uniandes.soccer.data.repository.authentication.Authenticator
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val authenticator : Authenticator
)  {

    suspend fun RegisterWithEmailPassword(email: String, password: String): FirebaseUser? {
        return authenticator.RegisterWithEmailPassword(email , password)
    }

    suspend fun signInWithEmailPassword(email: String, password: String): FirebaseUser? {
        return authenticator.signInWithEmailPassword(email , password)
    }

    fun signOut() {
        return authenticator.signOut()
    }

    fun getCurrentUser(): FirebaseUser? {
        return authenticator.getUser()
    }


}