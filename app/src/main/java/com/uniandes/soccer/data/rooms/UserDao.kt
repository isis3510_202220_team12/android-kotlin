package com.uniandes.soccer.data.rooms
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.uniandes.soccer.data.model.roomDB.UserRoom

@Dao
interface UserDao {


    @Query("SELECT * FROM users")
    suspend fun getUser(): List<UserRoom>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(users: UserRoom)

    @Query("DELETE FROM users")
    suspend fun deleteUser()
}