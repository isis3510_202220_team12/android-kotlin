package com.uniandes.soccer.data.repository.user

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.*
import com.uniandes.soccer.data.model.user.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import org.koin.core.component.getScopeId
import java.util.*
import javax.inject.Inject


class UserRepositoryDB @Inject constructor (
    private val firestore: FirebaseFirestore
    //private val firebaseStorage: FirebaseStorage
) {


    suspend fun createUser (
         email: String,
         password: String,
         name: String,
         age: Int,
         location: GeoPoint,
         isReferee: Boolean,
         teams: List<String>?,
         photo: String?,
         rating:Double?,
         description: String?,
         locationName: String
     ):String {
        val check = firestore.collection("Users")
            .whereEqualTo("email" , email).get().await()
        if (check.size()>0){
            throw Exception("An user with that email already exists")
        }
        return withContext(Dispatchers.IO) {
            val user = hashMapOf(
                "email" to email,
                "password" to password,
                "name" to name,
                "age" to age,
                "location" to location,
                "isReferee" to isReferee,
                "teams" to teams,
                "photo" to photo,
                "rating" to rating,
                "description" to description,
                "locationName" to locationName

            )
            val newUser = firestore.collection("Users").document()
            user["id"]=newUser.id
            newUser.set(user).addOnSuccessListener { documentReference ->
                Log.d("Create an user", "User created with ID: ${newUser.id}")
            }
                .addOnFailureListener { e ->
                    Log.w("Create an user", "Error adding the user", e)
                }
            newUser.id
        }
    }
   
     suspend fun getUserById(userId: String): User {
        return withContext(Dispatchers.IO){
            val user: DocumentSnapshot = firestore.collection("Users").document(userId).get().await()
            val id:String = if(user.get("id")!= null) user.get("id") as String else ""
            val email:String = if(user.get("email")!= null) user.get("email") as String else ""
            val password:String = if(user.get("password")!= null) user.get("password") as String else ""
            val name:String = if(user.get("name")!= null) user.get("name") as String else ""
            val age: Int = if(user.get("age")!= null) user.get("age").toString().toInt() else 0
            val location:GeoPoint = user.get("location") as GeoPoint
            val isReferee:Boolean = if(user.get("isReferee")!= null) user.get("isReferee") as Boolean else false
            val teams:List<String>? = user.get("teams") as List<String>?
            val photo: String? = user.get("photo") as String?
            val description: String? = user.get("description") as String?
            val locationName: String = user.get("locationName") as String
            val rating: Double? = if (user.get("rating") != null) user.get("rating").toString().toDouble() else null
            User(id = id, email = email, password=password,name = name, age = age,location = location,
                isReferee = isReferee,teams = teams, photo=photo, rating = rating, description=description, locationName = locationName)
        }
    }


    suspend fun getReferees(): List<User> {
        return withContext(Dispatchers.IO) {
            val list = mutableListOf<User>()
            val refs = firestore.collection("Users").whereEqualTo("isReferee", true).get().await()
            if(refs.size()>0) {
                for (a in refs.documents) {

                    if (a != null) {
                        val id: String = if (a.get("id") != null) a.get("id") as String else ""
                        val email: String = a.get("email") as String
                        val password: String = a.get("password") as String
                        val locationName: String = a.get("locationName") as String
                        val name: String = a.get("name") as String
                        val age: Int = a.get("age").toString().toInt()
                        val location: GeoPoint = a.get("location") as GeoPoint
                        val isReferee: Boolean = a.get("isReferee") as Boolean
                        val teams: ArrayList<String>? = a.get("teams") as ArrayList<String>?
                        val photo: String? = a.get("photo") as String?
                        val rating: Double? = if (a.get("rating") != null) a.get("rating").toString().toDouble() else null
                        val description: String = if (a.get("description") != null) a.get("description") as String else ""
                        val userF = User(
                            id = id, email = email, password = password, name = name, age = age,
                            location = location, isReferee = isReferee, teams = teams, photo = photo,
                            rating = rating, description = description, locationName = locationName)
                        list.add(userF)
                    }
                }
            }
            list
        }
    }


    suspend fun getPlayers(): List<User> {
        return withContext(Dispatchers.IO) {
            val list = mutableListOf<User>()
            val refs = firestore.collection("Users").whereEqualTo("isReferee", false).get().await()
            if(refs.size()>0) {
                for (a in refs.documents) {

                    if (a != null) {
                        val id: String = if (a.get("id") != null) a.get("id") as String else ""
                        val email: String = a.get("email") as String
                        val password: String = a.get("password") as String
                        val locationName: String = a.get("locationName") as String
                        val name: String = a.get("name") as String
                        val age: Int = a.get("age").toString().toInt()
                        val location: GeoPoint = a.get("location") as GeoPoint
                        val isReferee: Boolean = a.get("isReferee") as Boolean
                        val teams: ArrayList<String>? = a.get("teams") as ArrayList<String>?
                        val photo: String? = a.get("photo") as String?
                        val description: String = if (a.get("description") != null) a.get("description") as String else ""
                        val userF = User(
                            id = id, email = email, password = password, name = name, age = age,
                            location = location, isReferee = isReferee, teams = teams, photo = photo,
                            rating = null, description = description, locationName = locationName)
                        list.add(userF)
                    }
                }
            }
            list
        }
    }

    suspend fun getUserByEmail(emailP: String): User? {
        return withContext(Dispatchers.IO){
            var userF: User? = null
            val user = firestore.collection("Users")
                .whereEqualTo("email" , emailP).get().await()?.documents?.get(0)
            if(user!=null) {
                val id: String = if (user.get("id") != null) user.get("id") as String else ""
                val email: String = if (user.get("email") != null) user.get("email") as String else ""
                val password: String = if (user.get("password") != null) user.get("password") as String else ""
                val locationName : String = if (user.get("locationName") != null) user.get("locationName") as String else ""
                val name: String = if (user.get("name") != null) user.get("name") as String else ""
                val age: Int = if (user.get("age") != null) user.get("age").toString().toInt() else 0
                val location: GeoPoint = user.get("location") as GeoPoint
                val isReferee: Boolean = if (user.get("isReferee") != null) user.get("isReferee") as Boolean else false
                val teams: List<String>? = user.get("teams") as List<String>?
                val photo: String? = user.get("photo") as String?
                val description: String? = user.get("description") as String?
                val rating: Double? = if (user.get("rating") != null) user.get("rating").toString().toDouble() else null
                userF = User(
                    id = id,
                    email = email,
                    password = password,
                    name = name,
                    age = age,
                    location = location,
                    isReferee = isReferee,
                    teams = teams,
                    photo = photo,
                    rating = rating,
                    description = description,
                    locationName = locationName
                )
            }
            userF
        }
    }

    suspend fun updateUser(
        id: String,
        email: String,
        password: String,
        name: String,
        age: Int,
        location: GeoPoint,
        isReferee: Boolean,
        teams: List<String>?,
        photo: String?,
        rating: Double?,
        description: String?,
        locationName: String
    ) {
        return withContext(Dispatchers.IO) {
            val user = hashMapOf(
                "id" to id,
                "email" to email,
                "password" to password,
                "name" to name,
                "age" to age,
                "location" to location,
                "isReferee" to isReferee,
                "teams" to teams,
                "photo" to photo,
                "rating" to rating,
                "description" to description,
                "locationName" to locationName
            )

            firestore.collection("Users").document(id).set(user, SetOptions.merge()).await()
        }
    }


/*
    override suspend fun uploadUserPicture(id: String, image: Uri): Uri {
        return withContext(Dispatchers.IO){
            val refStorage = firebaseStorage.reference
            val idImg = UUID.randomUUID().toString()
            val ref = refStorage.child("UserProfile/"+ idImg)
            val uploadTask = ref.putFile(image).await()
            val user = firestore.collection("Users").document(id).update("userImage",idImg ).await()
            image
        }
    }

 */

/*
    override suspend fun getTeamsUsers(teamId: String): List<User> {
        return withContext(Dispatchers.IO) {
            val list = mutableListOf<User>()
            val team1 = firestore.collection("User").whereEqualTo("team1", teamId).get().await()
            val team2= firestore.collection("User").whereEqualTo("team2", teamId).get().await()
            for (a in team1.documents){
                val user = a.toObject(User::class.java)
                if (user != null) {
                    list.add(user)
                }
            }
            for (a in team2.documents){
                val user = a.toObject(User::class.java)
                if (user != null) {
                    list.add(user)
                }
            }
            list
        }
    }
    */



}