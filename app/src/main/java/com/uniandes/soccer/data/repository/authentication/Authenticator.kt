package com.uniandes.soccer.data.repository.authentication

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext


class Authenticator {

    suspend fun RegisterWithEmailPassword(email: String, password: String): FirebaseUser? {
        return withContext(Dispatchers.IO){
            Firebase.auth.createUserWithEmailAndPassword(email,password).await()
            Firebase.auth.signInWithEmailAndPassword(email , password).await()
            Firebase.auth.currentUser
        }

    }

    suspend fun signInWithEmailPassword(email: String, password: String): FirebaseUser? {
        return withContext(Dispatchers.IO){
            Firebase.auth.signInWithEmailAndPassword(email , password).await()
            Firebase.auth.currentUser
        }

    }

    fun signOut() {
        Firebase.auth.signOut()
    }

    fun getUser(): FirebaseUser? {
        return Firebase.auth.currentUser
    }
}