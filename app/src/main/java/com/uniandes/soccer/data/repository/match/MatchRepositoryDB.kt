package com.uniandes.soccer.data.repository.match

import android.util.Log
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.SetOptions
import com.uniandes.soccer.data.model.match.Match
import com.uniandes.soccer.data.model.team.Team
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import javax.inject.Inject


class MatchRepositoryDB @Inject constructor (
    private val firestore: FirebaseFirestore
) {


      suspend fun createMatch(
          dateMatch: Timestamp,
          referee: String,
          location: GeoPoint,
          team1: Team,
          locationName:String
    ) {
        withContext(Dispatchers.IO) {
            val team = hashMapOf(
                "id" to team1.id,
                "name" to team1.name,
                "captain" to team1.captain,
                "commentaries" to team1.commentaries,
                "ranking" to team1.ranking,
                "photo" to team1.photo,
            )
            val match = hashMapOf(
                "dateMatch" to dateMatch,
                "referee" to referee,
                "location" to location,
                "team1" to team,
                "locationName" to locationName
                )
            val newMatch = firestore.collection("Matches").document()
            match["id"]=newMatch.id
            newMatch.set(match).addOnSuccessListener { documentReference ->
                Log.d("Create a match", "Match created with ID: ${newMatch.id}")
            }
                .addOnFailureListener { e ->
                    Log.w("Create a match", "Error adding the match", e)
                }
        }
    }
   
     suspend fun getMatchByID(matchId: String): Match {
        return withContext(Dispatchers.IO){
            val match: DocumentSnapshot = firestore.collection("Matches").document(matchId).get().await()
            val id:String = if(match.get("id")!= null) match.get("id") as String else ""
            val referee:String = if(match.get("referee")!= null) match.get("referee") as String else ""
            val location:GeoPoint =  match.get("location") as GeoPoint
            val locationName:String =  if(match.get("locationName")!= null) match.get("locationName") as String else ""

            val team1:HashMap<String, Any> = match.get("team1") as HashMap<String, Any>
            val idF1 = if(team1["id"] != null) team1["id"] else ""
            val name = if(team1["name"] != null) team1["name"] else ""
            val captain = team1["captain"]
            val commentaries = if(team1["commentaries"] != null) team1["commentaries"] else ""
            val ranking = if(team1["ranking"] != null) team1["ranking"] else 0
            val photo = team1["photo"]
            val locationT = team1["location"]
            val team1F = Team(id = idF1 as String, name= name as String, captain= captain as String,
                commentaries= commentaries as String, ranking= ranking.toString().toInt(),photo=  photo, players = null,
                location = locationT as GeoPoint?)

            var team2F:Team?=null
            val team2:HashMap<String, Any>? = match.get("team2") as HashMap<String, Any>?
            if(team2!=null){
                val idF2 = if(team2["id"] != null) team2["id"] else ""
                val name2 = if(team2["name"] != null) team2["name"] else ""
                val captain2 = team2["captain"]
                val commentaries2 = if(team2["commentaries"] != null) team2["commentaries"] else ""
                val ranking2 = if(team2["ranking"] != null) team2["ranking"].toString().toInt()  else 0
                val photo2 = team2["photo"]
                val locationT = team1["location"]
                team2F = Team(id = idF2 as String, name= name2 as String, captain= captain2 as String,
                    commentaries= commentaries2 as String, ranking= ranking2 as Int,photo=  photo2, players = null,
                    location = locationT as GeoPoint?)

            }
            val dateMatch:Timestamp = match.get("dateMatch") as Timestamp
            Match(
                id=id,dateMatch = dateMatch, referee=referee,location = location, team1 = team1F, team2 = team2F,
                locationName = locationName)

        }
    }

     suspend fun getTeamsMatches(teamId: String): List<Match> {
        return withContext(Dispatchers.IO) {
            val list = mutableListOf<Match>()
            val team1Matches = firestore.collection("Matches").whereEqualTo("team1.id" , teamId).get().await()
            for (a in team1Matches.documents){


                val team2:HashMap<String, Any>? = a.get("team2") as HashMap<String, Any>?
                var team2F:Team? = null
                if (a != null) {
                    if(team2!=null){
                        val id = if(team2["id"] != null) team2["id"] else ""
                        val name = if(team2["name"] != null) team2["name"] else ""
                        val captain = team2["captain"]
                        val commentaries = if(team2["commentaries"] != null) team2["commentaries"] else ""
                        val ranking = if(team2["ranking"] != null) team2["ranking"].toString().toInt()  else 0
                        val photo = team2["photo"]
                        val locationT = team2["location"]
                        team2F = Team(id = id as String, name= name as String, captain= captain as String,
                            commentaries= commentaries as String, ranking= ranking as Int,photo=  photo,
                            players = null, location = locationT as GeoPoint?)
                    }
                    val idF:String = if(a.get("id")!= null) a.get("id") as String else ""
                    val referee:String = if(a.get("referee")!= null) a.get("referee") as String else ""
                    val location:GeoPoint =  a.get("location") as GeoPoint
                    val dateMatch:Timestamp = a.get("dateMatch") as Timestamp
                    val locationName:String = a.get("locationName") as String

                    val team1:HashMap<String, Any> = a.get("team1") as HashMap<String, Any>
                    val id = if(team1["id"] != null) team1["id"] else ""
                    val name = if(team1["name"] != null) team1["name"] else ""
                    val captain = team1["captain"]
                    val commentaries = if(team1["commentaries"] != null) team1["commentaries"] else ""
                    val ranking = if(team1["ranking"] != null) team1["ranking"].toString().toInt()  else 0
                    val photo = team1["photo"]
                    val locationT = team1["location"]
                    val team1F = Team(id = id as String, name= name as String, captain= captain as String,
                        commentaries= commentaries as String, ranking= ranking as Int,photo=  photo, players = null,
                        location = locationT as GeoPoint?)

                    val matchF = Match(
                        id=idF,dateMatch= dateMatch, location=location, referee = referee, team1 = team1F,
                        team2 = team2F, locationName = locationName)
                    list.add(matchF)
                }
            }
            val team2Matches= firestore.collection("Matches").whereEqualTo("team2.id", teamId).get().await()

            for (a in team2Matches.documents){

                val team2:HashMap<String, Any>? = a.get("team2") as HashMap<String, Any>?
                var team2F: Team? = null

                if (a != null ) {
                    if(team2!=null){
                        val id = if(team2["id"] != null) team2["id"] else ""
                        val name = if(team2["name"] != null) team2["name"] else ""
                        val captain = team2["captain"]
                        val commentaries = if(team2["commentaries"] != null) team2["commentaries"] else ""
                        val ranking = if(team2["ranking"] != null) team2["ranking"].toString().toInt()  else 0
                        val photo = team2["photo"]
                        val locationT = team2["location"]
                        team2F = Team(id = id as String, name= name as String, captain= captain as String,
                            commentaries= commentaries as String, ranking= ranking as Int,photo=  photo, players = null,
                            location = locationT as GeoPoint?)
                    }
                    val idF:String = if(a.get("id")!= null) a.get("id") as String else ""
                    val referee:String = if(a.get("referee")!= null) a.get("referee") as String else ""
                    val location:GeoPoint =  a.get("location") as GeoPoint
                    val dateMatch:Timestamp = a.get("dateMatch") as Timestamp
                    val locationName:String =  a.get("locationName") as String

                    val team1:HashMap<String, Any> = a.get("team1") as HashMap<String, Any>
                    val id = if(team1["id"] != null) team1["id"] else ""
                    val name = if(team1["name"] != null) team1["name"] else ""
                    val captain = team1["captain"]
                    val commentaries = if(team1["commentaries"] != null) team1["commentaries"] else ""
                    val ranking = if(team1["ranking"] != null) team1["ranking"].toString().toInt()  else 0
                    val photo = team1["photo"]
                    val locationT = team1["location"]
                    val team1F = Team(id = id as String, name= name as String, captain= captain as String,
                        commentaries= commentaries as String, ranking= ranking as Int,photo=  photo,
                        players = null, location = locationT as GeoPoint?)

                    val matchF = Match(id=idF,dateMatch= dateMatch, location=location, referee = referee, team1 = team1F,
                        team2 = team2F, locationName = locationName)
                    list.add(matchF)
                }
            }
            list
        }
    }

     suspend fun getJoinableMatches(teamId: String): List<Match> {
        return withContext(Dispatchers.IO) {
            val list = mutableListOf<Match>()
            val matches = firestore.collection("Matches").whereNotEqualTo("team1.id", teamId).get().await()
            for (a in matches.documents){

                val team2:HashMap<String, Any>? = a.get("team2") as HashMap<String, Any>?
                if (a != null && team2==null) {


                    val id:String = if(a.get("id")!= null) a.get("id") as String else ""
                    val referee:String = if(a.get("referee")!= null) a.get("referee") as String else ""
                    val location:GeoPoint =  a.get("location") as GeoPoint
                    val dateMatch:Timestamp = a.get("dateMatch") as Timestamp
                    val locationName:String =  a.get("locationName") as String

                    val team1:HashMap<String, Any> = a.get("team1") as HashMap<String, Any>
                    val id1 = if(team1["id"] != null) team1["id"] else ""
                    val name = if(team1["name"] != null) team1["name"] else ""
                    val captain = team1["captain"]
                    val commentaries = if(team1["commentaries"] != null) team1["commentaries"] else ""
                    val ranking = if(team1["ranking"] != null) team1["ranking"].toString().toInt() else 0
                    val photo = team1["photo"]
                    val locationT = team1["location"]
                    val team1F = Team(id = id1 as String, name= name as String, captain= captain as String,
                        commentaries= commentaries as String, ranking= ranking as Int,photo=  photo,
                        players = null, location = locationT as GeoPoint?)

                    val matchF = Match(id = id,dateMatch= dateMatch, location=location, referee = referee, team1 = team1F, team2 = null, locationName = locationName)
                    list.add(matchF)
                }
            }
            list
        }
    }



     suspend fun updateMatch(matchId: String, dateMatch:Timestamp, referee:String, location:GeoPoint, team1: Team, team2: Team, locationName: String){
        withContext(Dispatchers.IO) {
            val teamA = hashMapOf(
                "id" to team1.id,
                "name" to team1.name,
                "captain" to team1.captain,
                "commentaries" to team1.commentaries,
                "ranking" to team1.ranking,
                "photo" to team1.photo,
            )
            val teamB = hashMapOf(
                "id" to team2.id,
                "name" to team2.name,
                "captain" to team2.captain,
                "commentaries" to team2.commentaries,
                "ranking" to team2.ranking,
                "photo" to team2.photo,
            )
            val match1 = hashMapOf(
                "id" to matchId,
                "dateMatch" to dateMatch,
                "referee" to referee,
                "location" to location,
                "team1" to teamA,
                "team2" to teamB,
                "locationName" to locationName
            )
            firestore.collection("Matches").document(matchId).set(match1, SetOptions.merge()).await()
        }
    }

}