package com.uniandes.soccer.data.repository

import com.google.firebase.firestore.GeoPoint
import com.uniandes.soccer.data.model.match.Match
import com.uniandes.soccer.data.model.roomDB.TeamRoom
import com.uniandes.soccer.data.model.roomDB.UserRoom
import com.uniandes.soccer.data.model.team.Team
import com.uniandes.soccer.data.repository.team.TeamRepositoryDB
import com.uniandes.soccer.data.rooms.RoomDB
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class TeamRepository @Inject constructor(
    private val teamPersistence: TeamRepositoryDB,
    private val roomDB: RoomDB
    ) {
    private val teamsDao = roomDB.teamsDao()

    suspend fun getUsersTeams(userId:String): List<Team> {
        return teamPersistence.getUsersTeams(userId)
    }

    suspend fun getUsersTeamsRoom(userId:String): List<TeamRoom>? {
        return withContext(Dispatchers.IO){
            val data = teamsDao.getTeams(userId)
            data
        }
    }

    suspend fun insertTeamsRoom(teams: List<TeamRoom>) {
        return withContext(Dispatchers.IO){
            teamsDao.insertTeams(teams)
        }
    }

    suspend fun deleteTeamsRoom(){
        return withContext(Dispatchers.IO){
            teamsDao.deleteTeams()
        }
    }

    suspend fun getTeams(): List<Team> {
        return teamPersistence.getTeams()
    }


    suspend fun createTeam(name:String,captain: String, commentaries: String, ranking: Int, players: List<String>,
    location: GeoPoint?){
        return teamPersistence.createTeam(name,captain, commentaries, ranking, players, location)
    }

    suspend fun getTeamByID(teamId:String): Team =teamPersistence.getTeamByID(teamId)


}