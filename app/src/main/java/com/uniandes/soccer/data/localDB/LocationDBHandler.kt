package com.uniandes.soccer.data.localDB

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.uniandes.soccer.data.model.location.LocationModel

class LocationDBHandler(context: Context?) :
    SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        val query = ("CREATE TABLE " + TABLE_NAME + " ("
                + ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + LATITUDE_COL + " REAL,"
                + LONGITUDE_COL + " REAL)")

        db.execSQL(query)
    }

    fun addLocation(
        locationLatitude: Double?,
        locationLongitude: Double?
    ) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(LATITUDE_COL, locationLatitude)
        values.put(LONGITUDE_COL, locationLongitude)
        db.insert(TABLE_NAME, null, values)
        db.close()
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    companion object {
        private const val DB_NAME = "locationdb"

        private const val DB_VERSION = 1

        private const val TABLE_NAME = "mylocations"

        private const val ID_COL = "id"

        private const val LATITUDE_COL = "latitude"

        private const val LONGITUDE_COL = "longitude"
    }

    fun readLocation(): ArrayList<LocationModel>? {
        val db = this.readableDatabase

        val locationEntrances: Cursor = db.rawQuery("SELECT * FROM $TABLE_NAME", null)

        val courseModelArrayList: ArrayList<LocationModel> = ArrayList()

        if (locationEntrances.moveToFirst()) {
            do {
                courseModelArrayList.add(
                    LocationModel(
                        locationEntrances.getDouble(1),
                        locationEntrances.getDouble(2)
                    )
                )
            } while (locationEntrances.moveToNext())
        }
        locationEntrances.close()
        return courseModelArrayList
    }
}