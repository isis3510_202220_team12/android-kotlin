package com.uniandes.soccer.data.rooms
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.uniandes.soccer.data.model.roomDB.TeamRoom
import com.uniandes.soccer.data.model.roomDB.UserRoom

@Dao
interface TeamsDao {

    @Query("SELECT * FROM teams WHERE captain=:userId")
    suspend fun getTeams(userId:String): List<TeamRoom>?


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTeams(teams: List<TeamRoom>)

    @Query("DELETE FROM teams")
    suspend fun deleteTeams()
}