package com.uniandes.soccer.data.cache

import com.google.firebase.firestore.GeoPoint

object UserCache {
    var locationName: String = ""
    var name: String = ""
    var id: String = ""
    var email: String = ""
    var password: String = ""
    var age: Int = 0
    var location: GeoPoint = GeoPoint(0.0,0.0)
    var teams: List<String>? = null
    var isReferee: Boolean = false
    var photo: Any? = null

}