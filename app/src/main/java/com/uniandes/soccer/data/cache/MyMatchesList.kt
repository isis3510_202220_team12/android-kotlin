package com.uniandes.soccer.data.cache

import android.util.LruCache
import com.uniandes.soccer.data.model.match.Match

object MyMatchesList {
    val lru: LruCache<String, List<Match>> = LruCache(1024)
    fun saveMatchToCache(key: String, match: List<Match>){
       try {
           lru.put(key, match)
       } catch (e: Exception){
           e.printStackTrace()
       }
    }
    fun retrieveMatchesFromCache(key: String): List<Match>?{
        try {
            return lru.get(key)
        } catch (e: Exception){
            e.printStackTrace()
        }
        return null
    }
}