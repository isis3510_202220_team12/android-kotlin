package com.uniandes.soccer.data.rooms

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.google.firebase.firestore.GeoPoint
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import java.util.*

class Converters {

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }

    @TypeConverter
    fun stringToMapAny(value: String): HashMap<String, Any> {
        return Gson().fromJson(value,  object : TypeToken<HashMap<String, Any>>() {}.type)
    }

    @TypeConverter
    fun stringToMap(value: String): HashMap<String, String> {
        return Gson().fromJson(value,  object : TypeToken<HashMap<String, String>>() {}.type)
    }

    @TypeConverter
    fun mapToString(value: HashMap<String, String>?): String {
        return if(value == null) "" else Gson().toJson(value)
    }

    @TypeConverter
    fun mapToStringAny(value: HashMap<String, Any>?): String {
        return if(value == null) "" else Gson().toJson(value)
    }

    @TypeConverter
    fun stringToGeoPoint(value: String?): GeoPoint? {
        return if(value == null) GeoPoint(0.0,0.0) else Gson().fromJson(value,
            object : TypeToken<GeoPoint>() {}.type)
    }

    @TypeConverter
    fun geoPointToString(value: GeoPoint?): String? {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun stringToListString(value: String?): List<String>? {
        return if(value == null) listOf<String>() else Gson().fromJson(value,
            object : TypeToken<List<String>>() {}.type)
    }

    @TypeConverter
    fun listStringToString(value: List<String>?): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun stringToAny(value: String?): Any? {
        val aux = Gson().toJson(value)
        return Gson().fromJson(aux,
            object : TypeToken<String>() {}.type)
    }

    @TypeConverter
    fun anyToString(value: Any?): String {
        return Gson().toJson(value)
    }

}